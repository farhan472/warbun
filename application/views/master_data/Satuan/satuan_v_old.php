<main class="mt-5 mb-5">
    <section class="container">
        <div class="content-menu">
            <h5 class="card-title">Satuan</h5>
            <hr>
                <div id="message">
                </div>

                <div id="toolbar"> 
                    <a href="#" class="btn btn-sm btn-success" data-toggle="modal" data-target="#ModalaAdd"><span class="fa fa-plus"></span> Tambah Barang</a>
                </div>
                <table 
                class="table border-0 mt-4 mb-4" 
                id="tableSatuan" 
                data-toggle="table" 
                data-search="true"
                data-show-refresh="true" 
                data-pagination="true" 
                data-page-size="5" 
                data-page-list="[5,10, All]"
                data-show-toggle="true" 
                data-side-pagination="server" 
                data-resizable="false" 
                data-show-export="true"
                data-export-types="['json', 'xml', 'csv', 'txt', 'sql', 'excel', 'pdf']" 
                data-export-options='{"fileName": "Data_Satuan"}'
                data-click-to-select="true" 
                data-toolbar="#toolbar" 
                data-show-columns="true"
                data-show-pagination-switch="true" 
                data-url="<?= site_url("Master_data/satuan/dataSatuan") ?>">
                    <thead>
                        <tr>
                            <th data-formatter="numberFormatter">No</th>
                            <th data-field="NAMA_SATUAN" data-sortable="true">Nama Satuan</th>
                            <th data-field="STATUS" data-sortable="true">Status</th>
                            <th data-formatter="actionFormatter" data-field="ID" data-force-hide="true" class="text-center" data-width="200">Aksi</th>
                        </tr>
                    </thead>
                </table>

        </div>

        <!-- MODAL ADD -->
        <div class="modal fade" id="ModalaAdd" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title" id="myModalLabel">Tambah Data Satuan</h3>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <form class="form-horizontal" id="formTambah">
                        <div class="modal-body">
                            <div class="alert alert-danger print-error-msg" style="display:none"></div>
                            <div class="form-group">
                                <label class="control-label col-xs-3" >Nama Satuan</label>
                                <div class="col-xs-9">
                                    <input name="satuan" id="satuan" class="form-control" type="text" placeholder="Satuan" style="width:335px;">
                                </div>
                            </div>
        
                        </div>
        
                        <div class="modal-footer">
                            <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
                            <button class="btn btn-info" id="btn_simpan">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!--END MODAL ADD-->
 
        <!-- MODAL EDIT -->
        <div class="modal fade" id="ModalaEdit" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title" id="myModalLabel">Edit Data Satuan</h3>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <form class="form-horizontal" id="formEdit">
                        <div class="modal-body">
        
                        <div class="form-group">
                                <label class="control-label col-xs-3" >Nama Satuan</label>
                                <div class="col-xs-9">
                                    <input type="text" name="satuan2" id="satuan2" value="" placeholder="Satuan" style="width:335px;"  class="form-control">
                                </div>
                                    <input type="hidden" name="idSatuan" id="idSatuan" value="">
                            </div>
        
                        </div>
                        <div class="modal-footer">
                            <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
                            <button class="btn btn-info" id="btn_update">Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!--END MODAL EDIT-->
 
        <!--MODAL HAPUS-->
        <div class="modal fade" id="ModalHapus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel">Hapus Barang</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
                    </div>
                    <form class="form-horizontal" id="formHapus">
                    <div class="modal-body">
                                           
                            <input type="hidden" name="kode" id="textkode" value="">
                            <div class="alert alert-warning"><p>Apakah Anda yakin mau memhapus barang ini?</p></div>
                                         
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                        <button class="btn_hapus btn btn-danger" id="btn_hapus">Hapus</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        <!--END MODAL HAPUS-->

    </section>
</main>

<script>
    function numberFormatter(value, row, index) {
        var options = $('#tableSatuan').bootstrapTable('getOptions')
        var tes = 0
        if (!isNaN(options['pageSize'])) { //cek pagesize angka atau tidak, klo angka jalankan kode dibawah
            tes = ((options["pageNumber"] - 1) * options["pageSize"])
        }
        return index + 1 + tes;
    }

    function actionFormatter(value, row, index) {
        return [
            '<a href="#" class="badge badge-primary item_edit" data-toggle="modal" data-target="#ModalaEdit" data="'+value+'"><i class="fa fa-pencil-square-o" title="Edit Data"></i></a>',
            ' ',
            '<a href="#" class="badge badge-warning" id="btn-aktif" data="' + value + '" ><i class="fa fa-check" aria-hidden="true" title="Aktif / Nonaktif Data"></i></a>',
            ' ',
            '<a href="#" class="badge badge-danger item_hapus" data-toggle="modal" data-target="#ModalHapus" data="'+value+'"><i class="fa fa-trash" title="Hapus Data"></i></a>'
        ].join('');
    }

    function tambahData(){
            let satuan = $('#satuan').val();
            $.ajax({
                type : "POST",
                url  : "<?php echo site_url('master_data/Satuan/add'); ?>",
                dataType: "json",
                data : {satuan : satuan },
                success: function(data){
                    if($.isEmptyObject(data.error)){
                        $('[name="satuan"]').val("");
                        $('#ModalaAdd').modal('hide');
                        $('#tableSatuan').bootstrapTable('refresh');
                        
                        message("sukses","Berhasil Menambah Data!");
	                	$(".print-error-msg").css('display','none');

	                }else{
						$(".print-error-msg").css('display','block');
	                	$(".print-error-msg").html(data.error);
	                }
                },
                error : function(){
                      message("gagal","Gagal Menambah Data!");
                }
            });
            return false;
    }

    function aktifStatus(id){
        let idData = id ;
        $.ajax({
                type: 'post',
                url: '<?php echo site_url('master_data/Satuan/aktifStatus/'); ?>',
                data: {id : idData},
                success: function (data) {
                    $('#tableSatuan').bootstrapTable('refresh');
                    sukses("Berhasil Mengaktifkan/Menonaktifkan Data!");
                },
                error : function(){
                        gagal("Gagal Mengaktifkan/Menonaktifkan Data!");
                }
            });
    }

    function updateData(){
        let satuan = $('#satuan2').val();
        console.log(satuan);
        let id = $('#idSatuan').val();
            $.ajax({
                type : "POST",
                url  : "<?php echo site_url('master_data/Satuan/edit'); ?>",
                data : {id:id , satuan : satuan},
                success: function(data){
                    $('#satuan2').val("");
                    $('#idSatuan').val("");
                    $('#ModalaEdit').modal('hide');
                    $('#tableSatuan').bootstrapTable('refresh');

                    sukses("Berhasil Mengubah Data!");
                },
                error : function(){
                        gagal("Gagal Mengubah Data!");
                }
            });
            return false;
    }

    function hapusData(){
        var idData =$('#textkode').val();
        $.ajax({
        type : "POST",
        url  : "<?php echo site_url('master_data/Satuan/hapus/'); ?>",
                data : {id: idData},
                success: function(data){
                        $('#ModalHapus').modal('hide');
                        $('#tableSatuan').bootstrapTable('refresh');
                        
                        sukses("Berhasil Menghapus Data!");
                },
                error : function(){
                        gagal("Gagal Menghapus Data!");
                }
            });
            return false;
    }

    function message(status,pesan){
        if(status == 'sukses'){
            $('#message').empty();
            $('#message').append("<div class='alert alert-success alert-dismissible mt-3' role='alert' id='alertSukses'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button><span id='textSukses'>"+pesan+"</span></div>");
        }else{
            $('#message').empty();
            $('#message').append("<div class='alert alert-danger alert-dismissible mt-3' role='alert' id='alertGagal'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button><span id='textGagal'>"+pesan+"</span></div>");
        }
    }



</script>
<script>
// add 
$(document).on('submit','#formTambah',function(e){
    e.preventDefault();
    tambahData();
});
</script>

 <!-- update  -->
<script>
$(document).on('click','#btn_update',function(e){
    e.preventDefault();
    updateData();
});
</script>
<!-- get update -->
<script>
//GET UPDATE
$(document).on('click','.item_edit',function(){
            var id=$(this).attr('data');
            $.ajax({
                type : "GET",
                url  : "<?php echo site_url('master_data/Satuan/getDataSatuan/'); ?>" + id,
                dataType : "JSON",
                data : {id:id},
                success: function(data){
                        $('#ModalaEdit').modal('show');

                        $('[name="idSatuan"]').val(data[0].ID);
                        $('[name="satuan2"]').val(data[0].NAMA_SATUAN);
                }
            });
            return false;
        });
</script>

<script>
//GET HAPUS
$(document).on('click','.item_hapus',function(){
            var id=$(this).attr('data');
            $('#ModalHapus').modal('show');
            $('[name="kode"]').val(id);
        });
</script>

<!-- Hapus -->
<script>
$(document).on('click','#btn_hapus',function(e){
    e.preventDefault();
    hapusData();        
});
</script>

<script>
$(document).on('click', '#btn-aktif', function(){
            let id = $(this).attr('data');
            aktifStatus(id);
    });
</script>

