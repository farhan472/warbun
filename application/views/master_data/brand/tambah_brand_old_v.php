<main class="mt-5 mb-5">
    <section class="container">
        <div class="content-menu">
            <div class="card-body">
                <form action="<?php echo site_url('Master_data/brand/submitTambahBrand');?>" method="POST" enctype="multipart/form-data">
                <div class="form-row">
                    <div class="col-md-6">
                        <label for="kode">Kode Brand</label>
                        <input type="text" class="form-control no-border" name="kode" id="kode" placeholder="Masukkan Kode Brand" value="<?php echo set_value("kode")?>">
                        <?php echo form_error('kode', "<span class='text-danger'>", "</span>"); ?>
                    </div>
                    <div class="col-md-6">
                        <label for="nama">Nama Brand</label>
                        <input type="text" class="form-control no-border" name="nama" id="nama" placeholder="Masukkan Nama Brand" value="<?php echo set_value("nama")?>">
                        <?php echo form_error('nama', "<span class='text-danger'>", "</span>"); ?>
                    </div>
                    </div>
                    <div class="action clearfix mt-5">
                        <button class="btn btn-success float-right ml-3">Simpan</button>
                        <a href="<?php echo site_url('Master_data/brand')?>" class="btn btn-primary float-right">Kembali</a>
                    </div>
                </form>
            </div>
        </div>
    </section>
</main>
