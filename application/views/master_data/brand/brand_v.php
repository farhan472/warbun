<main class="mt-5 mb-5">
    <section class="container">
        <div class="content-menu">
            <div class="card-body">
                <h5 class="card-title">Brand</h5>
                <hr>
                <div id="message">
                </div>

                <div id="toolbar"> 
                    <a href="#" class="btn btn-sm btn-success" data-toggle="modal" data-target="#Modal" id="item_tambah"><i class="fa fa-plus"></i></a>
                </div>
                <table 
                class="table border-0 mt-4 mb-4" 
                id="tableBrand" 
                <?php echo DEFAULT_BOOTSTRAP_TABLE_CONFIG ?>
                data-export-options='{"fileName": "Data_Brand"}'
                data-url="<?= site_url("Master_data/brand/dataBrand") ?>">
                    <thead>
                        <tr>
                            <th data-formatter="numberFormatter">No</th>
                            <th data-field="KODE" data-sortable="true">Kode</th>
                            <th data-field="NAMA_BRAND" data-sortable="true">Nama Brand</th>
                            <th data-field="STATUS" data-sortable="true">Status</th>
                            <!-- <th data-field="CREATED_DATE" data-sortable="true">Created date</th> -->
                            <!-- <th data-field="UPDATED_DATE" data-sortable="true">Updated date</th> -->
                            <th data-formatter="actionFormatter" data-field="ID" data-force-hide="true" class="text-center" data-width="200">Aksi</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>

        <!-- MODAL -->
        <div class="modal fade" id="Modal" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Add -->
                    <div id="contentTambah" style="display:none">
                        <div class="modal-header">
                            <h3 class="modal-title" id="myModalLabel">Tambah Data Brand</h3>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        </div>
                        <form class="form-horizontal" id="formTambah">
                            <div class="modal-body">
                                <div class="alert alert-danger print-error-msg" style="display:none"></div>
                                <div class="form-group">
                                    <label class="control-label col-xs-3" >Nama Brand</label>
                                    <div class="col-xs-9">
                                        <input name="tambah_nama" id="tambah_nama" class="form-control" type="text" placeholder="Nama Brand" style="width:335px;">
                                    </div>
                                    <label class="control-label col-xs-3" >Kode Brand</label>
                                    <div class="col-xs-9">
                                        <input name="tambah_kode" id="tambah_kode" class="form-control" type="text" placeholder="Kode Brand" style="width:335px;">
                                    </div>
                                </div>
            
                            </div>
            
                            <div class="modal-footer">
                                <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
                                <button class="btn btn-info" id="btn_tambah">Simpan</button>
                            </div>
                        </form>
                    </div>

                    <!-- Edit -->
                    <div id="contentUbah" style="display:none">
                        <div class="modal-header">
                            <h3 class="modal-title" id="myModalLabel">Ubah Data Brand</h3>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        </div>
                        <form class="form-horizontal" id="formEdit">
                            <div class="modal-body">
                                <div class="alert alert-danger print-error-msg" style="display:none"></div>
                            <div class="form-group">
                                    <label class="control-label col-xs-3" >Nama Brand</label>
                                    <div class="col-xs-9">
                                        <input type="text" name="ubah_nama" id="ubah_nama" value="" placeholder="Nama Brand" style="width:335px;"  class="form-control">
                                    </div>
                                    <label class="control-label col-xs-3" >Kode Brand</label>
                                    <div class="col-xs-9">
                                        <input type="text" name="ubah_kode" id="ubah_kode" value="" placeholder="Kode Brand" style="width:335px;"  class="form-control">
                                    </div>
                                    <label class="control-label col-xs-3" >Status Brand</label>
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input" name="ubah_status" id="ubah_status" value="1">
                                        <label class="custom-control-label" for="ubah_status"></label>
                                    </div>
                                        <input type="hidden" name="ubah_id" id="ubah_id" value="">
                                </div>
            
                            </div>
                            <div class="modal-footer">
                                <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
                                <button class="btn btn-info" id="btn_update">Ubah</button>
                            </div>
                        </form>
                    </div>

                    <!-- Hapus -->
                    <div id="contentHapus" style="display:none">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel">Hapus Barang</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
                        </div>
                        <form class="form-horizontal" id="formHapus">
                            <div class="modal-body">
                                                
                                    <input type="hidden" name="hapus_id" id="hapus_id" value="">
                                    <div class="alert alert-warning"><p>Apakah Anda yakin mau menghapus barang ini?</p></div>
                                                
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                <button class="btn_hapus btn btn-danger" id="btn_hapus">Hapus</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!--END MODAL-->

    </section>
</main>

<script>
    function numberFormatter(value, row, index) {
        var options = $('#tableBrand').bootstrapTable('getOptions')
        var tes = 0
        if (!isNaN(options['pageSize'])) { //cek pagesize angka atau tidak, klo angka jalankan kode dibawah
            tes = ((options["pageNumber"] - 1) * options["pageSize"])
        }
        return index + 1 + tes;
    }

    function actionFormatter(value, row, index) {
        return [
            `<a href="#" class="badge badge-primary item_edit" data-toggle="modal" data-target="#Modal" data="${value}"><i class="fa fa-pencil-square-o" title="Edit Data"></i></a>

            <a href="#" class="badge badge-warning" id="btn-aktif" data="${value}" ><i class="fa fa-check" aria-hidden="true" title="Aktif / Nonaktif Data"></i></a>
            
            <a href="#" class="badge badge-danger item_hapus" data-toggle="modal" data-target="#Modal" data="${value}"><i class="fa fa-trash" title="Hapus Data"></i></a>`
        ];
    }

    function tambahData(){
        let nama = $('#tambah_nama').val();
        let kode = $('#tambah_kode').val();
        $.ajax({
            type : "POST",
            url  : "<?php echo site_url('master_data/Brand/add'); ?>",
            dataType: "json",
            data : {nama : nama , kode : kode },
            success: function(data){
                if($.isEmptyObject(data.error)){
                    $('[name="tambah_nama"]').val("");
                    $('[name="tambah_kode"]').val("");
                    $('#Modal').modal('hide');
                    $('#tableBrand').bootstrapTable('refresh');
                    
                    message("sukses",data.sukses);
                    $(".print-error-msg").css('display','none');

                }else{
                    $(".print-error-msg").css('display','block');
                    $(".print-error-msg").html(data.error);
                }
            },
            error : function(){
                $('#Modal').modal('hide');
                message("gagal","Proses Gagal");
            }
        });
        return false;
    }

    function updateData(){
        let nama = $('#ubah_nama').val();
        let kode = $('#ubah_kode').val();
        let id = $('#ubah_id').val();
        
        if ($('#ubah_status').prop('checked')) {
            var status = '1';
        }else {
            var status = '0';
        }
            $.ajax({
                type : "POST",
                url  : "<?php echo site_url('master_data/Brand/edit'); ?>",
                dataType: "json",
                data : {id:id , nama : nama , kode : kode ,status : status},
                success: function(data){
                    if($.isEmptyObject(data.error)){
                    $('#ubah_nama').val("");
                    $('#ubah_kode').val("");
                    $('#ubah_id').val("");
                    $('#Modal').modal('hide');
                    $('#tableBrand').bootstrapTable('refresh');

                    message("sukses",data.sukses);
                    $(".print-error-msg").css('display','none');

                    }else{
                        $(".print-error-msg").css('display','block');
                        $(".print-error-msg").html(data.error);
                    }
                },
                error : function(){
                    $('#Modal').modal('hide');
                    message("gagal","Proses Gagal");
                }
            });
            return false;
    }

    function hapusData(){
        var id =$('#hapus_id').val();
        $.ajax({
        type : "POST",
        url  : "<?php echo site_url('master_data/Brand/hapus/'); ?>",
        dataType: "json",
        data : {id: id},
                success: function(data){
                    $('#Modal').modal('hide');
                    $('#tableBrand').bootstrapTable('refresh');
                    if($.isEmptyObject(data.error)){
                        message("sukses",data.sukses);
                    }else{
                        message("gagal",data.error);
                    }
                },
                error : function(){
                    $('#Modal').modal('hide');
                    message("gagal","Proses Gagal");
                }
            });
            return false;
    }

    function aktifStatus(idData){
        let id = idData ;
        $.ajax({
                type: 'post',
                url: '<?php echo site_url('master_data/Brand/aktifStatus/'); ?>',
                dataType: "json",
                data: {id : id},
                success: function (data) {
                    $('#tableBrand').bootstrapTable('refresh');
                    message("sukses",data.sukses);
                },
                error : function(){
                    message("gagal","Proses Gagal");
                }
            });
    }


</script>
<script>
$(document).ready(function(){

// Get Tambah 
    $('#toolbar').on('click','#item_tambah',function(){
        $('#contentHapus').hide();
        $('#contentUbah').hide();

        $('#Modal').modal('show');
        $('#contentTambah').show();
    });

// Action Tambah 
    $('#contentTambah').on('submit','#formTambah',function(e){
        e.preventDefault();
        tambahData();
    });

// Get Hapus

    $('#tableBrand').on('click','.item_hapus',function(){
        var id=$(this).attr('data');
        $('#contentTambah').hide();
        $('#contentUbah').hide();

        $('#Modal').modal('show');
        $('#contentHapus').show();

        $('[name="hapus_id"]').val(id);
    });

// Action Hapus

    $('#contentHapus').on('click','#btn_hapus',function(e){
        e.preventDefault();
        hapusData();        
    });

// Get Ubah
    $('#tableBrand').on('click','.item_edit',function(){
        var id=$(this).attr('data');
        $.ajax({
            type : "GET",
            url  : "<?php echo site_url('master_data/Brand/getDataBrand/'); ?>" + id,
            dataType : "JSON",
            data : {id:id},
            success: function(data){
                    $('#contentTambah').hide();
                    $('#contentHapus').hide();
                    
                    $('#Modal').modal('show');
                    $('#contentUbah').show();

                    $('[name="ubah_id"]').val(id);
                    $('[name="ubah_nama"]').val(data.data[0].NAMA_BRAND);
                    $('[name="ubah_kode"]').val(data.data[0].KODE);
                    
                    for(let i = 0 ;i < data.status.length ; i++){
                        if (data.data[0].STATUS == '1') {
                            $('[name="ubah_status"]').prop("checked", true);
                        }else{
                            $('[name="ubah_status"]').prop("checked", false);
                        }
                    }
            }
        });
        return false;
    });

// Action Ubah 

    $('#contentUbah').on('click','#btn_update',function(e){
        e.preventDefault();
        updateData();
    });

// Action Aktif / Nonaktif 
    $('#tableBrand').on('click', '#btn-aktif', function(){
        let id = $(this).attr('data');
        aktifStatus(id);
    });

});
</script>