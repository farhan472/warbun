<main class="mt-5 mb-5">
    <section class="container">
        <div class="content-menu">
            <div class="card-body">
                <form action="<?php echo site_url('Master_data/posisi/submitEditPosisi/'.$id);?>" method="POST" enctype="multipart/form-data">
                <div class="form-row">
                    <div class="col-md-6">
                        <label for="posisi">Nama Posisi</label>
                        <?php $curval =  set_value("posisi") ? set_value("posisi") : $data['NAMA_POSISI']?>
                        <input type="text" class="form-control no-border" name="posisi" id="nama" placeholder="Masukkan Nama Posisi" value="<?php echo $curval?>">
                        <?php echo form_error('posisi', "<span class='text-danger'>", "</span>"); ?>
                    </div>
                    <div class="col-md-12 mt-3">
                        <span>Status</span>
                    <?php foreach($status as $st):?>
                        <div class="form-check form-check-radio form-check-inline">
                            <label class="form-check-label">
                                <input class="form-check-input" name="status" type="radio" value="<?php echo $st['KODE']?>"
                                    <?php echo ($data["STATUS"] == $st['KODE']? ' checked' : ''); ?>><?php echo $st['NAMA_STATUS']?>
                                <span class="form-check-sign"></span>
                            </label>
                        </div>
                    <?php endforeach ?>
                    <?= form_error('status', '<small class="text-danger">', '</small>') ?> 
                    </div>
                    <div class="action clearfix mt-5">
                        <button class="btn btn-success float-right ml-3">Simpan</button>
                        <a href="<?php echo site_url('Master_data/posisi')?>" class="btn btn-primary float-right">Kembali</a>
                    </div>
                </form>
            </div>
        </div>
    </section>
</main>
