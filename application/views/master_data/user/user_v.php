<main class="mt-5 mb-5">
    <section class="container">
        <div class="content-menu">
            <div class="card-body">
                <h5 class="card-title">User</h5>
                <hr>
                <?php echo $this->session->flashdata('status'); ?>
                    <?php 
                    $data=$this->session->flashdata('sukses');
                    if($data!=""){ ?>
                        <div id="notifikasi" class="alert alert-success"><strong>Sukses! </strong> <?=$data;?></div>
                    <?php } ?>
                    
                    <?php 
                    $data2=$this->session->flashdata('error');
                    if($data2!=""){ ?>
                        <div id="notifikasi" class="alert alert-danger"><strong> Error! </strong> <?=$data2;?></div>
                    <?php } ?>
                <div id="toolbar">
                    <a href="<?= site_url("Master_data/user/tambahUser") ?>" class="btn btn-success">
                        <span>Tambah User</span>
                    </a>
                </div>
                <table 
                class="table border-0 mt-4 mb-4" 
                id="tableUser" 
                data-toggle="table" 
                data-search="true"
                data-show-refresh="true" 
                data-pagination="true" 
                data-page-size="5" 
                data-page-list="[5,10, All]"
                data-show-toggle="true" 
                data-side-pagination="server" 
                data-resizable="false" 
                data-show-export="true"
                data-export-types="['json', 'xml', 'csv', 'txt', 'sql', 'excel', 'pdf']" 
                data-export-options='{"fileName": "Data_User"}'
                data-click-to-select="true" 
                data-toolbar="#toolbar" 
                data-show-columns="true"
                data-show-pagination-switch="true" 
                data-url="<?= site_url("Master_data/user/dataUser") ?>">
                    <thead>
                        <tr>
                            <th data-formatter="numberFormatter">No</th>
                            <th data-field="NAMA" data-sortable="true">Nama User</th>
                            <th data-field="NAMA_POSISI" data-sortable="true">Nama Posisi</th>
                            <th data-field="ALAMAT" data-sortable="true">Alamat</th>
                            <th data-field="EMAIL" data-sortable="true">Email</th>
                            <th data-field="NO_TELP" data-sortable="true">Nomor Telepon</th>
                            <th data-field="STATUS" data-sortable="true">Status</th>
                            <th data-formatter="actionFormatter" data-field="ID" data-force-hide="true" class="text-center" data-width="200">Aksi</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </section>
</main>

<script>
    function numberFormatter(value, row, index) {
        var options = $('#tableUser').bootstrapTable('getOptions')
        var tes = 0
        if (!isNaN(options['pageSize'])) { //cek pagesize angka atau tidak, klo angka jalankan kode dibawah
            tes = ((options["pageNumber"] - 1) * options["pageSize"])
        }
        return index + 1 + tes;
    }

    function actionFormatter(value, row, index) {
        return [
            '<a href="<?= site_url("Master_data/user/editUser/") ?>' + value + '" class="badge badge-warning"  ><i class="fa fa-pencil-square-o" title="Edit Data"></i></a>',
            ' ',
            '<a href="<?= site_url("Master_data/user/aktifstatus/") ?>' + value + '" class="badge badge-danger"   title="Aktif / Nonaktif Data"><i class="fa fa-check" aria-hidden="true"></i></a>'
        ].join('');
    }
</script>