<main class="mt-5 mb-5">
    <section class="container">
        <div class="content-menu">
            <div class="card-body">
                <form action="<?php echo site_url('Master_data/user/submitTambahUser');?>" method="POST" enctype="multipart/form-data">
                <div class="form-row">
                <div class="form-row">
                    <div class="col-md-6">
                        <label for="kode">Email</label>
                        <input type="text" class="form-control no-border" name="email" id="kode" placeholder="Masukkan Email" value="<?php echo set_value("email")?>">
                        <?php echo form_error('email', "<span class='text-danger'>", "</span>"); ?>
                    </div>
                    <div class="col-md-6">
                        <label for="nama">Nama</label>
                        <input type="text" class="form-control no-border" name="nama" id="kode" placeholder="Masukkan Nama" value="<?php echo set_value("nama")?>">
                        <?php echo form_error('nama', "<span class='text-danger'>", "</span>"); ?>
                    </div>
                    <div class="col-md-12 mt-2">
                        <label for="nama">Alamat</label>
                        <input type="text" class="form-control no-border" name="alamat" id="kode" placeholder="Masukkan Alamat" value="<?php echo set_value("alamat")?>">
                        <?php echo form_error('alamat', "<span class='text-danger'>", "</span>"); ?>
                    </div>
                    <div class="col-md-6 mt-2">
                        <label for="nama">No Telepon</label>
                        <input type="number" class="form-control no-border" name="no_telp" id="kode" placeholder="Masukkan Nomor Telepon" value="<?php echo set_value("no_telp")?>">
                        <?php echo form_error('no_telp', "<span class='text-danger'>", "</span>"); ?>
                    </div>
                    <div class="col-md-6 mt-2">
                        <label class="mt-lg-0 mt-4">Nama Posisi</label>
                        <select name="posisi" class="form-control js-example-basic-single">
                            <option value="" disabled="disabled" selected>Pilih Posisi</option>
                            <?php foreach($dataPosisi as $Posisi):?>
                                <option value="<?php echo $Posisi['ID']?>"
                                <?php if ($Posisi['ID'] == set_value("posisi")) {
                                        echo "selected";
                                    } ?>>
                                    <?php echo $Posisi['NAMA_POSISI']?>
                                </option>
                            <?php endforeach; ?>
                        </select> 
                        <?php echo form_error('barang', "<span class='text-danger'>", "</span>"); ?>
                    </div>
                    <div class="col-md-6 mt-2">
                        <label for="nama">Password</label>
                        <input type="password" class="form-control no-border" name="password" id="kode" placeholder="Masukkan Password" value="<?php echo set_value("password")?>">
                        <?php echo form_error('password', "<span class='text-danger'>", "</span>"); ?>
                    </div>
                    <div class="col-md-6 mt-2">
                        <label for="foto">Ubah Foto Profile</label>
                        <small class="text-danger">*MAX(2MB)</small>
                        <input type="file" class="form-control no-border" accept=".jpeg,.jpg,.png" name="foto" placeholder="Masukkan Gambar" value="<?php echo set_value("foto")?>">
                        <?php echo form_error('foto', "<span class='text-danger'>", "</span>"); ?>
                    </div>
                    </div>
                    <div class="action clearfix mt-5">
                        <button class="btn btn-success float-right ml-3">Simpan</button>
                        <a href="<?php echo site_url('Master_data/user')?>" class="btn btn-primary float-right">Kembali</a>
                    </div>
                </form>
            </div>
        </div>
    </section>
</main>
