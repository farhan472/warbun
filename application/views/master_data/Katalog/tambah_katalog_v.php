<main class="mt-5 mb-5">
    <section class="container">
        <div class="content-menu">
            <div class="card-body">
                <form action="<?php echo site_url('Master_data/katalog/submitTambahKatalog');?>" method="POST" enctype="multipart/form-data">
                <div class="form-row">
                    <div class="col-md-6">
                        <label class="mt-lg-0 mt-4">Nama Barang</label>
                        <select name="barang" class="form-control js-example-basic-single">
                            <option value="" disabled="disabled" selected>Pilih Barang</option>
                            <?php foreach($dataBarang as $Barang):?>
                                <option value="<?php echo $Barang['ID']?>"
                                <?php if ($Barang['ID'] == set_value("barang")) {
                                        echo "selected";
                                    } ?>>
                                    <?php echo $Barang['NAMA']?>
                                </option>
                            <?php endforeach; ?>
                        </select> 
                        <?php echo form_error('barang', "<span class='text-danger'>", "</span>"); ?>
                    </div>
                    <div class="col-md-6">
                        <label for="kode">Kode Katalog</label>
                        <select name="kode" class="form-control js-example-basic-single">
                            <option value="" disabled="disabled" selected>Pilih Kode</option>
                            <?php foreach($dataKode as $kode):?>
                                <option value="<?php echo $kode['ID']?>"
                                <?php if ($kode['ID'] == set_value("kode")) {
                                        echo "selected";
                                    } ?>>
                                    <?php echo $kode['KODE']?>
                                </option>
                            <?php endforeach; ?>
                        </select> 
                        <?php echo form_error('kode', "<span class='text-danger'>", "</span>"); ?>
                    </div>
                    </div>
                    <div class="action clearfix mt-5">
                        <button class="btn btn-success float-right ml-3">Simpan</button>
                        <a href="<?php echo site_url('Master_data/katalog')?>" class="btn btn-primary float-right">Kembali</a>
                    </div>
                </form>
            </div>
        </div>
    </section>
</main>
