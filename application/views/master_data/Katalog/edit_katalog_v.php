<main class="mt-5 mb-5">
    <section class="container">
        <div class="content-menu">
            <div class="card-body">
                <form action="<?php echo site_url('Master_data/katalog/submitEditKatalog/'.$id);?>" method="POST" enctype="multipart/form-data">
                <div class="form-row">
                <div class="col-md-6">
                        <label class="mt-lg-0 mt-4">Barang</label>
                        <select name="barang" class="form-control js-example-basic-single">
                            <option value="" disabled="disabled" selected>Pilih Barang</option>
                            <?php foreach($dataBarang as $Barang):?>
                                <option value="<?php echo $Barang['ID']?>"
                                <?php if ($Barang['ID'] == $data['ID_BARANG']) {
                                        echo "selected";
                                    } ?>>
                                    <?php echo $Barang['NAMA']?>
                                </option>
                            <?php endforeach; ?>
                            </select> 
                        <?php echo form_error('barang', "<span class='text-danger'>", "</span>"); ?>
                    </div>
                    <div class="col-md-6">
                        <label for="kode">Kode Katalog</label>
                        <select name="kode" class="form-control js-example-basic-single">
                            <option value="" disabled="disabled" selected>Pilih Kode</option>
                            <?php foreach($dataKode as $kode):?>
                                <option value="<?php echo $kode['ID']?>"
                                <?php if ($kode['ID'] == $data['KODE']) {
                                        echo "selected";
                                    } ?>>
                                    <?php echo $kode['KODE']?>
                                </option>
                            <?php endforeach; ?>
                        </select> 
                        <?php echo form_error('kode', "<span class='text-danger'>", "</span>"); ?>
                    </div>
                    <div class="col-md-12 mt-3">
                        <span>Status</span>
                    <?php foreach($status as $st):?>
                        <div class="form-check form-check-radio form-check-inline">
                            <label class="form-check-label">
                                <input class="form-check-input" name="status" type="radio" value="<?php echo $st['KODE']?>"
                                    <?php echo ($data["STATUS"] == $st['KODE']? ' checked' : ''); ?>><?php echo $st['NAMA_STATUS']?>
                                <span class="form-check-sign"></span>
                            </label>
                        </div>
                    <?php endforeach ?>
                    <?= form_error('status', '<small class="text-danger">', '</small>') ?> 
                    </div>
                    <div class="action clearfix mt-5">
                        <button class="btn btn-success float-right ml-3">Simpan</button>
                        <a href="<?php echo site_url('Master_data/katalog')?>" class="btn btn-primary float-right">Kembali</a>
                    </div>
                </form>
            </div>
        </div>
    </section>
</main>
