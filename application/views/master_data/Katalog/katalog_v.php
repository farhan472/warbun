<main class="mt-5 mb-5">
    <section class="container">
        <div class="content-menu">
            <div class="card-body">
                <h5 class="card-title">Katalog</h5>
                <hr>
                <div id="message">
                    
                </div>

                <div id="toolbar"> 
                    <a href="#" class="btn btn-sm btn-success" data-toggle="modal" data-target="#Modal" id="item_tambah"><i class="fa fa-plus"></i></a>
                </div>
                <table 
                class="table border-0 mt-4 mb-4" 
                id="tableKatalog" 
                <?php echo DEFAULT_BOOTSTRAP_TABLE_CONFIG ?>
                data-export-options='{"fileName": "Data_Katalog"}',
                data-url="<?= site_url("Master_data/katalog/dataKatalog") ?>">
                    <thead>
                        <tr>
                            <th data-formatter="numberFormatter">No</th>
                            <th data-field="NAMA" data-formatter="linkFormatter" data-sortable="true">Nama Barang</th>
                            <th data-field="KODE" data-sortable="true">Kode Katalog</th>
                            <th data-field="STATUS" data-sortable="true">Status</th>
                            <th data-formatter="actionFormatter" data-field="ID" data-force-hide="true" class="text-center" data-width="200">Aksi</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
        <!-- MODAL -->
        <div class="modal fade" id="Modal" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Add -->
                    <div id="contentTambah" style="display:none">
                        <div class="modal-header">
                            <h3 class="modal-title" id="myModalLabel">Tambah Data Katalog</h3>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        </div>
                        <form class="form-horizontal" id="formTambah">
                            <div class="modal-body">
                                <div class="alert alert-danger print-error-msg" style="display:none"></div>
                                <div class="form-group">
                                    <label class="control-label col-xs-3" >Nama Barang</label>
                                    <div class="col-xs-9">
                                        <select name="tambah_barang" id="tambah_barang" style="width:335px;" class="form-control" >
                                            <option value="" disabled selected>Pilih</option>
                                        </select>
                                    </div>
                                    <label class="control-label col-xs-3" >Nama Katalog</label>
                                    <div class="col-xs-9">
                                        <select name="tambah_kodeKatalog" id="tambah_kodeKatalog" style="width:335px;" class="form-control" >
                                            <option value="" disabled selected>Pilih</option>
                                        </select>
                                    </div>
                                </div>
            
                            </div>
            
                            <div class="modal-footer">
                                <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
                                <button class="btn btn-info" id="btn_tambah">Simpan</button>
                            </div>
                        </form>
                    </div>

                    <!-- Edit -->
                    <div id="contentUbah" style="display:none">
                        <div class="modal-header">
                            <h3 class="modal-title" id="myModalLabel">Ubah Data Katalog</h3>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        </div>
                        <form class="form-horizontal" id="formEdit">
                            <div class="modal-body">
                                <div class="alert alert-danger print-error-msg" style="display:none"></div>
                            <div class="form-group">
                            <label class="control-label col-xs-3" >Nama Barang</label>
                                    <div class="col-xs-9">
                                        <select name="ubah_barang" id="ubah_barang" style="width:335px;" class="form-control" >
                                            <option value="" disabled selected>Pilih</option>
                                        </select>
                                    </div>
                                    <label class="control-label col-xs-3" >Nama Katalog</label>
                                    <div class="col-xs-9">
                                        <select name="ubah_kodeKatalog" id="ubah_kodeKatalog" style="width:335px;" class="form-control" >
                                            <option value="" disabled selected>Pilih</option>
                                        </select>
                                    </div>
                                    <label class="control-label col-xs-3" >Status Katalog</label>
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input" name="ubah_status" id="ubah_status" value="1">
                                        <label class="custom-control-label" for="ubah_status"></label>
                                    </div>
                                        <input type="hidden" name="ubah_id" id="ubah_id" value="">
                                </div>
            
                            </div>
                            <div class="modal-footer">
                                <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
                                <button class="btn btn-info" id="btn_update">Ubah</button>
                            </div>
                        </form>
                    </div>

                    <!-- Hapus -->
                    <div id="contentHapus" style="display:none">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel">Hapus Barang</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
                        </div>
                        <form class="form-horizontal" id="formHapus">
                            <div class="modal-body">
                                                
                                    <input type="hidden" name="hapus_id" id="hapus_id" value="">
                                    <div class="alert alert-warning"><p>Apakah Anda yakin mau menghapus barang ini?</p></div>
                                                
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                <button class="btn_hapus btn btn-danger" id="btn_hapus">Hapus</button>
                            </div>
                        </form>
                    </div>

                    <!-- Info Barang -->
                    <div id="contentBarang" style="display:none">
                        <div class="modal-header">
                            <h3 class="modal-title" id="myModalLabel">Data Barang</h3>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        </div>
                            <div class="modal-body">
                                <table class="table">
                                    <tr>
                                        <td>Nama</td>
                                        <td id="info_nama"></td>
                                    </tr>
                                    <tr>
                                        <td>Kode</td>
                                        <td id="info_kode"></td>
                                    </tr>
                                    <tr>
                                        <td>Deskripsi</td>
                                        <td id="info_deskripsi"></td>
                                    </tr>
                                    <tr>
                                        <td>Jumlah Barang</td>
                                        <td id="info_jumlah"></td>
                                    </tr>
                                    <tr>
                                        <td>Harga Satuan</td>
                                        <td id="info_harga"></td>
                                    </tr>
                                </table>
                            </div>
                            <div class="modal-footer">
                                <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
                            </div>
                    </div>
                </div>
            </div>
        </div>
        <!--END MODAL-->
    </section>
</main>

<script>
    function numberFormatter(value, row, index) {
        var options = $('#tableKatalog').bootstrapTable('getOptions')
        var tes = 0
        if (!isNaN(options['pageSize'])) { //cek pagesize angka atau tidak, klo angka jalankan kode dibawah
            tes = ((options["pageNumber"] - 1) * options["pageSize"])
        }
        return index + 1 + tes;
    }

    function actionFormatter(value, row, index) {
        return [
            `<a href="#" class="badge badge-primary item_edit" data-toggle="modal" data-target="#Modal" data="${value}"><i class="fa fa-pencil-square-o" title="Edit Data"></i></a>

            <a href="#" class="badge badge-warning" id="btn-aktif" data="${value}" ><i class="fa fa-check" aria-hidden="true" title="Aktif / Nonaktif Data"></i></a>
            
            <a href="#" class="badge badge-danger item_hapus" data-toggle="modal" data-target="#Modal" data="${value}"><i class="fa fa-trash" title="Hapus Data"></i></a>`
        ];
    }

    function linkFormatter(value, row, index) {
        return `<a href='#' class='link_barang' data='${row.ID}' data-toggle='modal' data-target='#Modal'>${value}</a>`;
    }

    function tambahData(){
        let barang = $('#tambah_barang').val();
        let kodeKatalog = $('#tambah_kodeKatalog').val();
        $.ajax({
            type : "POST",
            url  : "<?php echo site_url('master_data/Katalog/add'); ?>",
            dataType: "json",
            data : {kode : kodeKatalog , barang : barang },
            success: function(data){
                if($.isEmptyObject(data.error)){
                    $('[name="tambah_barang"]').val("");
                    $('[name="tambah_kodeKatalog"]').val("");
                    $('#Modal').modal('hide');
                    $('#tableKatalog').bootstrapTable('refresh');
                    
                    message("sukses",data.sukses);
                    $(".print-error-msg").css('display','none');

                }else{
                    $(".print-error-msg").css('display','block');
                    $(".print-error-msg").html(data.error);
                }
            },
            error : function(){
                $('#Modal').modal('hide');
                message("gagal","Proses Gagal");
            }
        });
        return false;
    }

    function updateData(){
        let barang = $('#ubah_barang').val();
        let kodeKatalog = $('#ubah_kodeKatalog').val();
        let id = $('#ubah_id').val();
        if ($('#ubah_status').prop('checked')) {
            var status = '1';
        }else {
            var status = '0';
        }
            $.ajax({
                type : "POST",
                url  : "<?php echo site_url('master_data/Katalog/edit'); ?>",
                dataType: "json",
                data : {id:id , kode : kodeKatalog , barang : barang, status:status},
                success: function(data){
                    if($.isEmptyObject(data.error)){
                    $('#ubah_barang').val("");
                    $('#ubah_kodeKatalog').val("");
                    $('#ubah_id').val("");
                    $('#Modal').modal('hide');
                    $('#tableKatalog').bootstrapTable('refresh');

                    message("sukses",data.sukses);
                    $(".print-error-msg").css('display','none');

                    }else{
                        $(".print-error-msg").css('display','block');
                        $(".print-error-msg").html(data.error);
                    }
                },
                error : function(){
                    $('#Modal').modal('hide');
                    message("gagal","Proses Gagal");
                }
            });
            return false;
    }

    function hapusData(){
        var id =$('#hapus_id').val();
        $.ajax({
        type : "POST",
        url  : "<?php echo site_url('master_data/Katalog/hapus/'); ?>",
        dataType: "json",
        data : {id: id},
                success: function(data){
                    $('#Modal').modal('hide');
                    $('#tableKatalog').bootstrapTable('refresh');
                    if($.isEmptyObject(data.error)){
                        message("sukses",data.sukses);
                    }else{
                        message("gagal",data.error);
                    }
                },
                error : function(){
                    $('#Modal').modal('hide');
                    message("gagal","Proses Gagal");
                }
            });
            return false;
    }

    function aktifStatus(idData){
        let id = idData ;
        $.ajax({
                type: 'post',
                url: '<?php echo site_url('master_data/Katalog/aktifStatus/'); ?>',
                dataType: "json",
                data: {id : id},
                success: function (data) {
                    $('#tableKatalog').bootstrapTable('refresh');
                    message("sukses",data.sukses);
                },
                error : function(){
                    message("gagal","Proses Gagal");
                }
            });
    }


</script>
<script>
$(document).ready(function(){

// Get Tambah 
    $('#toolbar').on('click','#item_tambah',function(){
        $.ajax({
            type : "GET",
            url  : "<?php echo site_url('master_data/Katalog/getDataBarangKode/'); ?>",
            dataType : "JSON",
            // data : {id:id},
            success: function(data){
                    $('#contentHapus').hide();
                    $('#contentUbah').hide();
                    $('#contentBarang').hide();

                    $('#Modal').modal('show');
                    $('#contentTambah').show();
                    
                    $('#tambah_barang').empty();
                    for(let i = 0;i+1 <= data.dataBarang.length ; i++){
                        $('#tambah_barang').append(`<option value="${data.dataBarang[i].ID}" >${data.dataBarang[i].NAMA}</option>`);
                    }
                    
                    $('#tambah_kodeKatalog').empty();
                    for(let i = 0;i+1 <= data.dataKodeKatalog.length ; i++){
                        $('#tambah_kodeKatalog').append(`<option value="${data.dataKodeKatalog[i].ID}" >${data.dataKodeKatalog[i].KODE}</option>`);
                    }
            }
        });
        return false;
        
    });

// Action Tambah 
    $('#contentTambah').on('submit','#formTambah',function(e){
        e.preventDefault();
        tambahData();
    });

// Get Hapus

    $('#tableKatalog').on('click','.item_hapus',function(){
        var id=$(this).attr('data');
        $('#contentTambah').hide();
        $('#contentUbah').hide();
        $('#contentBarang').hide();

        $('#Modal').modal('show');
        $('#contentHapus').show();

        $('[name="hapus_id"]').val(id);
    });

// Action Hapus

    $('#contentHapus').on('click','#btn_hapus',function(e){
        e.preventDefault();
        hapusData();        
    });

// Get Ubah
    $('#tableKatalog').on('click','.item_edit',function(){
        var id=$(this).attr('data');
        $.ajax({
            type : "GET",
            url  : "<?php echo site_url('master_data/Katalog/getDataBarangKodeKatalog/'); ?>" + id,
            dataType : "JSON",
            data : {id:id},
            success: function(data){
                    $('#contentTambah').hide();
                    $('#contentHapus').hide();
                    $('#contentBarang').hide();
                    
                    $('#Modal').modal('show');
                    $('#contentUbah').show();
                    
                    $('#ubah_barang').empty();
                    for(let i = 0;i+1 <= data.dataBarang.length ; i++){
                        if (data.dataBarang[i].ID == data.dataKatalog[0].ID_BARANG) {
                            $('#ubah_barang').append(`<option value="${data.dataBarang[i].ID}" selected>${data.dataBarang[i].NAMA}</option>`);
                        }else{
                            $('#ubah_barang').append(`<option value="${data.dataBarang[i].ID}" >${data.dataBarang[i].NAMA}</option>`);
                        }
                    }
                    
                    $('#ubah_kodeKatalog').empty();
                    for(let i = 0;i+1 <= data.dataKodeKatalog.length ; i++){
                        if (data.dataKodeKatalog[i].ID == data.dataKatalog[0].KODE) {
                            $('#ubah_kodeKatalog').append(`<option value="${data.dataKodeKatalog[i].ID}" selected>${data.dataKodeKatalog[i].KODE}</option>`);   
                        }else{
                            $('#ubah_kodeKatalog').append(`<option value="${data.dataKodeKatalog[i].ID}" >${data.dataKodeKatalog[i].KODE}</option>`);   
                        }
                    }
                    for(let i = 0 ;i < data.status.length ; i++){
                        if (data.dataKatalog[0].STATUS == '1') {
                            $('[name="ubah_status"]').prop("checked", true);
                        }else{
                            $('[name="ubah_status"]').prop("checked", false);
                        }
                    }
                    $('[name="ubah_id"]').val(id);
            }
        });
        return false;
    });

// Action Ubah 

    $('#contentUbah').on('click','#btn_update',function(e){
        e.preventDefault();
        updateData();
    });

// Action Aktif / Nonaktif 
    $('#tableKatalog').on('click', '#btn-aktif', function(){
        let id = $(this).attr('data');
        aktifStatus(id);
    });



//get Barang 
    $('#tableKatalog').on('click','.link_barang',function(){
        var id = $(this).attr('data');
        $.ajax({
            type : "GET",
            url  : "<?php echo site_url('master_data/Katalog/getDataBarang/'); ?>" + id,
            dataType : "JSON",
            data : {id:id},
            success: function(data){
                    console.log(data.dataBarang[0]);
                    $('#contentTambah').hide();
                    $('#contentUbah').hide();
                    $('#contentHapus').hide();
                    
                    $('#Modal').modal('show');
                    $('#contentBarang').show();

                    $('#info_nama').text(data.dataBarang[0].NAMA);
                    $('#info_kode').text(data.dataBarang[0].KODE);
                    $('#info_deskripsi').text(data.dataBarang[0].DESKRIPSI);
                    $('#info_jumlah').text(data.dataBarang[0].JUMLAH_BARANG);
                    $('#info_harga').text(data.dataBarang[0].HARGA_SATUAN);
            }
        });
        return false;
    });
});
</script>