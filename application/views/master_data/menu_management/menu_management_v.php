<main>
	<section class="container mb-5">
		<div class="content-menu">
			<div class="card-body">
				<?php echo $this->session->flashdata('status'); ?>
				<h3 class="border-bottom">Hirarki Menu</h3>
				<form action="<?php echo site_url('Master_data/menu_management/submitMenu')?>" method="POST" id="myForm" enctype="multipart/form-data">
					<div class="form-row">
						<div class="col-md-12">
						<label>Nama Posisi</label>  
							<select name="ID" id="menu_management" class="form-control js-example-basic-single" onchange="selectChange(this.value)">
							<option value="" disabled="disabled" selected>Pilih Posisi</option>
								<?php foreach($dataPosisi as $posisi):?>
											<option value="<?php echo $posisi['ID']?>" >
												<?php echo $posisi['NAMA_POSISI']?>
										</option>
								<?php endforeach; ?>
							</select> 
							<?php echo form_error('job_title', '<span class="text-danger">', "</span>"); ?>
						</div>
					</div>
				</form>
			</div>
		</div>
	</section>
</main>
<script>
function selectChange(val) {
    $('#myForm').submit();
}

// $(document).ready(function() {
//          $(".js-example-basic-single").select2({
//             width: 'resolve'
//          });
//       });
//          $(document).ready(function() {  
//          $(".js-example-hide-search").select2({
//             width: 'resolve',
//             minimumResultsForSearch: -1
//          });
//       });
</script>