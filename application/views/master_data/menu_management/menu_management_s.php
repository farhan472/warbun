<main class="mb-5">
	<section class="container">
		<div class="content-menu">
			<div class="card-body">
				<?php echo $this->session->flashdata('status'); ?>
				<div class="form-row">
					<div class="col justify-content-center">
						<h3 style="margin:0px;padding: 10px 0px;">Hirarki Menu</h3>
					</div>
				</div>
				<form action="<?php echo site_url('Master_data/menu_management/submitHirarkiMenu')?>" method="POST" id="myForm" enctype="multipart/form-data">
						<div class="form-row justify-content-center">
							<div class="col-md-11">
								<label>Nama Job Title</label>  
										<select name="ID" id="menu_management" class="form-control js-example-hide-search" disabled="disabled" onchange="selectChange(this.value)">
										<option value="" selected>Pilih Job Title</option>
											<?php foreach($dataPosisi as $Posisi):?>
												<option value="<?php echo $Posisi['ID']?>" 
													<?php if($id == $Posisi['ID'])echo 'selected' ?>>
													<?php echo $Posisi['NAMA_POSISI']?>
												</option>
									<?php endforeach; ?>
								</select> 
								<?php echo form_error('job_title', '<span class="text-danger">', "</span>"); ?>
							</div>
							<div class="col-md-1">
							<a class="btn simpan btn-xs" role="button" style="margin-top: 30px; background-color:#005db5; border-radius:20px; color: white;">
								<!-- <i class="fas fa-lg fa-sync-alt" style="color: white;"></i> -->
								Reset
							</a>
							</div>
						</div>
						<!-- input hidden jstree (array) lalu foreach di controller masukkan ke db -->
						
						<!-- how to get checked data from jstree -->
						<div class="form-group">
							

							<div id="jstree"></div>
							<input type="hidden" name="idshow" id="idshow">
							<input type="hidden" name="id" value="<?php echo $id ?>">
							
							<div class="action clearfix mt-5">
								<div class="row">
									<div class="col">
										<button class="btn btn-success float-right btnGetCheckedItem" type="submit">Simpan</button>
										<a href="<?php echo site_url('Master_data/menu_management')?>" class="btn btn-primary float-right">Kembali</a>
									</div>
								</div>
							</div>
						</div>
				</form>
			</div>
		</div>
	</section>
</main>
<script>
   $(document).ready(function(){
	   var id = "<?php echo isset($id) ? $id : 0 ?>";
         $("#jstree")
	  // call `.jstree` with the options object
	  
      .jstree({
		  'plugins' : ["contextmenu","checkbox","noclose"],
		  'core' : {
            "themes":{
					"check_callback": true,
					'name': 'proton',
            	'responsive': true,
               "icons":false} ,
            'data' : {
               "url" : "<?php echo base_url();?>index.php/Master_data/Menu_management/getMenu/"+id,
               "plugins" : ["noclose"],
			   "dataType" : "json",
			   "get_selected" : true
            }
         },
		  "checkbox" : {
			"three_state": false,
			// "two_state": false,
			"keep_selected_style" : false,
		}
      	
      })
      // .jstree(true).settings.core.data = newData;
      // .jstree(true).refresh();
      .on("changed.jstree", function (e, data) {
         id = data.selected
      })

      .bind("ready.jstree", function (event, data) {
         // you get two params - event & data - check the core docs for a detailed description
         // data.inst.change_state('li[selected=selected]', false);
			$(this).jstree("open_all");
	  })

	  // function checkChildrenStatus adalah function recursive
	  function checkChildrenStatus(data, childnode, ischeck = true){ 
		//   melakukan foreach untuk check nodenya apakah node punya child atau tidak
		
			childnode.forEach(node => {
				if(ischeck){
					data.instance.check_node(node);
				} else {
					data.instance.uncheck_node(node);
				}

		// cek apakah node tsb memiliki children, jika ada, maka di recursive(jalankan ulang) dengan menggunakan child tersebut untuk pengulangannya(foreach)
				if(data.instance.get_node(node).children){
					checkChildrenStatus(data, data.instance.get_node(node).children, ischeck)
				}
			});
	  }

	  $('#jstree').on('activate_node.jstree', function (e, data) {
		// cek apakah node tidak diselect,
		if (!data.node.state.selected){
			// jika tidak diselect, maka akan cek apakah node tersebut parent?
			if (data.instance.is_parent(data.node)) {
				// jika benar parent, maka mengambil isi childrennya dan menjalankan function checkChildrenStatus (recursive)
				var childnode = data.node.children;
				checkChildrenStatus(data, childnode, false);
			}
			
			// jika node diselect
		} else { 
			// cek apakah node tersebut children?
			if (data.instance.is_leaf(data.node)) {
				// jika children, maka ambil parentnya
				var parentnode = data.node.parent;
				// lakukan ceklist node parent
				do {
					data.instance.check_node(parentnode);
					parentnode = data.instance.get_node(parentnode).parent;
					// selama node tersebut masih memiliki parent
				} while (parentnode)

				//  jika parent
			} else {
				// maka ambil childrennya
				var childnode = data.node.children;
				// lalu lempar children tersebut dan jalankan function checkChildrenStatus (recursive)
				checkChildrenStatus(data, childnode);
			}
		}

    })


		$('a.btn.simpan.btn-xs').on('click',function(e){
			var id = "<?php echo isset($id) ? $id : 0 ?>";
			e.preventDefault();
			// $('#jstree').jstree('deselect_all');
			$('#jstree').jstree("destroy").empty();
			$('#jstree').jstree
                ({
						'plugins' : ["contextmenu","checkbox","noclose"],
						"checkbox" : {
							"three_state": false,
							"keep_selected_style" : false},
                  'core' : {
							"themes":{
								'name': 'proton',
            				'responsive': true,
               			"icons":false} ,
                     'data': {
								"url" 			: "<?php echo base_url();?>index.php/Master_data/Menu_management/getMenu/"+id,
            				"plugins" 		: ["noclose"],
			   				"dataType" 		: "json",
			   				"get_selected" : true
								}
                     }
                })

					 .bind("ready.jstree", function (event, data) {
         // you get two params - event & data - check the core docs for a detailed description
         $(this).jstree("open_all");
					 })		
		})

   })

</script>
<!-- <script>
    $(document).ready(function() {
    $(".js-example-basic-single").select2({
        width: 'resolve'
    });
});
    $(document).ready(function() {  
    $(".js-example-hide-search").select2({
        width: 'resolve',
        minimumResultsForSearch: -1
    });
});
</script> -->
<script type="text/javascript">

			$('.btnGetCheckedItem').click(function(){
			var checked_ids = []; 
			var selectedNodes = $('#jstree').jstree("get_selected", true);
			$.each(selectedNodes, function() {
				checked_ids.push(this.id);
			});
			// You can assign checked_ids to a hidden field of a form before submitting to the server
			$('#idshow').val(checked_ids);
		});
        
    </script>