<main class="mt-5 mb-5">
    <section class="container">
        <div class="content-menu">
            <div class="card-body">
                <h5 class="card-title">Tambah Barang</h5>
                <hr>
                <a class="btn btn-primary btn-sm text-white">Barang</a>
                <a href="<?= site_url('Master_data/barang/tambahGambar/'.$id.'/tambah')?>" class="btn btn-secondary btn-sm text-white">Gambar Barang</a>
                <hr>
                <form action="<?php echo site_url('Master_data/barang/submitTambahEditBarang/'.$id);?>" method="POST" enctype="multipart/form-data">
                <div class="form-row">
                <div class="col-md-4">
                        <label class="mt-lg-0 mt-4">Brand</label>
                        <select name="brand" class="form-control js-example-basic-single">
                            <option value="" disabled="disabled" selected>Pilih Brand</option>
                            <?php foreach($dataBrand as $brand):?>
                                <option value="<?php echo $brand['ID_BRAND']?>"
                                <?php if ($brand['ID'] == $data['ID_BRAND']) {
                                        echo "selected";
                                    } ?>>
                                    <?php echo $brand['NAMA_BRAND']?>
                                </option>
                            <?php endforeach; ?>
                            </select> 
                        <?php echo form_error('divisi_departemen', "<span class='text-danger'>", "</span>"); ?>
                    </div>
                    <div class="col-md-4">
                        <label for="kode">Kode Barang</label>
                        <input type="text" class="form-control no-border" name="kode" id="kode" placeholder="Masukkan Kode Barang" value="<?php echo $data['KODE']?>">
                        <?php echo form_error('kode', "<span class='text-danger'>", "</span>"); ?>
                    </div>
                    <div class="col-md-4">
                        <label for="nama">Harga Satuan</label>
                        <input type="numeric" class="form-control no-border" name="harga" id="nama" placeholder="Masukkan Harga Satuan" value="<?php echo $data['HARGA_SATUAN']?>">
                        <?php echo form_error('harga', "<span class='text-danger'>", "</span>"); ?>
                    </div>
                    <div class="col-md-6">
                        <label for="nama">Nama Barang</label>
                        <input type="text" class="form-control no-border" name="nama" id="nama" placeholder="Masukkan Nama Barang" value="<?php echo $data['NAMA']?>">
                        <?php echo form_error('nama', "<span class='text-danger'>", "</span>"); ?>
                    </div>
                    <div class="col-md-6">
                        <label for="nama">Jumlah Barang</label>
                        <input type="numeric" class="form-control no-border" name="jumlah" id="nama" placeholder="Masukkan Jumlah Barang" value="<?php echo $data['HARGA_SATUAN']?>">
                        <?php echo form_error('jumlah', "<span class='text-danger'>", "</span>"); ?>
                    </div>
                    <div class="col-md-12">
                        <label for="nama">Deskripsi Barang</label>
                        <input type="text" class="form-control no-border" name="deskripsi" id="nama"  placeholder="Masukkan Deskripsi Barang" onKeyPress="if(this.value.length==255) return false;" onkeydown="return event.keyCode !== 69 && event.keyCode !== 187 && event.keyCode !== 188 && event.keyCode !== 189 && event.keyCode !== 190" value="<?php echo $data['DESKRIPSI']?>">
                        <?php echo form_error('deskripsi', "<span class='text-danger'>", "</span>"); ?>
                    </div>
                    </div>
                    <div class="action clearfix mt-5">
                        <button class="btn btn-success float-right ml-3">Simpan</button>
                </form>
                <form action="<?php echo site_url('Master_data/barang/hapusDataBarang/'.$id)?>" method="post" enctype="multipart/form-data">
                    <?php foreach ($gambar as $g) :?>
                        <input type="hidden" value="<?= $g['FILE_NAME']; ?>" name="gambar[]">
                    <?php endforeach ?>
                    <button class="btn batal float-left">Batal</button>
                </form>
                    </div>
            </div>
        </div>
    </section>
</main>
