<main class="mt-5 mb-5">
    <section class="container">
        <div class="content-menu">
                <h5 class="card-title">Barang</h5>
                <hr>
                <?php echo $this->session->flashdata('status'); ?>
                    <?php 
                    $data=$this->session->flashdata('sukses');
                    if($data!=""){ ?>
                        <div id="notifikasi" class="alert alert-success"><strong>Sukses! </strong> <?=$data;?></div>
                    <?php } ?>
                    
                    <?php 
                    $data2=$this->session->flashdata('error');
                    if($data2!=""){ ?>
                        <div id="notifikasi" class="alert alert-danger"><strong> Error! </strong> <?=$data2;?></div>
                    <?php } ?>
                <div id="toolbar">
                    <a href="<?= site_url("Master_data/barang/tambahBarang") ?>" class="btn btn-success">
                        <span>Tambah Barang</span>
                    </a>
                </div>
                <table 
                class="table border-0 mt-4 mb-4" 
                id="tableBarang" 
                data-toggle="table" 
                data-search="true"
                data-show-refresh="true" 
                data-pagination="true" 
                data-page-size="5" 
                data-page-list="[5,10, All]"
                data-show-toggle="true" 
                data-side-pagination="server" 
                data-resizable="false" 
                data-show-export="true"
                data-export-types="['json', 'xml', 'csv', 'txt', 'sql', 'excel', 'pdf']" 
                data-export-options='{"fileName": "Data_Barang"}'
                data-click-to-select="true" 
                data-toolbar="#toolbar" 
                data-show-columns="true"
                data-show-pagination-switch="true" 
                data-url="<?= site_url("Master_data/barang/dataBarang") ?>">
                    <thead>
                        <tr>
                            <th data-formatter="numberFormatter">No</th>
                            <th data-field="NAMA_BRAND" data-sortable="true">Brand</th>
                            <th data-field="KODE" data-sortable="true">Kode Barang</th>
                            <th data-field="NAMA" data-sortable="true">Nama Barang</th>
                            <th data-field="DESKRIPSI" data-sortable="true">Deskripsi Barang</th>
                            <th data-field="HARGA_SATUAN" data-sortable="true">Harga Satuan</th>
                            <th data-field="JUMLAH_BARANG" data-sortable="true">Jumlah Barang</th>
                            <th data-field="STATUS" data-sortable="true">Status</th>
                            <!-- <th data-field="CREATED_DATE" data-sortable="true">Created date</th> -->
                            <!-- <th data-field="UPDATED_DATE" data-sortable="true">Updated date</th> -->
                            <th data-formatter="actionFormatter" data-field="ID" data-force-hide="true" class="text-center" data-width="200">Aksi</th>
                        </tr>
                    </thead>
                </table>
        </div>
    </section>
</main>

<script>
    function numberFormatter(value, row, index) {
        var options = $('#tableBarang').bootstrapTable('getOptions')
        var tes = 0
        if (!isNaN(options['pageSize'])) { //cek pagesize angka atau tidak, klo angka jalankan kode dibawah
            tes = ((options["pageNumber"] - 1) * options["pageSize"])
        }
        return index + 1 + tes;
    }

    function actionFormatter(value, row, index) {
        return [
            '<a href="<?= site_url("Master_data/barang/editBarang/") ?>' + value + '" class="badge badge-warning"  ><i class="fa fa-pencil-square-o" title="Edit Data"></i></a>',
            ' ',
            '<a href="<?= site_url("Master_data/barang/aktifstatus/") ?>' + value + '" class="badge badge-danger"   title="Aktif / Nonaktif Data"><i class="fa fa-check" aria-hidden="true"></i></a>'
        ].join('');
    }
</script>