<main class="mt-5 mb-5">
    <section class="container">
        <div class="content-menu">
            <div class="card-body">
                <h5 class="card-title">Edit Barang</h5>
                <hr>
                <a class="btn btn-primary btn-sm text-white">Barang</a>
                <a href="<?= site_url('Master_data/barang/tambahGambar/'.$id.'/edit')?>" class="btn btn-secondary btn-sm text-white">Gambar Barang</a>
                <hr>
                <form action="<?php echo site_url('Master_data/barang/submitEditBarang/'.$id);?>" method="POST" enctype="multipart/form-data">
                <div class="form-row">
                <div class="col-md-4">
                        <label class="mt-lg-0 mt-4">Brand</label>
                        <select name="brand" class="form-control js-example-basic-single">
                            <option value="" disabled="disabled" selected>Pilih Brand</option>
                            <?php foreach($dataBrand as $brand):?>
                                <option value="<?php echo $brand['ID']?>"
                                <?php if ($brand['ID'] == $data['ID_BRAND']) {
                                        echo "selected";
                                    } ?>>
                                    <?php echo $brand['NAMA_BRAND']?>
                                </option>
                            <?php endforeach; ?>
                            </select> 
                        <?php echo form_error('divisi_departemen', "<span class='text-danger'>", "</span>"); ?>
                    </div>
                    <div class="col-md-4">
                        <label for="kode">Kode Barang</label>
                        <input type="text" class="form-control no-border" name="kode" disabled id="kode" placeholder="Masukkan Kode Barang" value="<?php echo $data['KODE']?>">
                        <?php echo form_error('kode', "<span class='text-danger'>", "</span>"); ?>
                    </div>
                    <div class="col-md-4">
                        <label for="nama">Harga Satuan</label>
                        <input type="numeric" class="form-control no-border" name="harga" id="nama" placeholder="Masukkan Harga Satuan" value="<?php echo $data['HARGA_SATUAN']?>">
                        <?php echo form_error('harga', "<span class='text-danger'>", "</span>"); ?>
                    </div>
                    <div class="col-md-6">
                        <label for="nama">Nama Barang</label>
                        <input type="text" class="form-control no-border" name="nama" id="nama" placeholder="Masukkan Nama Barang" value="<?php echo $data['NAMA']?>">
                        <?php echo form_error('nama', "<span class='text-danger'>", "</span>"); ?>
                    </div>
                    <div class="col-md-6">
                        <label for="nama">Jumlah Barang</label>
                        <input type="numeric" class="form-control no-border" name="jumlah" disabled id="nama" placeholder="Masukkan Jumlah Barang" value="<?php echo $data['HARGA_SATUAN']?>">
                        <?php echo form_error('jumlah', "<span class='text-danger'>", "</span>"); ?>
                    </div>
                    <div class="col-md-12">
                        <label for="nama">Deskripsi Barang</label>
                        <input type="text" class="form-control no-border" name="deskripsi" id="nama"  placeholder="Masukkan Deskripsi Barang" onKeyPress="if(this.value.length==255) return false;" onkeydown="return event.keyCode !== 69 && event.keyCode !== 187 && event.keyCode !== 188 && event.keyCode !== 189 && event.keyCode !== 190" value="<?php echo $data['DESKRIPSI']?>">
                        <?php echo form_error('deskripsi', "<span class='text-danger'>", "</span>"); ?>
                    </div>
                    <div class="col-md-6 mt-4">
                        <span>Status</span>
                    <?php foreach($status as $st):?>
                        <div class="form-check form-check-radio form-check-inline">
                            <label class="form-check-label">
                                <input class="form-check-input" name="status" type="radio" value="<?php echo $st['KODE']?>"
                                    <?php echo ($data["STATUS"] == $st['KODE']? ' checked' : ''); ?>><?php echo $st['NAMA_STATUS']?>
                                <span class="form-check-sign"></span>
                            </label>
                        </div>
                    <?php endforeach ?>
                    <?= form_error('status', '<small class="text-danger">', '</small>') ?> 
                    </div>
                    </div>
                    <div class="action clearfix mt-5">
                        <button class="btn btn-success float-right ml-3">Simpan</button>
                        <a href="<?php echo site_url('Master_data/barang')?>" class="btn btn-primary float-right">Kembali</a>
                    </div>
                </form>
            </div>
        </div>
    </section>
</main>
