<main class="mt-5 mb-5">
    <section class="container">
        <div class="content-menu">
            <div class="card-body">
                <h5 class="card-title">Tambah Barang</h5>
                <hr>
                    <?php if ($frompage == 'edit') {?>
                        <a href="<?= site_url('Master_data/barang/EditBarang/'.$ID)?>" class="btn btn-primary btn-sm text-white">Barang</a>
                    <?php }else if($frompage == 'tambah'){?>
                        <a href="<?= site_url('Master_data/barang/tambahEditBarang/'.$ID)?>" class="btn btn-primary btn-sm text-white">Barang</a>
                    <?php }?>
                <a href="#" class="btn btn-primary btn-sm text-white">Gambar Barang</a>
                <hr>
                <?php echo $this->session->flashdata('status'); ?>
                <form action="<?php echo site_url('Master_data/barang/submitTambahGambar/'.$ID.'/'.$frompage);?>" method="POST" enctype="multipart/form-data">
                <div class="form-row">
                    <div class="col-md-5 pt-3">
                        <label for="kode">Gambar Barang</label>
                        <small class="text-danger">*File Sertifikat (2MB)</small>
                        <input type="file" class="form-control no-border " name="gambar" placeholder="Masukkan Gambar" value="<?php echo set_value("gambar")?>">
                        <?php echo form_error('gambar', "<span class='text-danger'>", "</span>"); ?>
                        <input type="hidden" value="<?=$ID?>" name="id_barang">
                    </div>
                    <div class="action clearfix mt-5">
                        <button class="btn btn-success float-right ml-3">Tambah</button>
                    </div>
                    </div>
                    <hr>
                </form>
                <form action="<?php echo site_url('')?>" method="post" enctype="multipart/form-data">
                    <table class="table-responsive no-border ">
                        <thead>
                            <tr>
                                <th>Gambar</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <?php foreach ($gambar as $key => $g) { ?>
                        <tbody>
                            <tr>
                                <td><img src="<?php echo base_url('uploads/gambarBarang/'.$g['FILE_NAME'])?>" alt="" style="width:200px;"></td>
                                <td><a href="<?php echo site_url('Master_data/barang/hapusGambar/'.$ID.'/'.$g['FILE_NAME'].'/'.$frompage)?>" class="btn btn-danger">Hapus</a></td>
                                <td><a href="<?php echo site_url('Master_data/barang/mainGambar/'.$ID.'/'.$g['ID'].'/'.$frompage)?>" class="btn btn-primary">Main</a></td>
                            </tr>
                            
                        </tbody>
                        <?php }?>
                    </table>
                </form>
                    <div class="action clearfix mt-5">
                        <a href="<?php echo site_url('Master_data/barang/index')?>" class="btn btn-success float-right ml-3">Simpan</a>
                        <?php if ($frompage == 'edit') {?>
                            <a href="<?php echo site_url('Master_data/barang/EditBarang/'.$ID)?>" class="btn btn-primary float-right">Kembali</a>
                        <?php }else if($frompage == 'tambah'){?>
                            <a href="<?php echo site_url('Master_data/barang/tambahEditBarang/'.$ID)?>" class="btn btn-primary float-right">Kembali</a>
                        <?php }?>
                <?php if ($frompage == 'edit') {?>
                <?php }else if($frompage == 'tambah'){?>
                    <form action="<?php echo site_url('Master_data/barang/hapusDataBarang/'.$ID.'/'.$frompage)?>" method="post" enctype="multipart/form-data">
                        <?php foreach ($gambar as $g) :?>
                            <input type="hidden" value="<?= $g['FILE_NAME']; ?>" name="gambar[]">
                        <?php endforeach ?>
                        <button class="btn batal float-left">Batal</button>
                    </form>
                <?php }?>
                    </div>
            </div>
        </div>
    </section>
</main>
