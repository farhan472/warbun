        <link rel="stylesheet" href="<?php echo base_url() ?>/assets/css/style_login.css" type="text/css">
        <main>        
            <div class="row justify-content-around">
                <div class="col-8">
                    <form class="box" action="<?php echo site_url('register/register_success')?>" method="POST" enctype="multipart/form-data">
                        <h1>Login</h1>
                        <p class="text-muted"> Please enter your data!</p> 
                        <?php echo $this->session->flashdata('status'); ?>
                        <div class="form-row">
                            <div class="col-md-12">
                                <input type="text" class="form-control no-border" name="email" id="kode" placeholder="Masukkan Email" value="<?php echo set_value("email")?>">
                                <?php echo form_error('email', "<span class='text-danger'>", "</span>"); ?>
                            </div>
                            <div class="col-md-12">
                                <input type="text" class="form-control no-border" name="nama" id="kode" placeholder="Masukkan Nama" value="<?php echo set_value("nama")?>">
                                <?php echo form_error('nama', "<span class='text-danger'>", "</span>"); ?>
                            </div>
                            <div class="col-md-12">
                                <input type="text" class="form-control no-border" name="alamat" id="kode" placeholder="Masukkan Alamat" value="<?php echo set_value("alamat")?>">
                                <?php echo form_error('alamat', "<span class='text-danger'>", "</span>"); ?>
                            </div>
                            <div class="col-md-12">
                                <input type="number" class="form-control no-border" name="no_telp" id="kode" placeholder="Masukkan Nomor Telepon" value="<?php echo set_value("no_telp")?>">
                                <?php echo form_error('no_telp', "<span class='text-danger'>", "</span>"); ?>
                            </div>
                            <br>
                            <div class="col-md-12">
                                <a class="forgot text-muted" href="<?php echo site_url('login')?>">Login</a>
                                <input type="submit" name="" value="Register" href="#">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </main>

    </body>
</html> 
