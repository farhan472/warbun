<link rel="stylesheet" href="<?php echo base_url() ?>/assets/css/style_login.css" type="text/css">
<main class="mt-5 mb-5">
    <div class="row justify-content-around">
        <div class="col-8">
            <form class="box"  action="<?php echo site_url('register/create_password_success')?>" method="POST" enctype="multipart/form-data">
                <?php echo $this->session->flashdata('status'); ?>
                    <div class="form-row">
                        <div class="col-md-12">
                            <label for="kode">Kode</label>
                            <input type="number" class="form-control no-border" name="kode" id="kode" placeholder="Masukkan Kode" value="<?php echo set_value("kode")?>">
                            <?php echo form_error('kode', "<span class='text-danger'>", "</span>"); ?>
                        </div>
                        <div class="col-md-12">
                            <label for="password">Password</label>
                            <input type="password" class="form-control no-border" name="password" id="password" placeholder="Masukkan Password" value="<?php echo set_value("password")?>">
                            <?php echo form_error('password', "<span class='text-danger'>", "</span>"); ?>
                        </div>
                        <div class="col-md-12">
                            <label for="password2">Re-type Password</label>
                            <input type="password" class="form-control no-border" name="password2" id="password2" placeholder="Masukkan Ulang Password" value="<?php echo set_value("password2")?>">
                            <?php echo form_error('password2', "<span class='text-danger'>", "</span>"); ?>
                        </div>
                        <div class="col-md-12">
                            <input type="submit" name="" value="Register" href="#">
                        </div>
                    </div>
                </form>
        </div>
    </div>
</main>
</body>
</html> 
