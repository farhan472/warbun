    <main>
        <section class="">
        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img class="d-block w-100" src="<?php echo base_url()?>assets/image/carousel3.jpg" alt="First slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="<?php echo base_url()?>assets/image/carousel3.jpg" alt="Second slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="<?php echo base_url()?>assets/image/carousel3.jpg" alt="Third slide">
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
        </section>
        <br>
        <section class="container">
            <div class="row">
                <div class="col-9">
                    <div class="row">
                        <?php foreach ($catalog as $key => $cat) {?>
                            <div class="col-md-4">
                                <div class="card  mb-5">
                                    <?php if($cat['FILE_NAME'] == NULL) { ?>
                                        <img class="card-img-top" src="<?php echo base_url('uploads/gambarBarang/default-gambar.png')?>" alt="Card image" style="width:100%;">
                                    <?php }else{?>
                                        <img class="card-img-top" src="<?php echo base_url('uploads/gambarBarang/'.$cat['FILE_NAME'])?>" alt="Card image" style="width:100%:">
                                    <?php } ?>
                                    <div class="card-body">
                                        <h4 class="card-title"><?php echo $cat['NAMA']?></h4>
                                        <p class="card-text"><?php echo $cat['DESKRIPSI']?></p>
                                        <P class="float-left">Rp.<?php echo $cat['HARGA_SATUAN']?></P>
                                        <a href="#" class="btn btn-primary float-right">See Profile</a>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>    
                </div>
                <div class="col-3" >
                    <p>Iklan</p>
                    <img src="<?php echo base_url()?>assets/image/carousel1.jpg" alt="" style="width:100%">
                    <br>
                    <br>
                    <p>Iklan</p>
                    <img src="<?php echo base_url()?>assets/image/carousel1.jpg" alt="" style="width:100%">
                </div>
            </div>
        </section>
    </main>
    <br>