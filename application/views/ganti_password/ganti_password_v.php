<main class="mt-5 mb-5">
    <section class="container">
        <div class="card col-md-6 offset-md-3">
            <div class="card-body ">
            <?php echo $this->session->flashdata('status'); ?>
                <h5 class="card-title">Ganti Password</h5>
                <hr>
                <form action="<?php echo site_url('ganti_password/submitGantiPassword/'.$id);?>" method="POST" enctype="multipart/form-data">
                <div class="form-row">
                    <div class="col-md-12">
                        <label for="password_lama">Password Lama</label>
                        <input type="password" class="form-control no-border" name="password_lama" id="kode" placeholder="Masukkan Password Lama" value="<?php echo set_value("password_lama")?>">
                        <?php echo form_error('password_lama', "<span class='text-danger'>", "</span>"); ?>
                    </div>
                    <div class="col-md-12 mt-2">
                        <label for="nama">Password Baru</label>
                        <input type="password" class="form-control no-border" name="password" id="nama" placeholder="Masukkan Password Baru" value="<?php echo set_value("password")?>">
                        <?php echo form_error('password', "<span class='text-danger'>", "</span>"); ?>
                    </div>
                    <div class="col-md-12 mt-2">
                        <label for="password2">Ketik Ulang Password</label>
                        <input type="password" class="form-control no-border" name="password2" id="password2" placeholder="Masukkan Ulang Password" value="<?php echo set_value("password2")?>">
                        <?php echo form_error('password2', "<span class='text-danger'>", "</span>"); ?>
                    </div>
                    </div>
                    <div class="action clearfix mt-5">
                        <button class="btn btn-success float-right ml-3">Simpan</button>
                        <a href="<?php echo site_url('home')?>" class="btn btn-primary float-right">Kembali</a>
                    </div>
                </form>
            </div>
        </div>
    </section>
</main>
