<main class="mt-5 mb-5">
    <section class="container">
        <div class="content-menu">
            <h5 class="card-title">Catatan</h5>
            <hr>
                <div id="toolbar">
                    <a href="#" class="btn btn-sm btn-success" data-toggle="modal" data-target="#Modal" id="item_tambah"><i class="fa fa-plus"></i></a>
                </div>
                <table 
                class="table border-0 mt-4 mb-4" 
                id="tableCatatan" 
                <?php echo DEFAULT_BOOTSTRAP_TABLE_CONFIG ?> 
                data-export-options='{"fileName": "Data_Catatan"}'
                data-url="<?= site_url("Keuangan/Catatan/dataCatatan") ?>">
                    <thead>
                        <tr>
                            <th data-formatter="numberFormatter">No</th>
                            <th data-field="NAMA_PENGIRIM" data-sortable="true">Nama Pengirim</th>
                            <th data-field="CATATAN" data-sortable="true">Catatan</th>
                            <th data-field="NAMA_PENERIMA" data-sortable="true">Nama Penerima</th>
                            <th data-field="STATUS" data-sortable="true">Status</th>
                            <th data-formatter="actionFormatter" data-field="ID" data-force-hide="true" class="text-center" data-width="200">Aksi</th>
                        </tr>
                    </thead>
                </table>
        </div>
        <!-- MODAL -->
        <div class="modal fade" id="Modal" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Add -->
                    <div id="contentTambah" style="display:none">
                        <div class="modal-header">
                            <h3 class="modal-title" id="myModalLabel">Tambah Data Catatan</h3>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        </div>
                        <form class="form-horizontal" id="formTambah">
                            <div class="modal-body">
                                <div class="alert alert-danger print-error-msg" style="display:none"></div>
                                <div class="form-group">
                                    <!-- <label class="control-label col-xs-3" >Nama Pengirim</label>
                                    <div class="col-xs-9">
                                        <select name="tambah_pengirim" id="tambah_pengirim" style="width:335px;" class="form-control" >
                                            <option value="" disabled selected>Pilih</option>
                                        </select>
                                    </div> -->
                                    <label class="control-label col-xs-3" >Nama Penerima</label>
                                    <div class="col-xs-9">
                                        <select name="tambah_penerima" id="tambah_penerima" style="width:335px;" class="form-control" >
                                            <option value="" disabled selected>Pilih</option>
                                        </select>
                                    </div>
                                    <label class="control-label col-xs-3" >Catatan</label>
                                    <div class="col-xs-9">
                                        <textarea name="tambah_catatan" id="tambah_catatan" cols="50" rows="5"></textarea>
                                    </div>
                                </div>
            
                            </div>
            
                            <div class="modal-footer">
                                <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
                                <button class="btn btn-info" id="btn_tambah">Simpan</button>
                            </div>
                        </form>
                    </div>

                    <!-- Edit -->
                    <div id="contentUbah" style="display:none">
                        <div class="modal-header">
                            <h3 class="modal-title" id="myModalLabel">Ubah Data Catatan</h3>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        </div>
                        <form class="form-horizontal" id="formEdit">
                            <div class="modal-body">
                                <div class="alert alert-danger print-error-msg" style="display:none"></div>
                            <div class="form-group">
                                    <l<label class="control-label col-xs-3" >Nama Penerima</label>
                                    <div class="col-xs-9">
                                        <select name="ubah_penerima" id="ubah_penerima" style="width:335px;" class="form-control" >
                                            <option value="" disabled selected>Pilih</option>
                                        </select>
                                    </div>
                                    <label class="control-label col-xs-3" >Catatan</label>
                                    <div class="col-xs-9">
                                        <textarea name="ubah_catatan" id="ubah_catatan" cols="50" rows="5" disabled></textarea>
                                    </div>
                                    <label class="control-label col-xs-3" >Status Catatan</label>
                                    <div class="col-xs-9">
                                        <select name="ubah_status" id="ubah_status" style="width:335px;" class="form-control" >
                                            <option value="" disabled selected>Pilih</option>
                                        </select>
                                    </div>
                                        <input type="hidden" name="ubah_id" id="ubah_id" value="">
                                </div>
            
                            </div>
                            <div class="modal-footer">
                                <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
                                <button class="btn btn-info" id="btn_update">Ubah</button>
                            </div>
                        </form>
                    </div>

                    <!-- Hapus -->
                    <div id="contentHapus" style="display:none">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel">Hapus Barang</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
                        </div>
                        <form class="form-horizontal" id="formHapus">
                            <div class="modal-body">
                                                
                                    <input type="hidden" name="hapus_id" id="hapus_id" value="">
                                    <div class="alert alert-warning"><p>Apakah Anda yakin mau menghapus barang ini?</p></div>
                                                
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                <button class="btn_hapus btn btn-danger" id="btn_hapus">Hapus</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!--END MODAL-->
    </section>
</main>

<script>
    function numberFormatter(value, row, index) {
        var options = $('#tableCatatan').bootstrapTable('getOptions')
        var tes = 0
        if (!isNaN(options['pageSize'])) { //cek pagesize angka atau tidak, klo angka jalankan kode dibawah
            tes = ((options["pageNumber"] - 1) * options["pageSize"])
        }
        return index + 1 + tes;
    }

    function actionFormatter(value, row, index) {
        return [
            '<a href="#" class="badge badge-primary item_edit" data-toggle="modal" data-target="#Modal" data="'+value+'"><i class="fa fa-pencil-square-o" title="Edit Data"></i></a>',
            ' ',
            // '<a href="#" class="badge badge-warning" id="btn-aktif" data="' + value + '" ><i class="fa fa-check" aria-hidden="true" title="Aktif / Nonaktif Data"></i></a>',
            // ' ',
            // '<a href="#" class="badge badge-danger item_hapus" data-toggle="modal" data-target="#Modal" data="'+value+'"><i class="fa fa-trash" title="Hapus Data"></i></a>'
        ].join('');
    }

    function tambahData(){
        let catatan = $('#tambah_catatan').val();
        // let pengirim = $('#tambah_pengirim').val();
        let penerima = $('#tambah_penerima').val();
        $.ajax({
            type : "POST",
            url  : "<?php echo site_url('Keuangan/Catatan/add'); ?>",
            dataType: "json",
            data : {catatan : catatan ,penerima:penerima},
            success: function(data){
                if($.isEmptyObject(data.error)){
                    $('[name="tambah_catatan"]').val("");
                    // $('[name="tambah_pengirim"]').val("");
                    $('[name="tambah_penerima"]').val("");
                    
                    $('#Modal').modal('hide');
                    $('#tableCatatan').bootstrapTable('refresh');
                    
                    message("sukses",data.sukses);
                    $(".print-error-msg").css('display','none');

                }else{
                    $(".print-error-msg").css('display','block');
                    $(".print-error-msg").html(data.error);
                }
            },
            error : function(){
                $('#Modal').modal('hide');
                message("gagal","Proses Gagal");
            }
        });
        return false;
    }

    function updateData(){
        let status = $('#ubah_status').val();
        let id = $('#ubah_id').val();
            $.ajax({
                type : "POST",
                url  : "<?php echo site_url('keuangan/Catatan/edit'); ?>",
                dataType: "json",
                data : {id:id , status:status},
                success: function(data){
                    if($.isEmptyObject(data.error)){
                    $('#ubah_satuan').val("");
                    $('#ubah_penerima').val("");
                    $('#ubah_catatan').val("");
                    $('#ubah_id').val("");
                    
                    $('#Modal').modal('hide');
                    $('#tableCatatan').bootstrapTable('refresh');

                    message("sukses",data.sukses);
                    $(".print-error-msg").css('display','none');

                    }else{
                        $(".print-error-msg").css('display','block');
                        $(".print-error-msg").html(data.error);
                    }
                },
                error : function(){
                    $('#Modal').modal('hide');
                    message("gagal","Proses Gagal");
                }
            });
            return false;
    }

    function hapusData(){
        var id =$('#hapus_id').val();
        $.ajax({
        type : "POST",
        url  : "<?php echo site_url('keuangan/Catatan/hapus/'); ?>",
        dataType: "json",
        data : {id: id},
                success: function(data){
                    $('#Modal').modal('hide');
                    $('#tableCatatan').bootstrapTable('refresh');
                    if($.isEmptyObject(data.error)){
                        message("sukses",data.sukses);
                    }else{
                        message("gagal",data.error);
                    }
                },
                error : function(){
                    $('#Modal').modal('hide');
                    message("gagal","Proses Gagal");
                }
            });
            return false;
    }

    function aktifStatus(idData){
        let id = idData ;
        $.ajax({
                type: 'post',
                url: '<?php echo site_url('keuangan/Catatan/aktifStatus/'); ?>',
                dataType: "json",
                data: {id : id},
                success: function (data) {
                    $('#tableCatatan').bootstrapTable('refresh');
                    message("sukses",data.sukses);
                },
                error : function(){
                    message("gagal","Proses Gagal");
                }
            });
    }

</script>
<script>
$(document).ready(function(){

// Get Tambah 
    $('#toolbar').on('click','#item_tambah',function(){
        $.ajax({
            type : "GET",
            url  : "<?php echo site_url('Keuangan/Catatan/getDataUserStatus/'); ?>",
            dataType : "JSON",
            // data : {id:id},
            success: function(data){
                    console.log(data);
                    $('#contentHapus').hide();
                    $('#contentUbah').hide();

                    $('#contentTambah').show();
                    $('#Modal').modal('show');
                    
                    $('#tambah_penerima').empty();
                    for(let i = 0;i+1 <= data.user.length ; i++){
                        $('#tambah_penerima').append(`<option value="${data.user[i].ID}" >${data.user[i].NAMA}</option>`);
                    }
                    
                    $('#tambah_pengirim').empty();
                    for(let i = 0;i+1 <= data.user.length ; i++){
                        if (<?php echo $this->session->userdata('id_user')?> == data.user[i].ID) {
                            $('#tambah_pengirim').append(`<option value="${data.user[i].ID}" selected disabled>${data.user[i].NAMA}</option>`);
                        }
                    }
            }
        });
        return false;
        
    });

// Action Tambah 
    $('#contentTambah').on('submit','#formTambah',function(e){
        e.preventDefault();
        tambahData();
    });

// Get Hapus

    // $('#tableCatatan').on('click','.item_hapus',function(){
    //     var id=$(this).attr('data');
    //     $('#contentTambah').hide();
    //     $('#contentUbah').hide();

    //     $('#Modal').modal('show');
    //     $('#contentHapus').show();

    //     $('[name="hapus_id"]').val(id);
    // });

// Action Hapus

    // $('#contentHapus').on('click','#btn_hapus',function(e){
    //     e.preventDefault();
    //     hapusData();        
    // });

// Get Ubah
    $('#tableCatatan').on('click','.item_edit',function(){
        var id=$(this).attr('data');
        $.ajax({
            type : "GET",
            url  : "<?php echo site_url('keuangan/Catatan/getDataUserStatusCatatan/'); ?>" + id,
            dataType : "JSON",
            data : {id:id},
            success: function(data){
                    $('#contentTambah').hide();
                    $('#contentHapus').hide();
                    
                    $('#Modal').modal('show');
                    $('#contentUbah').show();

                    $('[name="ubah_id"]').val(id);
                    $('[name="ubah_catatan"]').val(data.catatan[0].CATATAN);

                    $('#ubah_penerima').empty();
                    for(let i = 0;i+1 <= data.user.length ; i++){
                        if (data.catatan[0].ID_PENERIMA == data.user[i].ID) {
                            $('#ubah_penerima').append(`<option value="" selected disabled>${data.user[i].NAMA}</option>`);
                        }
                    }
                    
                    $('#ubah_status').empty();
                    for(let i = 0;i+1 <= data.status.length ; i++){
                        if (data.status[i].KODE == data.catatan[0].STATUS) {
                            $('#ubah_status').append(`<option value="${data.status[i].KODE}" selected>${data.status[i].NAMA_STATUS}</option>`);
                        }else{
                            $('#ubah_status').append(`<option value="${data.status[i].KODE}" >${data.status[i].NAMA_STATUS}</option>`);
                        }
                    }
            }
        });
        return false;
    });

// Action Ubah 

    $('#contentUbah').on('click','#btn_update',function(e){
        e.preventDefault();
        updateData();
    });

// Action Aktif / Nonaktif 
    // $('#tableCatatan').on('click', '#btn-aktif', function(){
    //     let id = $(this).attr('data');
    //     aktifStatus(id);
    // });

});
</script>

