<style type="text/css">
    .txtedit{
        display: none;
        width: 98%;
    }
</style>
<main class="mt-5 mb-5">
    <section class="container">
        <div class="content-menu">
            <div class="row">
                <div class="col-md-8">
                    <div class="row">
                        <!-- select produk start -->
                        <div class="col-md-12">
                            <div class="content-menu">
                                <div class="form-group">
                                    <?php $kodePembelianDecrypt = $this->encryption->decrypt(str_replace(['miringmiring', 'samasama', 'plusplus', 'et'], ['/', '=', '+', '@'], $kodePembelian))?>
                                    <h4 for="exampleFormControlSelect1">Kasir | <?= $kodePembelianDecrypt?></h4>
                                    <a href="<?php echo site_url()?>/home" class="linkHome"></a>
                                    <hr>
                                    <form action="<?php echo site_url('keuangan/kasir/tambahItem') ?>" id="myForm" method="post">
                                        <select id="select" class="js-example-basic-single" name="id" onchange="tambahData()">
                                            <option selected disabled="disabled" id="option" value ="">Pilih Produk</option>
                                        </select>
                                        <input type="hidden" name="kodepembelian" value="<?php echo $kodePembelian?>">
                                        <button type="submit" value="submit" class="positive" name="save" id="save" style="display:none;">
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- select produk end -->

                        <!-- table start -->
                        <div class="col-md-12">
                            <div class="content-menu">
                                <h5 class="card-title">Produk</h5>
                                <hr>
                                <?php echo $this->session->flashdata('status'); ?>
                                <div id="tableItem">
                                <table class="table table-responsive" >
                                    <thead>
                                        <tr>
                                        <th>Kode</th>
                                        <th>Nama</th>
                                        <th>Jumlah</th>
                                        <th>Harga</th>
                                        <th>Total</th>
                                        <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tableBody">
                                    </tbody>
                                </table>
                                </div>
                            </div>
                        </div>
                        <!-- table end -->
                    </div>
                </div>
                <!-- penjumlahan start -->
                <div class="col-md-4">
                    <div class="content-menu" style="height:93%;">
                        <h4 style="text-align:center;"><?= $kodePembelianDecrypt?></h4>
                        <!-- <hr> -->
                        <table class="table">
                            <thead>
                                <th>Produk</th>
                                <th>Jumlah</th>
                                <th>Harga</th>
                            </thead>
                            <tbody id="hargaProduk">
                            </tbody>
                            <tbody id="bayar">
                                <tr>
                                    <td><span class='' >Bayar</span></td>
                                    <td><span class='' ></span>
                                    <td><input type='number' min="0" id='rupiah' value='0' style='width:100%;'></td>
                                </tr>
                                <hr>
                                <tr>
                                    <td><span class='' >Kembalian</span></td>
                                    <td><span class='' ></span></td>
                                    <td><span class='' id='kembalian'></span></td>
                                </tr>
                                <hr>
                                <tr>
                                    <td><span class='' ></span></td>
                                    <td></td>
                                    <td><button type='button' class='btn btn-primary' id='simpan'>Simpan</button></td>
                                </tr>
                      </tbody>
                        </table>
                    </div>
                </div>
                <!-- penjumlahan end -->
            </div>
        </div>
    </section>
</main>

<script>
    function ambilDataItem(){
        // let link = '<?= site_url('keuangan/kasir/hapusItem/') ?>';
        $.ajax({
            type :'POST',
            url:'<?= site_url('keuangan/kasir/getDataItem') ?>',
            dataType:'json',
            success: function(data){
                // console.log(data);
                let baris = '';
                for(let i = 0;i < data.length;i++){
                        baris += `<tr>
                                    <td><span class='' >${data[i].KODE_BARANG}</span></td>
                                    <td><span class='' >${data[i].NAMA_BARANG} </span></td>
                                    <td><span class='edit' >${data[i].JUMLAH}</span>
                                    <input type='number' min="0" class='txtedit' data-id='${data[i].ID}' data-field='JUMLAH' id='jumlahtxt_${data[i].ID}' value='${data[i].JUMLAH}'></td>
                                    <td><span class='' >${data[i].HARGA_SATUAN}</span></td>
                                    <td><span class='' >${data[i].HARGA_TOTAL}</span></td>
                                    <td><button id='${data[i].ID}' class='btn btn-danger btn-sm hapus_data'> <i class='fa fa-trash'></i> Hapus </button></td>
                                </tr>`
                }
                $('#tableBody').html(baris);
            }
        })
    }
    
    function ambilDataProduk(){
        $.ajax({
            type :'POST',
            url:'<?= site_url('keuangan/kasir/getDataProduk') ?>',
            dataType:'json',
            success: function(data){
                // console.log(data);
                let baris = '';
                for(let i = 0;i < data.length;i++){
                    if (data[i].JUMLAH_BARANG == 0 ) {
                        baris += `<option value="${data[i].ID}" disabled>${data[i].NAMA} \(SOLD OUT!!\)</option>`
                    }else{
                        baris += `<option value="${data[i].ID}">${data[i].NAMA} \(${data[i].JUMLAH_BARANG}\)</option>`
                    }
                }
                $('#select').append(baris);
            }
        })
    }

    function tambahData(){
        let frm = $('#myForm');
        frm.submit(function (ev) {
            ev.preventDefault();
        })
        $.ajax({
                type: frm.attr('method'),
                url: frm.attr('action'),
                data: frm.serialize(),
                dataType:'json',
                success: function (data) {
                    // console.log(data);
                    message('sukses',data.sukses)
                    refreshData();
                }
            });
    }
    
    function ambilDataHargaProduk(){
        let kodePembelian = '<?= $kodePembelianDecrypt ?>';
        $.ajax({
            type :'POST',
            url:"<?= site_url('keuangan/kasir/getDataHargaProduk/') ?>"+kodePembelian,
            dataType:'json',
            success: function(data){
                $('#hargaProduk').empty();
                // $('#bayar').empty();
                let total = 0;
                let totalBayar = $('#rupiah').val();
                for(let i = 0; i < data.length ; i++){
                    total += parseInt(data[i].HARGA_TOTAL);
                }
                
                let baris = '';
                let bayar = '';
                for(let i = 0;i < data.length;i++){
                    baris += `<tr>
                                    <td><span class='' >${data[i].NAMA_BARANG}</span></td>
                                    <td><span class='' >${data[i].JUMLAH}</span>
                                    <td><span class='harga' >${data[i].HARGA_TOTAL}</span></td>
                             </tr>`
                }
                    baris += `<tr>
                                    <td><span class='' >Total</span></td>                                    <td><span class='' ></span>
                                    <td><span class='harga' id='total'>${total}</span></td>
                             </tr>`
                $('#hargaProduk').append(baris);
            }
        })
    }

    function refreshData(){
        ambilDataItem();
        ambilDataHargaProduk();
        hitungTotal(true);
    }

    function hitungTotal(repeat){
        if (repeat == true) {
            $('#kembalian').empty();
            let bayar = $('#rupiah').val() == "" ? 0 : $('#rupiah').val();
            let total = $('#total').text() == "" ? 0 : $('#total').text();
            let kembalian =  parseInt(bayar) - parseInt(total);
            let kodePembelian = '<?= $kodePembelianDecrypt ?>';
            $('#kembalian').append(kembalian);

            $.ajax({
                type: 'post',
                url: '<?php echo site_url('keuangan/kasir/insertTotal/')?>'+kodePembelian,
                data: { bayar: bayar, total: total, kembalian: kembalian },
                success: function (data) {
                    hitungTotal(false);
                }
            });
        }else{
            $('#kembalian').empty();
            let bayar = $('#rupiah').val() == "" ? 0 : $('#rupiah').val();
            let total = $('#total').text() == "" ? 0 : $('#total').text();
            let kembalian =  parseInt(bayar) - parseInt(total);
            let kodePembelian = '<?= $kodePembelianDecrypt ?>';
            $('#kembalian').append(kembalian);
        }
    }

</script>
<script>
$(document).ready(function(){
    ambilDataItem();
    ambilDataProduk();
    ambilDataHargaProduk();
    
    $(".js-example-basic-single").select2({
    width: 'resolve'
    });
    
    $(".js-example-hide-search").select2({
    width: 'resolve',
    minimumResultsForSearch: -1
    });
    
    $('#bayar').on('change', '#rupiah', function(){
        hitungTotal(true);
    });

    $('#bayar').on('click', '#simpan', function(){
        let kodePembelian = '<?= $kodePembelianDecrypt ?>';
        let url = "<?= site_url('home')?>";
        $.ajax({
            type: 'POST',
            url: "<?php echo site_url('keuangan/kasir/updateHeader')?>",
            data: {kode : kodePembelian},
            dataType:'json',
            success: function(data) {
                if (data.sukses !== undefined ) {
                    message('sukses',data.sukses);
                    setTimeout(() => {
                        location.replace(url);
                    }, 2000);
                }else{
                    message('error',data.error);
                }
            },
            error : function(){
                    message("gagal","Proses Gagal");
            }
        });
    });

    $('#tableBody').on('click', '.hapus_data', function(){
        let id = $(this).attr('id');
        if(confirm('Yakin ??')){

        $.ajax({
            type: 'POST',
            url: "<?php echo site_url('keuangan/kasir/hapusItem')?>",
            data: {id:id},
            dataType:'json',
            success: function(data) {
                message('sukses',data.sukses)
                refreshData();
            },
            error : function(){
                    message("gagal","Proses Gagal");
            }
        });
        }
    });

    $('#tableBody').on('click', '.edit', function(){
        // Hide input element
        $('.txtedit').hide();

        // Show next input element
        $(this).next('.txtedit').show().focus();

        // Hide clicked element
        $(this).hide();
    });

    // Focus out from a textbox
    $('#tableBody').on('focusout', '.txtedit', function(){

        // Get edit id, field name and value
        let edit_id = $(this).data('id');
        let fieldname = $(this).data('field');
        let value = $(this).val();

        // assign instance to element variable
        let element = this;

        // Send AJAX request
        $.ajax({
        url: '<?= site_url('keuangan/kasir/updateItem') ?>',
        type: 'post',
        dataType:'json',
        data: { field:fieldname, value:value, id:edit_id},
        success:function(response){
            
            // Hide Input element
            $(element).hide();

            // Update viewing value and display it
            $(element).prev('.edit').show();
            $(element).prev('.edit').text(value);
            refreshData();
            
            if(response.sukses === undefined){
                message('gagal',response.error);
            }else{
                message('sukses',response.sukses);
            }
        },
        error : function(){
                message("gagal","Proses Gagal");
        }
        });
    });
});
</script>

