<div class="container bg-white">
  <nav class="navbar navbar-expand-lg navbar-light container-fluid ">
    <a class="navbar-brand" href="<?php echo site_url() ?>"><h1>Logo</h1></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav ml-auto nav-flex-icons">
        <?php foreach ($checkChild as $key => $cekc) { ?>
          <?php $cc[] = $cekc['PARENT_ID']?>
        <?php }?>
        <?php //var_dump($cc);?>
        <?php foreach ($top as $key => $top) { ?>
          <?php if(in_array($top['MENU_ID'],$cc,true)) {?>
          <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <?php echo $top['NAMA_MENU']?>
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <?php if($top['MENU_ID'] == '1') { ?>
                <?php foreach ($childMaster as $key => $cm) {?>
                  <a class="dropdown-item" href="<?php echo site_url($cm['URL']) ?>"><?php echo $cm['NAMA_MENU']?></a>
                <?php }?>
              <?php }else if($top['MENU_ID'] == '8'){?>
                <?php foreach ($childKeuangan as $key => $ck) {?>
                  <a class="dropdown-item" href="<?php echo site_url($ck['URL']) ?>"><?php echo $ck['NAMA_MENU']?></a>
                <?php }?>
              <?php }?>
            </div>
          </li>
          <?php }else {?>
              <li class="nav-item">
              <a class="nav-link" href="<?php echo site_url($top['URL']) ?>"><?php echo $top['NAMA_MENU']?></a>
            </li> 
          <?php }?>
        <?php }?>
        <li class="nav-item">
          <a class="nav-link" href="<?php echo site_url('Landing') ?>">Switch User</a>
        </li>
        <?php if($this->session->userdata('login')) { ?>
          <li class="nav-item avatar dropdown">
              <?php if($dataUser['FOTO'] == NULL && '<') { ?>
                  <img src="<?php echo base_url('uploads/fotoProfile/default_picture.png')?>" class="nav-link dropdown-toggle rounded-circle z-depth-0" alt="avatar image" id="navbarDropdownMenuLink-55" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="width:55px;">
              <?php }else{?>
                  <img src="<?php echo base_url('uploads/fotoProfile/'.$dataUser['FOTO'])?>" class="nav-link dropdown-toggle rounded-circle z-depth-0" alt="avatar image" id="navbarDropdownMenuLink-55" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="width:55px;">
              <?php } ?>
              <!-- <label><?php //echo $this->session->userdata('NAMA')?></label> -->
            <div class="dropdown-menu dropdown-menu-lg-right dropdown-secondary" aria-labelledby="navbarDropdownMenuLink-55">
              <a class="dropdown-item" href="<?php echo site_url('profile/index/'.$idUser); ?>">Profile</a>
              <a class="dropdown-item" href="<?php echo site_url('ganti_password/index/'.$idUser); ?>">Ganti Password</a>
              <a class="dropdown-item" href="<?php echo site_url('home/logout'); ?>">Logout</a>
            </div>
          </li>
          <?php } else { ?>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo site_url('login'); ?>">Login</a>
          </li>
          <?php } ?>
      </ul>
    </div>
  </nav>
</div>
