        
    
    </div>
     </div>
    <footer>
        <div class="bg-primary container-fluid bg-info waves-effect waves-light" style="height:70px;bottom:0;">
            <br>
            <div class="text-center">
            <strong>Copyright 2020 </strong>© Warbun - E-Commerce, All Rights Reserved
            </div>
        </div>
    </footer>
    <script>
        function message(status,pesan){
            if(status == 'sukses'){
                $('#toast-sukses').toast({delay: 3000});
                $('#pesan-sukses').text(pesan);
                $('#toast-sukses').toast('show');
            }else{
                $('#toast-gagal').toast({delay: 3000});
                $('#pesan-gagal').text(pesan);
                $('#toast-gagal').toast('show');
            }
        }
    </script>
   
    
    
    
    
    <script src="<?php echo base_url() ?>/assets/js/main.js"></script>
    <!-- Resizable -->
    <script src="<?php echo base_url('assets/js/jquery.resizableColumns.min.js') ?>"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="<?php echo base_url('assets/bootstrap-table/dist/bootstrap-table.min.js')?>"></script>
    <!-- Latest compiled and minified Locales -->
    <script src="<?php echo base_url('assets/bootstrap-table/dist/locale/bootstrap-table-id-ID.min.js') ?>"></script>
    <!-- Resizable -->
    <script src="<?php echo base_url('assets/bootstrap-table/dist/extensions/resizable/bootstrap-table-resizable.js') ?>"></script>
    <!-- Jstree -->
    <script src="<?php echo base_url('assets/jstree/dist/jstree.min.js')?>"></script>

    <!-- export bootstrap-table -->
    <script src="<?php echo base_url('assets/js/export/tableExport.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/export/jspdf.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/export/jspdf.plugin.autotable.js') ?>"></script>
    <script src="<?php echo base_url('assets/bootstrap-table/dist/extensions/export/bootstrap-table-export.js') ?>"></script>
  </body>
</html> 