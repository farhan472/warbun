<div class="wrapper">
    <!-- Sidebar Holder -->
    <nav id="sidebar" class="active">
            <div class="sidebar-header">
                <a href="<?php site_url('Home')?>"  class="sidemenu" style="text-decoration: none;">
                    <img src="<?php echo base_url('assets/image/logo-wb2.png')?>" alt="" style="width:50px">
                </a>
            </div>



        <ul class="list-unstyled components">
            <?php $i = 0;?>
            <?php foreach ($checkChild as $key => $cc) { ?>
                <?php $ckc[] = $cc['PARENT_ID']?>
            <?php } ?>
            <?php foreach ($top as $key => $top) { ?>
                    <?php if(!in_array($top['MENU_ID'],$ckc,true)){ ?>
                        <li class="nav-item">
                            <a class="sidemenu" href="<?php echo site_url($top['URL']) ?>" style="text-decoration: none;"><?php echo $top['NAMA_MENU'] ?></a>
                        </li>
                    <?php }else{ ?>
                        <?php $i++?>
                        <li>
                            <a href="<?php echo '#pageSubmenu'.$i?>" class="sidemenu" data-toggle="collapse" aria-expanded="false" style="text-decoration: none;">
                            <?php echo $top['NAMA_MENU'] ?>
                            </a>
                                <ul class="collapse list-unstyled" id="<?php echo 'pageSubmenu'.$i?>">
                                    <?php foreach ($child as $key => $ch) { ?>
                                        <?php if ($ch['PARENT_ID'] == $top['MENU_ID']) { ?>
                                            <li><a href="<?php echo site_url($top['URL'].'/'.$ch['URL']) ?>" class="sidemenu" style="text-decoration: none;" ><?php echo $ch['NAMA_MENU'] ?></a></li>
                                        <?php } ?>
                                    <?php } ?>
                                </ul>
                        </li>
                    <?php } ?>
            <?php } ?>
        </ul>   
    </nav>

    <!-- Page Content Holder -->
    <div id="content">

    <nav class="navbar navbar-default" >
        <div class="container-fluid" style="border-radius:25px;">

            <div class="navbar-header">
                <button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
                    <i class="fa fa-bars"></i>
                </button>

            </div>            
            <div class="dropdown">
                    <?php if($dataUser['FOTO'] == NULL && '<'){ ?>
                            <img src="<?php echo base_url('uploads/fotoProfile/default_picture.png')?>" class="nav-link dropdown-toggle rounded-circle z-depth-0" alt="avatar image" id="navbarDropdownMenuLink-55" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="width:55px;">
                        <?php }else{ ?>
                            <img src="<?php echo base_url('uploads/fotoProfile/'.$dataUser['FOTO'])?>" class="nav-link dropdown-toggle rounded-circle z-depth-0" alt="avatar image" id="navbarDropdownMenuLink-55" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="width:55px;">
                        <?php } ?>
                        <!-- <label><?php //echo $this->session->userdata('NAMA')?></label> -->
                        <div class="dropdown-menu dropdown-menu-lg-right dropdown-secondary" aria-labelledby="navbarDropdownMenuLink-55" style="left : -75px;">
                            <a class="dropdown-item" href="<?php echo site_url('profile/index/'.$idUser); ?>">Profile</a>
                            <a class="dropdown-item" href="<?php echo site_url('ganti_password/index/'.$idUser); ?>">Ganti Password</a>
                            <a class="dropdown-item" href="<?php echo site_url('home/logout'); ?>">Logout</a>
                        </div>
            </div>
        </div>
    </nav>

    <div id="message">
        <div aria-live="polite" aria-atomic="true" style="position: relative;z-index: 1;">
            <div style="position: absolute; top: 0rem; right: 2rem;">
                <div class="toast fade rounded" id="toast-sukses">
                    <div class="toast-header bg-gradient-success">
                        <strong class="mr-auto"><i class="fa fa-globe"></i> Success!</strong>
                        <!-- <button type="button" class="ml-2 mb-1 close" data-dismiss="toast">&times;</button> -->
                    </div>
                    <div class="toast-body bg-success" id="pesan-sukses">
                    </div>
                </div>
            </div>
            <div style="position: absolute; top: 0rem; right: 2rem;">
                <div class="toast fade bg-danger rounded" id="toast-gagal">
                    <div class="toast-header">
                        <strong class="mr-auto"><i class="fa fa-globe"></i> Error !</strong>
                        <!-- <button type="button" class="ml-2 mb-1 close" data-dismiss="toast">&times;</button> -->
                    </div>
                    <div class="toast-body" id="pesan-gagal">
                    </div>
                </div>
            </div>
        </div>
    </div>
    