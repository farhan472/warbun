<!DOCTYPE html>
<html>
<head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

<?php if (empty($this->uri->segment(2)) || $this->uri->segment(2) == 'index') {
    $kondisi = $this->uri->segment(1);
}else{
    $kondisi = $this->uri->segment(2);
}
?>
<?php foreach ($title as $key => $sb) { ?>
        <?php if ($sb['URL'] == strtolower($kondisi)){ ?>
            <title>Warbun | <?php echo $sb['NAMA_MENU']; ?></title>
        <?php  } else if(strtolower($kondisi) == 'home' || $this->uri->segment(1) == '' || $this->uri->segment(1) == 'Home') {?>
            <title>Warbun | Home</title>
        <?php  } else if(strtolower($kondisi) == 'register') {?>
            <title>Warbun | Register</title>
        <?php } else if(strtolower($kondisi) == 'login') {?>
            <title>Warbun | Login</title>
        <?php } else if(strtolower($kondisi) == 'profile') {?>
            <title>Warbun | Profile</title>
        <?php } else if(strtolower($kondisi) == 'ganti_password') {?>
            <title>Warbun | Ganti Password</title>
        <?php } ?>
    <?php } ?>

    <link href="<?php echo base_url('assets/image/logo-wb2.png')?>" type="image/png" rel="icon" >
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/style.css" type="text/css">
    
    <!-- Bootstrap 4.2 CSS -->
	<link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/dist/css/bootstrap.min.css') ?>"  crossorigin="anonymous">

	<!-- Bootstrap CSS -->
    <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous"> -->

	<!-- <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css') ?>"  crossorigin="anonymous"> -->
	<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap-grid.min.css') ?>"  crossorigin="anonymous">
	<link rel="stylesheet" href="<?php echo base_url('assets/bootstrap-table/dist/bootstrap-table.min.css') ?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/fontawesome-5.14.0/css/fontawesome.min.css') ?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/jquery.resizableColumns.css') ?>">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	
  <script src="<?php echo base_url('assets/js/jquery.min.js') ?>" crossorigin="anonymous"></script>
  <script src="<?php echo base_url('assets/js/popper.min.js') ?>" crossorigin="anonymous"></script>
  <!-- <script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>" crossorigin="anonymous"></script> -->
  <script src="<?php echo base_url('assets/bootstrap/dist/js/bootstrap.min.js') ?>" crossorigin="anonymous"></script>



  <!-- Jstree -->
  <link rel="stylesheet" href="<?php echo base_url('assets/jstree-theme/dist/themes/proton/style.min.css')?>"/>

  <!-- Select2 -->
  <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
  <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>

  <!-- Select 2 -->
  <link href="<?php echo base_url('assets/select2/dist/css/select2.css') ?>" rel="stylesheet" />
  <script src="<?php echo base_url('assets/select2/dist/js/select2.full.js') ?>"></script>
  <script src="<?php echo base_url('assets/select2/dist/js/select2.min.js') ?>"></script>

</head>
<body>
