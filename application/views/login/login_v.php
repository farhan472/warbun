<link rel="stylesheet" href="<?php echo base_url() ?>/assets/css/style_login.css" type="text/css">
<main class="mt-5 mb-5">
    <!-- <div class="container"> -->
        <div class="row justify-content-around">
            <div class="col-8">
                <form class="box" action="<?php echo site_url('login/login_success')?>" method="POST" enctype="multipart/form-data">
                    <h1>Login</h1>
                    <p class="text-muted"> Please enter your login and password!</p> 
                    <?php echo $this->session->flashdata('status'); ?>
                    <input type="text" class="form-control no-border" name="email" id="kode" placeholder="Masukkan Email" value="<?php echo set_value("kode")?>">
                    <?php echo form_error('email', "<span class='text-danger'>", "</span>"); ?>
                    
                    <input type="password" class="form-control no-border" name="password" id="nama" placeholder="Masukkan Password" value="<?php echo set_value("nama")?>">
                    <?php echo form_error('password', "<span class='text-danger'>", "</span>"); ?> 
                    <br>
                    <a class="forgot text-muted" href="#">Forgot password? /</a>
                    <a class="forgot text-muted" href="<?php echo site_url('register/index')?>">Register</a>
                    <input type="submit" name="" value="Login" href="#">
                    <!-- <div class="col-md-12">
                        <ul class="social-network social-circle">
                            <li><a href="#" class="icoFacebook" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a href="#" class="icoTwitter" title="Twitter"><i class="fab fa-twitter"></i></a></li>
                            <li><a href="#" class="icoGoogle" title="Google +"><i class="fab fa-google-plus"></i></a></li>
                        </ul>
                    </div> -->
                </form>
            </div>
        </div>
</main>
    <!-- </div> -->
  </body>
</html> 