<main class="mt-5 mb-5">
    <section class="container">
        <div class="card col-md-6 offset-md-3">
            <div class="card-body">
            <?php echo $this->session->flashdata('status'); ?>
                <?php if ($dataUser['FOTO'] == NULL && '<') {?>
                    <img src="<?php echo base_url('uploads/fotoProfile/default_picture.png')?>" alt="" style="width:200px;" class="mx-auto d-block">
                <?php }else {?>
                    <img src="<?php echo base_url('uploads/fotoProfile/'.$dataUser['FOTO'])?>" alt="" style="width:200px;" class="mx-auto d-block">
                <?php }?> 
                <form action="<?php echo site_url('profile/submitEditProfile/'.$id);?>" method="POST" enctype="multipart/form-data">
                <div class="form-row">
                    <div class="col-md-12 pt-3">
                        <label for="foto">Ubah Foto Profile</label>
                        <small class="text-danger">*MAX(2MB)</small>
                        <input type="file" class="form-control no-border " name="foto" placeholder="Masukkan Gambar" value="<?php echo set_value("foto")?>">
                        <?php echo form_error('foto', "<span class='text-danger'>", "</span>"); ?>
                    </div>
                    <div class="col-md-12 pt-3">
                        <label for="email">Email User</label>
                        <?php $curval =  set_value("email") ? set_value("email") : $data['EMAIL']?>
                        <input type="text" class="form-control no-border" name="email" id="email" placeholder="Masukkan Email User" value="<?php echo $curval?>">
                        <?php echo form_error('email', "<span class='text-danger'>", "</span>"); ?>
                    </div>
                    <div class="col-md-12 pt-3">
                        <label for="nama">Nama User</label>
                        <?php $curval =  set_value("nama") ? set_value("nama") : $data['NAMA']?>
                        <input type="text" class="form-control no-border" name="nama" id="nama" placeholder="Masukkan Nama User" value="<?php echo $curval?>">
                        <?php echo form_error('nama', "<span class='text-danger'>", "</span>"); ?>
                    </div>
                    <div class="col-md-12 pt-3">
                        <label for="alamat">Alamat</label>
                        <?php $curval =  set_value("alamat") ? set_value("alamat") : $data['ALAMAT']?>
                        <input type="text" class="form-control no-border" name="alamat" id="alamat" placeholder="Masukkan Alamat User" value="<?php echo $curval?>">
                        <?php echo form_error('alamat', "<span class='text-danger'>", "</span>"); ?>
                    </div>
                    <div class="col-md-12 pt-3">
                        <label for="no_telp">No Telepon</label>
                        <?php $curval =  set_value("no_telp") ? set_value("no_telp") : $data['NO_TELP']?>
                        <input type="number" class="form-control no-border" name="no_telp" id="no_telp" placeholder="Masukkan No Telepon" value="<?php echo $curval?>">
                        <?php echo form_error('no_telp', "<span class='text-danger'>", "</span>"); ?>
                    </div>
                    <div class="col-md-12 pt-3">
                        <label for="posisi">Nama Posisi</label>
                        <select name="posisi" disabled="disabled" class="form-control js-example-basic-single">
                            <option value="" disabled="disabled" selected>Pilih Posisi</option>
                            <?php foreach($dataPosisi as $posisi):?>
                                <option disabled="disabled" value="<?php echo $posisi['ID']?>"
                                <?php if ($posisi['ID'] == $data['POSISI_ID']) {
                                        echo "selected";
                                    } ?>>
                                    <?php echo $posisi['NAMA_POSISI']?>
                                </option>
                            <?php endforeach; ?>
                        </select> 
                        <?php echo form_error('posisi', "<span class='text-danger'>", "</span>"); ?>
                    </div>
                    <div class="action clearfix mt-5">
                        <button class="btn btn-success float-right ml-3">Simpan</button>
                        <a href="<?php echo site_url('home')?>" class="btn btn-primary float-right">Kembali</a>
                    </div>
                </form>
            </div>
        </div>
    </section>
</main>
