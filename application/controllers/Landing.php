<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Landing extends MY_Controller {
    public function __construct()
	{
		parent::__construct();
		// if (!$this->session->login){
        //     redirect('login');
        // }
		$this->load->helper('form');
		$this->load->model('Home_m');
		$this->encryption->initialize(
			array(
				'cipher' => 'aes-192',
				'mode' => 'cfb',
				'key' => '8XtTlwWOpEVjBepLqRJGKroKIvUBUyqI'
			)
		);
    }
	public function index(){
        $data['judul'] = 'Home';
        $data['catalog'] = $this->Home_m->getKatalog()->result_array();
        $this->load->view('templates_user/Landing_v',$data);
        // $this->template('templates_user/landing',$data);
	}
	
	public function home(){
        // $data['judul'] = 'Home';
		// $data['catalog'] = $this->Home_m->getKatalog()->result_array();
        redirect('Home');
	}
	
	// public function login(){
    //     $this->login('login/login_v');
	// }
	
	
	// public function logout(){
    //     $this->session->sess_destroy();
    //     redirect('login');
	// }
	
}
