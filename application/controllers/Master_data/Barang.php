<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Barang extends MY_Controller {
    public function __construct()
	{
		parent::__construct();
		if (!$this->session->login){
            redirect('login');
        }
		$exist = $this->session->getexist;
			if(!is_numeric(array_search('barang', array_column($exist, 'URL')))){
				redirect('home');
		}
		$this->load->helper('form');
		$this->load->model('master_data_m/Barang_m');
		$this->load->model('master_data_m/Master_data_m');
		$this->encryption->initialize(
			array(
				'cipher' => 'aes-192',
				'mode' => 'cfb',
				'key' => '8XtTlwWOpEVjBepLqRJGKroKIvUBUyqI'
			)
		);
		// $exist = $this->session->getexist;
		// 	if(!is_numeric(array_search('domisili', array_column($exist, 'URL')))){
		// 		redirect('home');
		// 	}
		
    }
    
	public function index(){
        $data['judul'] = 'Barang';
        $this->template('master_data/barang/barang_v',$data);
    }

    public function dataBarang()
	{
		$get = $this->input->get();
		
		$limit = !empty($get['limit']) ? $get['limit'] : 0;
		$search = !empty($get['search']) ? $get['search'] : "";
		$offset = !empty($get['offset']) ? $get['offset'] : 0;
		$sort = !empty($get['sort']) ? $get['sort'] : "ID";
		$order = !empty($get['order']) ? $get['order'] : "asc";

		$data['total'] = $this->Barang_m->getBarang("", 0, 0, $search)->num_rows();

		$this->db->order_by($sort, $order);

		
		$patterns = array();
		$patterns[0] = '/^a$/';
		$patterns[1] = '/^ak$/';
		$patterns[2] = '/^akt$/';
		$patterns[3] = '/^akti$/';
		$patterns[4] = '/^aktif$/';
		$patterns[5] = '/^n$/';
		$patterns[6] = '/^no$/';
		$patterns[7] = '/^non$/';
		$patterns[8] = '/^nona$/';
		$patterns[9] = '/^nonak$/';
		$patterns[10] = '/^nonakt$/';
		$patterns[11] = '/^nonakti$/';
		$patterns[12] = '/^nonaktif$/';
		$replacements = array();
		$replacements[0] = '1';
		$replacements[1] = '1';
		$replacements[2] = '1';
		$replacements[3] = '1';
		$replacements[4] = '1';
		$replacements[5] = '0';
		$replacements[6] = '0';
		$replacements[7] = '0';
		$replacements[8] = '0';
		$replacements[9] = '0';
		$replacements[10] = '0';
		$replacements[11] = '0';
		$replacements[12] = '0';
		
		$data['rows'] = $this->Barang_m->getBarang("", $limit, $offset, $search,$patterns,$replacements)->result_array();
		
		foreach ($data['rows'] as $key => $value) {

			$data['rows'][$key]['ID'] = str_replace(['/', '=', '+', '@'],['miringmiring', 'samasama', 'plusplus', 'et'],$this->encryption->encrypt($value['ID']));			
			// print_r($data['rows'][$key]['id']); die();
			$data['rows'][$key]['STATUS'] = str_replace([1, 0], ['Aktif', 'Nonaktif'], $value['STATUS']);	
        }
        $this->db->last_query();
		echo json_encode($data);
    }
    
    public function tambahBarang()
	{
		$data['dataBrand'] = $this->Barang_m->getBrand()->result_array();
		$this->template('master_data/barang/tambah_barang_v',$data);
	}

	public function submitTambahBarang()
	{
		$this->form_validation->set_rules('brand', 'Brand', 'required|trim');
		$this->form_validation->set_rules('kode', 'Kode Barang', 'required|trim|is_unique[ADM_BARANG.KODE]');
		$this->form_validation->set_rules('nama', 'Nama Barang', 'required|trim');
		$this->form_validation->set_rules('deskripsi', 'Deskripsi Barang', 'required|trim|min_length[10]|max_length[255]');
		$this->form_validation->set_rules('harga', 'Harga Barang', 'required|trim|numeric');
		$this->form_validation->set_rules('jumlah', 'Jumlah Barang', 'required|trim|numeric');
		$post_xss = $this->security->xss_clean($this->input->post());

		if ($this->form_validation->run() == FALSE) {
			$data['dataBrand'] = $this->Barang_m->getBrand()->result_array();
			$this->template('master_data/barang/tambah_barang_v',$data);
		} else {
			$data = [
				"ID_BRAND" => $post_xss["brand"],
				"KODE" => $post_xss["kode"],
				"NAMA" => $post_xss["nama"],
				"HARGA_SATUAN" => $post_xss["harga"],
				"JUMLAH_BARANG" => $post_xss["jumlah"],
				"DESKRIPSI" => $post_xss["deskripsi"],
				// "CREATED_DATE" => date('Y-m-d H:i:s')
			];

			$insert = $this->Barang_m->insertBarang($data);

			if ($insert) {
				// $this->session->set_flashdata('status', '<div class="alert alert-success alert-dismissible mt-3" role="alert">
				// <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				// Berhasil Menambah Barang
				// </div>');
				// redirect(site_url('Master_data/Barang'));
				$id = $this->db->select_max('ID')->get('ADM_BARANG')->row_array();
				$id_encrypt = str_replace(['/', '=', '+', '@'],['miringmiring', 'samasama', 'plusplus', 'et'],$this->encryption->encrypt($id['ID']));
				redirect(site_url('Master_data/barang/tambahGambar/'.$id_encrypt.'/tambah'));
				
			} else {
				$this->session->set_flashdata('status', 'Gagal menambah Barang');
				$data['dataBrand'] = $this->Barang_m->getBrand()->result_array();
				$this->template('master_data/barang/tambah_barang_v',$data);
			}
		}
	}

	/////////////// Tambah gambar
	public function tambahGambar($id_encrypt,$frompage)
	{
		$data['ID'] = $id_encrypt;
		$id = $this->encryption->decrypt(str_replace(['miringmiring', 'samasama', 'plusplus', 'et'], ['/', '=', '+', '@'], $id_encrypt));
		$data['gambar'] = $this->Barang_m->getGambar($id)->result_array();
		$data['id_gambar'] = str_replace(['/', '=', '+', '@'],['miringmiring', 'samasama', 'plusplus', 'et'],$this->encryption->encrypt($data['ID']));
		$data['frompage'] = $frompage;
		$this->template('master_data/barang/tambah_gambar_v',$data);
	}
	public function submitTambahGambar($id_encrypt,$frompage){
		if ($_FILES['gambar']['error'] == 4) {
			$this->form_validation->set_rules('gambar', 'Gambar', 'required');
		}
		if ($_FILES['gambar']['error'] == 2 ) {
			$this->form_validation->set_rules('gambar', 'Gambar', 'required', ['required' => 'File melebihi 2MB']);
		}
		if ($_FILES['gambar']['error'] == 0 ) {
			$this->form_validation->set_rules('gambar', 'Gambar', 'callback_file_gambar');
		}
		$post_xss = $this->security->xss_clean($this->input->post());
		if ($this->form_validation->run() == FALSE) {
			$id = $this->encryption->decrypt(str_replace(['miringmiring', 'samasama', 'plusplus', 'et'], ['/', '=', '+', '@'], $id_encrypt));
			$data['ID'] = $id_encrypt;
			$data['gambar'] = $this->Barang_m->getGambar($id)->result_array();
			$data['id_gambar'] = str_replace(['/', '=', '+', '@'],['miringmiring', 'samasama', 'plusplus', 'et'],$this->encryption->encrypt($data['ID']));
			$data['frompage'] = $frompage;
			$this->template('master_data/barang/tambah_gambar_v',$data);
		} else {
			$id_barang = $this->encryption->decrypt(str_replace(['miringmiring', 'samasama', 'plusplus', 'et'], ['/', '=', '+', '@'], $post_xss['id_barang']));
			if ($_FILES['gambar']['error'] == 0) {
				$file = $_FILES['gambar'];
				$upload = $this->uploadGambar($file);
				$data['file_name'] = $upload['file_name'];
				$data['orig_name'] = $upload['orig_name'];
			
				$main = $this->db->select('MAIN_GAMBAR')->where('ID_BARANG',$id_barang)->get('PR_GAMBAR_BARANG')->num_rows();
				// var_dump($main); die();
				if(empty($main)){
					$main_gambar = 1;
				}else{
					$main_gambar = 0;
				}
				// var_dump($main_gambar); die();
			
			
				$dataGambar = [
					"ID_BARANG" => $id_barang,
					"FILE_NAME" => $data['file_name'],
					"ORIG_NAME" => $data['orig_name'],
					"MAIN_GAMBAR" => $main_gambar
					];
				// var_dump($dataGambar); die;
				$this->Barang_m->insertGambar($dataGambar);

				$this->session->set_flashdata('status', '<div class="alert alert-success alert-dismissible mt-3" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				Berhasil Menambah Gambar
				</div>');
				redirect(site_url('Master_data/barang/tambahGambar/'.$id_encrypt.'/'.$frompage));			
			} else {
				$id = $this->encryption->decrypt(str_replace(['miringmiring', 'samasama', 'plusplus', 'et'], ['/', '=', '+', '@'], $id_encrypt));
				$data['ID'] = $id_encrypt;
				$data['gambar'] = $this->Barang_m->getGambar($id)->result_array();
				$data['frompage'] = $frompage;
				$this->template('master_data/barang/tambah_gambar_v', $data);
			}
		}
	}

	////////////// Tambah Edit Barang 
	public function tambahEditBarang($id){
		$decrypt_id = $this->encryption->decrypt(str_replace(['miringmiring', 'samasama', 'plusplus', 'et'], ['/', '=', '+', '@'], $id));
		$data['data'] = $this->Barang_m->editBarang($decrypt_id)->row_array();
		$data['id'] = $id;
		$data['gambar'] = $this->Barang_m->getGambar($decrypt_id)->result_array();
		$data['dataBrand'] = $this->Barang_m->getBrand()->result_array();
		$this->template('master_data/barang/tambah_edit_barang_v', $data);
	}

	public function submitTambahEditBarang($id)
	{
		$post_xss = $this->security->xss_clean($this->input->post());
		$decrypt_id = $this->encryption->decrypt(str_replace(['miringmiring', 'samasama', 'plusplus', 'et'], ['/', '=', '+', '@'], $id));

		// $kode = $this->db->query('SELECT "KODE" FROM public."ADM_BARANG" WHERE "ID" = '.$decrypt_id)->row()->KODE;
		// if ($kode != $post_xss['kode'])
        // 	{
		// 		$this->form_validation->set_rules('kode', 'Kode Barang', 'required|trim|is_unique[ADM_BARANG.KODE]');    
        // } else 
        // {
			$this->form_validation->set_rules('brand', 'Brand', 'required|trim');
			// $this->form_validation->set_rules('kode', 'Kode Barang', 'required|trim');
			$this->form_validation->set_rules('nama', 'Nama Barang', 'required|trim');
			$this->form_validation->set_rules('deskripsi', 'Deskripsi Barang', 'required|trim|min_length[10]|max_length[255]');
			$this->form_validation->set_rules('harga', 'Harga Barang', 'required|trim|numeric');
			// $this->form_validation->set_rules('jumlah', 'Jumlah Barang', 'required|trim|numeric');
		//	}


		if ($this->form_validation->run() == FALSE) {
			$data['id'] = $id;
			$data['data'] = $this->Barang_m->getBarang($decrypt_id)->row_array();
			$data['gambar'] = $this->Barang_m->getGambar($decrypt_id)->result_array();
			$data['dataBrand'] = $this->Barang_m->getBrand()->result_array();
			$data['status'] = $this->Master_data_m->getStatus()->result_array();
         	$this->template('master_data/barang/tambah_edit_barang_v', $data);
		} else {
			$data = [
				"ID_BRAND" => $post_xss["brand"],
				// "KODE" => $post_xss["kode"],
				"NAMA" => $post_xss["nama"],
				"HARGA_SATUAN" => $post_xss["harga"],
				// "JUMLAH_BARANG" => $post_xss["jumlah"],
				"DESKRIPSI" => $post_xss["deskripsi"],
				"STATUS" => 1
			];
			
			$update = $this->Barang_m->updateBarang($decrypt_id, $data);
			if ($update) {
				redirect(site_url('Master_data/barang/tambahGambar/'.$id.'/tambah'));
			} else {
				$this->session->set_flashdata('status', 'Gagal mengubah Barang');
				$this->template('master_data/barang/edit_barang_v', $data);
				// redirect('Master_data/Barang/editBarang/' . $id);
			}
		}
	}


	////////////// Edit barang 

	public function editBarang($id_encrypt)
	{
		$decrypt_id = $this->encryption->decrypt(str_replace(['miringmiring', 'samasama', 'plusplus', 'et'], ['/', '=', '+', '@'], $id_encrypt));
		$data['data'] = $this->Barang_m->editBarang($decrypt_id)->row_array();
		$data['id'] = $id_encrypt;
		$data['dataBrand'] = $this->Barang_m->getBrand()->result_array();
		$data['status'] = $this->Master_data_m->getStatus()->result_array();
		$this->template('master_data/barang/edit_barang_v', $data);
	}

	public function submitEditBarang($id)
	{
		$post_xss = $this->security->xss_clean($this->input->post());
		$decrypt_id = $this->encryption->decrypt(str_replace(['miringmiring', 'samasama', 'plusplus', 'et'], ['/', '=', '+', '@'], $id));

		// $kode = $this->db->query('SELECT "KODE" FROM public."ADM_BARANG" WHERE "ID" = '.$decrypt_id)->row()->KODE;
		// if ($kode != $post_xss['kode'])
        // 	{
		// 		$this->form_validation->set_rules('kode', 'Kode Barang', 'required|trim|is_unique[ADM_BARANG.KODE]');    
        // } else 
        // {
			$this->form_validation->set_rules('brand', 'Brand', 'required|trim');
			// $this->form_validation->set_rules('kode', 'Kode Barang', 'required|trim');
			$this->form_validation->set_rules('nama', 'Nama Barang', 'required|trim');
			$this->form_validation->set_rules('deskripsi', 'Deskripsi Barang', 'required|trim|min_length[10]|max_length[255]');
			$this->form_validation->set_rules('harga', 'Harga Barang', 'required|trim|numeric');
			// $this->form_validation->set_rules('jumlah', 'Jumlah Barang', 'required|trim|numeric');
		//	}


		if ($this->form_validation->run() == FALSE) {
			$data['id'] = $id;
			$data['data'] = $this->Barang_m->getBarang($decrypt_id)->row_array();
			$data['dataBrand'] = $this->Barang_m->getBrand()->result_array();
			$data['status'] = $this->Master_data_m->getStatus()->result_array();
         	$this->template('master_data/barang/edit_barang_v', $data);
		} else {
			$data = [
				"ID_BRAND" => $post_xss["brand"],
				// "KODE" => $post_xss["kode"],
				"NAMA" => $post_xss["nama"],
				"HARGA_SATUAN" => $post_xss["harga"],
				// "JUMLAH_BARANG" => $post_xss["jumlah"],
				"DESKRIPSI" => $post_xss["deskripsi"],
				"STATUS" => $post_xss["status"]
			];

			$update = $this->Barang_m->updateBarang($decrypt_id, $data);
			if ($update) {
				redirect(site_url('Master_data/barang/tambahGambar/'.$id.'/edit'));
			} else {
				$this->session->set_flashdata('status', 'Gagal mengubah Barang');
				$this->template('master_data/barang/edit_barang_v', $data);
				// redirect('Master_data/Barang/editBarang/' . $id);
			}
		}
	}

	public function aktifstatus($id){
		$decrypt_id = $this->encryption->decrypt(str_replace(['miringmiring', 'samasama', 'plusplus', 'et'], ['/', '=', '+', '@'], $id));
		$data['rows'] = $this->db->get_where('ADM_BARANG',['ID' => $decrypt_id])->row_array();
		if($data['rows']['STATUS'] == '1'){
			$input = ["STATUS" => '0'];
			$this->Barang_m->aktif($decrypt_id,$input);
			$this->session->set_flashdata('sukses',"Data Berhasil DiNonaktifkan");
			redirect('Master_data/barang/index');
		}else if($data['rows']['STATUS'] == '0'){
			$input = ["STATUS" => '1'];
			$this->Barang_m->aktif($decrypt_id,$input);
			$this->session->set_flashdata('sukses',"Data Berhasil DiAktifkan");
			redirect('Master_data/barang/index');
		}
	}

	private function uploadGambar($file) {
		$dir = FCPATH . "uploads/gambarBarang";
		$dir = str_replace(array("\\", "//"), "/", $dir);
		$config['upload_path'] 		= $dir;
		$config['allowed_types'] 	= 'jpg|jpeg|png';
		$config['file_name']		= $file['name'];
		$config['encrypt_name'] 	= true;
		$config['overwrite'] 		= true;
		$this->load->library('upload');
		$this->upload->initialize($config);
		
		if (!file_exists($dir)) {
            mkdir($dir, 777, true);//root
        }

        if ($this->upload->do_upload('gambar')) {
			$gbr = $this->upload->data();
			//Compress Image
			$config['image_library']='gd2';
			$config['source_image']='./uploads/gambarBarang/'.$gbr['file_name'];
			$config['create_thumb']= FALSE;
			$config['maintain_ratio']= FALSE;
			$config['quality']= '100%';
			$config['width']= 300;
			$config['height']= 300;
			$config['new_image']= './uploads/gambarBarang/'.$gbr['file_name'];
			$this->load->library('image_lib', $config);
			$this->image_lib->resize();
			return $this->upload->data();
        } else {
            return $this->upload->display_errors();
        }
	}

	public function file_gambar($str){
		$allowed_mime_type_arr = array('image/jpg','image/jpeg','image/png');
		$mime = get_mime_by_extension($_FILES['gambar']['name']);
		if(isset($_FILES['gambar']['name']) && $_FILES['gambar']['name']!=""){
			 if(in_array($mime, $allowed_mime_type_arr)){
				  return true;
			 }else{
				  $this->form_validation->set_message('gambar', 'Hanya mendukung format file jpg/jpeg/png');
				  return false;
			 }
		}else{
			 $this->form_validation->set_message('gambar', 'Please choose a file to upload.');
			 return false;
		}
	}

	public function hapusDataBarang($id_encrypt){
		$post_xss = $this->security->xss_clean($this->input->post());
		$id = $this->encryption->decrypt(str_replace(['miringmiring', 'samasama', 'plusplus', 'et'], ['/', '=', '+', '@'], $id_encrypt));
		if(!empty($post_xss['gambar'])){
			foreach ($post_xss['gambar'] as $gambar){
				$file_gambar = FCPATH . "uploads/gambarBarang/".$gambar;
				unlink($file_gambar);
				// print_r($attachment);
			}
		}
		//  die();
		$this->Barang_m->deleteDataBarang($id);
		redirect('Master_data/Barang');
	}
	
	public function hapusGambar($id_hapus, $file,$frompage) {
		$newfile = "'".$file."'"; 
		$id = $this->db->query('SELECT "ID" FROM public."PR_GAMBAR_BARANG" WHERE "FILE_NAME" = '.$newfile)->row()->ID;

		$delete = $this->Barang_m->deleteGambar($file);

		if($delete) {
			$file = FCPATH . "uploads/gambarBarang/". $file;
			unlink($file);

			$this->session->set_flashdata('status', '<div class="alert alert-success alert-dismissible mt-3" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			Berhasil Menghapus Gambar
			</div>');
			redirect('Master_data/barang/tambahGambar/'.$id_hapus.'/'.$frompage);			
		}
		// $id = $this->encryption->decrypt(str_replace(['miringmiring', 'samasama', 'plusplus', 'et'], ['/', '=', '+', '@'], $id_hapus));
		// $data['ID'] = $id_hapus;
		// $this->template_talent('master_data/barang/tambah_gambar_v', $data);
	}

	public function mainGambar($id_barang, $id_gambar,$frompage){
		$idbarang_decrypt = $this->encryption->decrypt(str_replace(['miringmiring', 'samasama', 'plusplus', 'et'], ['/', '=', '+', '@'], $id_barang));
		$idgambar_decrypt = $this->encryption->decrypt(str_replace(['miringmiring', 'samasama', 'plusplus', 'et'], ['/', '=', '+', '@'], $id_gambar));
		// $data['rows'] = $this->db->get_where('PR_GAMBAR_POSISI',['ID' => $idgambar_decrypt])->row_array();
		// $data['row'] = $this->db->get_where('PR_GAMBAR_BARANG',['ID_BARANG' => $idbarang_decrypt])->row_array();
		
		$input = ["MAIN_GAMBAR" => '0'];
		$nonaktif = $this->Barang_m->nonaktifMain($idbarang_decrypt,$input);
		$input = ["MAIN_GAMBAR" => '1'];
		$check = $this->Barang_m->aktifMain($id_gambar,$input);

		$this->session->set_flashdata('status', '<div class="alert alert-success alert-dismissible mt-3" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			Berhasil Mengubah Main Gambar
			</div>');
		redirect('Master_data/barang/tambahGambar/'.$id_barang.'/'.$frompage);
    }
}
