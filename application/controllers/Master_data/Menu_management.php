<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Menu_management extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		if (!$this->session->login){
            redirect('login');
        }
		$exist = $this->session->getexist;
			if(!is_numeric(array_search('menu_management', array_column($exist, 'URL')))){
				redirect('home');
		}
		$this->load->helper('form');
		$this->load->model('master_data_m/Menu_management_m');
		$this->load->model('Login_m');
		$this->encryption->initialize(
			array(
				'cipher' => 'aes-192',
				'mode' => 'cfb',
				'key' => '8XtTlwWOpEVjBepLqRJGKroKIvUBUyqI'
			)
		);
		// if(!$this->session->login)
        // {
		// 	redirect('login');
		// }
		// $exist = $this->session->getexist;
		// 	if(!is_numeric(array_search('menu_management', array_column($exist, 'URL')))){
		// 		redirect('home');
		// 	}
    }

    public function index(){
        $data['dataPosisi'] = $this->Menu_management_m->getPosisi()->result_array();
        $this->template('master_data/menu_management/menu_management_v', $data);
    }
	 
	 public function submitMenu(){
      $this->form_validation->set_rules('ID', 'Nama Menu', 'required');
      $post_xss = $this->security->xss_clean($this->input->post());
		if ($this->form_validation->run() == FALSE) {
         $data['dataPosisi'] = $this->Menu_management_m->getPosisi()->result_array();
         $this->template('master_data/menu_management/menu_management_v', $data);

		} else {
			$id = $post_xss['ID'];
			$data['dataPosisi'] = $this->Menu_management_m->getPosisi()->result_array();
			$data['id'] = $id;
			$this->template('master_data/menu_management/menu_management_s', $data);
		}
	}
	
	public function submitHirarkiMenu(){
		$post_xss = $this->security->xss_clean($this->input->post());
		$jsfields = $post_xss['idshow'];
		// var_dump($jsfields); die;
		// print_r(explode(",", $jsfields)); die;
		
		$id = $post_xss['id'];
		// var_dump($id); die;
		$id_menu = explode(",", $jsfields);

		$delete = $this->Menu_management_m->deleteHirarkiMenu($id);

		$data = [];
		foreach ($id_menu as $key => $value) {
			$data[] = [
				"POSISI_ID"				=> $id,
				"MENU_MANAGEMENT_ID" 	=> $id_menu[$key]
			];
		}

		$insert = $this->Menu_management_m->insertHirarkiMenu($data);
		// echo $this->db->last_query(); die;
		// print_r($data); die;

		if ($delete && $insert) 
		{
			$this->session->set_flashdata('status', '<div class="alert alert-success alert-dismissible mt-3" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			Berhasil Mengubah Menu Management
			</div>');
			$this->session->unset_userdata('getexist');
			$getPosisi = $this->session->id_posisi;
			$getExist = $this->Login_m->getExist($getPosisi)->result_array();
			$this->session->set_userdata('getexist',$getExist);
			redirect('Master_data/menu_management');
		} 
		else 
		{
			$this->session->set_flashdata('status', 'Gagal Mengubah Menu Management');
			redirect('Master_data/menu_management');
		};
	}

	 public function getMenu($id=''){
		$checked_id = [];

		if (!empty($id)) {
			$dataRelasi = $this->Menu_management_m->getMenuRelasi($id)->result_array();

			foreach ($dataRelasi as $key => $value) {
				$checked_id[] = $value['MENU_MANAGEMENT_ID'];
			}
		}

		$get_data = $this->Menu_management_m->getMenu()->result();
		// var_dump($data['get_data']); die;
		$data_hirarki = array();
		
		foreach ($get_data as $key => $getHirarki) {
		$cek = $this->cek($getHirarki->MENU_ID);
		if($cek == true){
			$data_hirarki[$key] = array(
				'id'		=> $getHirarki->MENU_ID,
				'text'		=> $getHirarki->NAMA_MENU,
				'icon' 		=> false,
				'children' 	=> $this->get_parent($getHirarki->MENU_ID, $checked_id)
			);
		}else{
			$data_hirarki[$key] = array(
				'id'	=> $getHirarki->MENU_ID,
				'text'	=> $getHirarki->NAMA_MENU,
				'icon' 	=> false
			);
		}

		if (in_array($getHirarki->MENU_ID, $checked_id)) {
			$data_hirarki[$key]['state'] = array(
				'opened' 	=> true,
            	'selected'	=> true
			);
		}
	}
	// print_r($data_hirark	i);die;
	echo json_encode($data_hirarki);
}	

function cek($id=''){
	$get_cek = $this->db->get_where('ADM_MENU_MANAGEMENT', array('PARENT_ID'=>$id))->result();
	if(!empty($get_cek)){
		return true;
	}else{
		return false;
	}
}


function get_parent($id_parent='', $checked_id=[]){
$get_data = $this->Menu_management_m->getParent($id_parent)->result();

$data_hirarki = array();
foreach ($get_data as $key => $getHirarki){
	$cek = $this->cek($getHirarki->MENU_ID);
	if($cek == true){
		$data_hirarki[$key] = array(
			'id'	=> $getHirarki->MENU_ID,
			'text'	=> $getHirarki->NAMA_MENU,
			'children' => $this->get_parent($getHirarki->MENU_ID),
			'icon' => false
		);
	}else{
		$data_hirarki[$key] = array(
			'id'	=> $getHirarki->MENU_ID,
			'text'	=> $getHirarki->NAMA_MENU,
			'icon' => false
		);
	}

	if (in_array($getHirarki->MENU_ID, $checked_id)) {
		$data_hirarki[$key]['state'] = array(
			'opened' 	=> true,
			'selected'	=> true
		);
	}
}

return $data_hirarki;
}

public function getNamaHirarki(){
	$data['dataNamaHirarki'] = $this->Hirarki_m->getNamaHirarki()->result_array();
	$this->template('master_data/hirarki/hirarki_v', $data);
	// print_r($data['dataNamaHirarki']); die();	
}


}