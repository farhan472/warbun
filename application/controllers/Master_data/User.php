<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Controller {
    public function __construct()
	{
		parent::__construct();
		if (!$this->session->login){
            redirect('login');
        }
		$this->load->helper('form');
		$this->load->model('master_data_m/User_m');
		$this->load->model('master_data_m/Master_data_m');
		$this->encryption->initialize(
			array(
				'cipher' => 'aes-192',
				'mode' => 'cfb',
				'key' => '8XtTlwWOpEVjBepLqRJGKroKIvUBUyqI'
				)
			);
		$exist = $this->session->getexist;
		if(!is_numeric(array_search('user', array_column($exist, 'URL')))){
		redirect('home');
		}
    }
    
	public function index(){
        $data['judul'] = 'User';
        $this->template('master_data/user/user_v',$data);
    }

    public function dataUser()
	{
		$get = $this->input->get();
		
		$limit = !empty($get['limit']) ? $get['limit'] : 0;
		$search = !empty($get['search']) ? $get['search'] : "";
		$offset = !empty($get['offset']) ? $get['offset'] : 0;
		$sort = !empty($get['sort']) ? $get['sort'] : "ID";
		$order = !empty($get['order']) ? $get['order'] : "asc";

		$data['total'] = $this->User_m->getUser("", 0, 0, $search)->num_rows();

		$this->db->order_by($sort, $order);

		
		$patterns = array();
		$patterns[0] = '/^a$/';
		$patterns[1] = '/^ak$/';
		$patterns[2] = '/^akt$/';
		$patterns[3] = '/^akti$/';
		$patterns[4] = '/^aktif$/';
		$patterns[5] = '/^n$/';
		$patterns[6] = '/^no$/';
		$patterns[7] = '/^non$/';
		$patterns[8] = '/^nona$/';
		$patterns[9] = '/^nonak$/';
		$patterns[10] = '/^nonakt$/';
		$patterns[11] = '/^nonakti$/';
		$patterns[12] = '/^nonaktif$/';
		$replacements = array();
		$replacements[0] = '1';
		$replacements[1] = '1';
		$replacements[2] = '1';
		$replacements[3] = '1';
		$replacements[4] = '1';
		$replacements[5] = '0';
		$replacements[6] = '0';
		$replacements[7] = '0';
		$replacements[8] = '0';
		$replacements[9] = '0';
		$replacements[10] = '0';
		$replacements[11] = '0';
		$replacements[12] = '0';
		
		$data['rows'] = $this->User_m->getUser("", $limit, $offset, $search,$patterns,$replacements)->result_array();
		
		foreach ($data['rows'] as $key => $value) {

			$data['rows'][$key]['ID'] = str_replace(['/', '=', '+', '@'],['miringmiring', 'samasama', 'plusplus', 'et'],$this->encryption->encrypt($value['ID']));			
			// print_r($data['rows'][$key]['id']); die();
			$data['rows'][$key]['STATUS'] = str_replace([1, 0], ['Aktif', 'Nonaktif'], $value['STATUS']);	
		}
		echo json_encode($data);
    }
    
    public function tambahUser()
	{   
        $data['dataPosisi'] = $this->User_m->getPosisi()->result_array();
		$this->template('master_data/user/tambah_user_v',$data);
	}

	public function submitTambahUser()
	{
		$this->form_validation->set_rules('email', 'Email', 'required|trim|is_unique[PR_USER.EMAIL]');
		$this->form_validation->set_rules('nama', 'Nama', 'required|trim');
		$this->form_validation->set_rules('alamat', 'Alamat', 'required|trim');
		$this->form_validation->set_rules('no_telp', 'Nomor Telepon', 'required|trim|numeric|max_length[15]|is_unique[PR_USER.NO_TELP]');
		$this->form_validation->set_rules('posisi', 'Posisi', 'required|trim');
		$this->form_validation->set_rules('password', 'Password', 'required|trim|min_length[5]|max_length[16]');
		$post_xss = $this->security->xss_clean($this->input->post());
		if ($_FILES['foto']['error'] == 4) {
			$this->form_validation->set_rules('foto', 'Foto', 'required');
		} 
		if ($_FILES['foto']['error'] == 2 ) {
			$this->form_validation->set_rules('foto', 'Foto', 'required', ['required' => 'File melebihi 2MB']);
		}
		if ($_FILES['foto']['error'] == 0 ) {
			$this->form_validation->set_rules('foto', 'Foto', 'callback_file_gambar');
		}
		if ($this->form_validation->run() == FALSE) {
            $data['dataPosisi'] = $this->User_m->getPosisi()->result_array();
			$this->template('master_data/user/tambah_user_v',$data);
		} else {
			if ($_FILES['foto']['error'] == 0) {
				$file = $_FILES['foto'];
				$upload = $this->uploadFoto($file);
				$data['file_name'] = $upload['file_name'];
				$data['orig_name'] = $upload['orig_name'];

				$foto_profile =array("FOTO"=>$data['file_name'],"FOTO_ORIGNAME"=>$data['orig_name']);
			}
			
			$dataUser = [
				"EMAIL" => $post_xss["email"],
				"ALAMAT" => $post_xss["alamat"],
				"NO_TELP" => $post_xss["no_telp"],
				"NAMA" => $post_xss["nama"],
				"POSISI_ID" => $post_xss["posisi"],
				"PASSWORD" =>  password_hash($post_xss['password'], PASSWORD_DEFAULT),
			];

			if(!empty($foto_profile)) {
				$merge = array_merge($foto_profile,$dataUser);
            } else {
                $merge = $dataUser;
            }
            

			$insert = $this->User_m->insertUser($merge);

			if ($insert) {
				$this->session->set_flashdata('status', '<div class="alert alert-success alert-dismissible mt-3" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				Berhasil Menambah User
				</div>');
				redirect(site_url('Master_data/user'));
				
			} else {
                $this->session->set_flashdata('status', 'Gagal menambah User');
                $data['dataPosisi'] = $this->User_m->getPosisi()->result_array();
				$this->template('master_data/user/tambah_user_v',$data);
			}
		}
	}

	public function editUser($id)
	{
		$decrypt_id = $this->encryption->decrypt(str_replace(['miringmiring', 'samasama', 'plusplus', 'et'], ['/', '=', '+', '@'], $id));
		$data['data'] = $this->User_m->editUser($decrypt_id)->row_array();
		$data['id'] = $id;
        $data['status'] = $this->Master_data_m->getStatus()->result_array();
        $data['dataPosisi'] = $this->User_m->getPosisi()->result_array();
		$this->template('master_data/user/edit_user_v', $data);
	}

	public function submitEditUser($id)
	{
		$post_xss = $this->security->xss_clean($this->input->post());
		$decrypt_id = $this->encryption->decrypt(str_replace(['miringmiring', 'samasama', 'plusplus', 'et'], ['/', '=', '+', '@'], $id));

		$email = $this->db->query('SELECT "EMAIL" FROM public."PR_USER" WHERE "ID" = '.$decrypt_id)->row()->EMAIL;
		$no_telp = $this->db->query('SELECT "NO_TELP" FROM public."PR_USER" WHERE "ID" = '.$decrypt_id)->row()->NO_TELP;
		if ($no_telp != $post_xss['no_telp'])
        {
			$this->form_validation->set_rules('no_telp', 'Nomor Telepon', 'required|trim|numeric|max_length[15]|is_unique[PR_USER.NO_TELP]');    
		}
		else if ($email != $post_xss['email'] )
        {
				$this->form_validation->set_rules('email', 'Email', 'required|trim|is_unique[PR_USER.EMAIL]');    
		}
		else if ($email != $post_xss['email'] && $no_telp != $post_xss['no_telp'] )
        {
				$this->form_validation->set_rules('email', 'Email', 'required|trim|is_unique[PR_USER.EMAIL]');    
				$this->form_validation->set_rules('no_telp', 'Nomor Telepon', 'required|trim|numeric|max_length[15]|is_unique[PR_USER.NO_TELP]');    
		} else 
        {
            $this->form_validation->set_rules('email', 'Email', 'required|trim');
            $this->form_validation->set_rules('nama', 'Nama', 'required|trim');
            $this->form_validation->set_rules('alamat', 'Alamat', 'required|trim');
            $this->form_validation->set_rules('no_telp', 'Nomor Telepon', 'required|trim|numeric|max_length[15]');
            $this->form_validation->set_rules('posisi', 'Posisi', 'required|trim');
			if ($_FILES['foto']['error'] == 2 ) {
				$this->form_validation->set_rules('foto', 'Foto', 'required', ['required' => 'File melebihi 2MB']);
			}
			if ($_FILES['foto']['error'] == 0 ) {
				$this->form_validation->set_rules('foto', 'Foto', 'callback_file_gambar');
			}
		}
		if ($this->form_validation->run() == FALSE) {
         	$data['id'] = $id;
			$data['data'] = $this->User_m->getUser($decrypt_id)->row_array();
            $data['status'] = $this->Master_data_m->getStatus()->result_array();
            $data['dataPosisi'] = $this->User_m->getPosisi()->result_array();
            $this->template('master_data/user/edit_user_v', $data);
		} else {
			
			
			if ($_FILES['foto']['error'] == 0) {
				$file = $_FILES['foto'];
				$upload = $this->uploadFoto($file);
				$data['file_name'] = $upload['file_name'];
				$data['orig_name'] = $upload['orig_name'];

				$foto_profile =array("FOTO"=>$data['file_name'],"FOTO_ORIGNAME"=>$data['orig_name']);
			}

			$dataUser = [
				"EMAIL" => $post_xss["email"],
				"ALAMAT" => $post_xss["alamat"],
				"NO_TELP" => $post_xss["no_telp"],
				"NAMA" => $post_xss["nama"],
				"POSISI_ID" => $post_xss["posisi"],
				"STATUS" => $post_xss["status"],
			];

			if(!empty($foto_profile)) {
				$gambar = $this->User_m->getUser($decrypt_id)->row_array();
				$file_gambar = FCPATH . "uploads/fotoProfile/".$gambar['FOTO'];
				unlink($file_gambar);
				$merge = array_merge($foto_profile,$dataUser);
            } else {
                $merge = $dataUser;
			}
			
			$update = $this->User_m->updateUser($decrypt_id, $merge);
			if ($update) {
				
				$this->session->set_flashdata('status', '<div class="alert alert-success alert-dismissible mt-3" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				Berhasil Mengubah User
				</div>');
				redirect(site_url('Master_data/user'));
			} else {
                $this->session->set_flashdata('status', 'Gagal mengubah user');
                $data['dataPosisi'] = $this->User_m->getPosisi()->result_array();
                $data['status'] = $this->Master_data_m->getStatus()->result_array();
				$this->template('master_data/user/edit_user_v', $data);
				// redirect('Master_data/User/editUser/' . $id);
			}
		}
	}

	public function gantiPassword($id){
        $data['judul'] = 'Ganti Password';
        $decrypt_id = $this->encryption->decrypt(str_replace(['miringmiring', 'samasama', 'plusplus', 'et'], ['/', '=', '+', '@'], $id));
		$data['id'] = $id;  
        $this->template('master_data/user/ganti_password_v',$data);
	}
	
	public function submitGantiPassword($id)
	{
        $decrypt_id = $this->encryption->decrypt(str_replace(['miringmiring', 'samasama', 'plusplus', 'et'], ['/', '=', '+', '@'], $id));
        $databaseuser = $this->User_m->getDataUser($decrypt_id)->row_array();
        $data['id'] = $id;
        
        
        $this->form_validation->set_rules('password_lama', 'Password Lama', 'required|trim');
        $this->form_validation->set_rules('password', 'Password Baru', 'required|trim');
        $this->form_validation->set_rules('password2', 'Password Confirmation', 'required|trim|matches[password]');
        $post_xss = $this->security->xss_clean($this->input->post());
        
        if ($this->form_validation->run() == FALSE) {
            $data['id'] = $id;
            $this->template('master_data/user/ganti_password_v',$data);
        } else {
            $password = $post_xss['password_lama'];
            if(password_verify($password, $databaseuser['PASSWORD']) || $password == $databaseuser['PASSWORD'])
            // if(password_verify($password, $databaseuser['PASSWORD']))
            // if($password == $databaseuser['PASSWORD'])
            {
                $data = [
                    "PASSWORD" => password_hash($post_xss['password'], PASSWORD_DEFAULT)
                ];
                
                $insert = $this->User_m->gantiPassword($decrypt_id,$data);
                
                if ($insert) {
                    $this->session->set_flashdata('status', '<div class="alert alert-success alert-dismissible mt-3" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    Berhasil Mengubah Password
                    </div>');
                    redirect(site_url('Master_data/user'));
                } else {
                    $this->session->set_flashdata('status', 'Gagal Mengubah Password');
                    $this->template('master_data/user/ganti_password_v');
                }
            }else{
                $this->session->set_flashdata('status', '<div class="alert alert-danger alert-dismissible mt-3" style="width:66%;border-radius:100px;font-weight:bold;" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                Password Lama Anda Salah
                </div>');
                redirect('Master_data/user/gantiPassword/'.$id);
            }
        }
	}

	public function aktifstatus($id){
		$decrypt_id = $this->encryption->decrypt(str_replace(['miringmiring', 'samasama', 'plusplus', 'et'], ['/', '=', '+', '@'], $id));
		$data['rows'] = $this->db->get_where('PR_USER',['ID' => $decrypt_id])->row_array();
		if($data['rows']['STATUS'] == '1'){
			$input = ["STATUS" => '0'];
			$this->User_m->aktif($decrypt_id,$input);
			$this->session->set_flashdata('sukses',"Data Berhasil DiNonaktifkan");
			redirect('Master_data/user/index');
		}else if($data['rows']['STATUS'] == '0'){
			$input = ["STATUS" => '1'];
			$this->User_m->aktif($decrypt_id,$input);
			$this->session->set_flashdata('sukses',"Data Berhasil DiAktifkan");
			redirect('Master_data/user/index');
		}
	}

	private function uploadFoto($file) {
		$dir = FCPATH . "uploads/fotoProfile";
		$dir = str_replace(array("\\", "//"), "/", $dir);
		$config['upload_path'] 		= $dir;
		$config['allowed_types'] 	= 'jpg|jpeg|png';
		$config['file_name']		= $file['name'];
		$config['encrypt_name'] 	= true;
		$config['overwrite'] 		= true;
		$this->load->library('upload');
		$this->upload->initialize($config);
		
		if (!file_exists($dir)) {
            mkdir($dir, 777, true);//root
        }

        if ($this->upload->do_upload('foto')) {
			$gbr = $this->upload->data();
			//Compress Image
			$config['image_library']='gd2';
			$config['source_image']='./uploads/fotoProfile/'.$gbr['file_name'];
			$config['create_thumb']= FALSE;
			$config['maintain_ratio']= FALSE;
			$config['quality']= '100%';
			$config['width']= 300;
			$config['height']= 300;
			$config['new_image']= './uploads/fotoProfile/'.$gbr['file_name'];
			$this->load->library('image_lib', $config);
			$this->image_lib->resize();
			return $this->upload->data();
        } else {
            return $this->upload->display_errors();
        }
    }
    
    public function file_gambar($str){
		$allowed_mime_type_arr = array('image/jpg','image/jpeg','image/png');
		$mime = get_mime_by_extension($_FILES['foto']['name']);
		if(isset($_FILES['foto']['name']) && $_FILES['foto']['name']!=""){
			 if(in_array($mime, $allowed_mime_type_arr)){
				  return true;
			 }else{
				  $this->form_validation->set_message('foto', 'Hanya mendukung format file jpg/jpeg/png');
				  return false;
			 }
		}else{
			 $this->form_validation->set_message('foto', 'Please choose a file to upload.');
			 return false;
		}
	}

	// function checkUserExists($key)
	// {
	// 	$check = $this->User_m->checkUser($key);
	// 	 if ($check == TRUE){
	// 		 return TRUE;
	// 		} else
	// 		{
	// 			return FALSE;
	// 			$this->form_validation->set_message('checkUserExists', '{field} sudah digunakan');
	// 	 }
	// }
}
