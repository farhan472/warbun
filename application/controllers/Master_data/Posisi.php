<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Posisi extends MY_Controller {
    public function __construct()
	{
		parent::__construct();
		if (!$this->session->login){
            redirect('login');
        }
		$this->load->helper('form');
		$this->load->model('master_data_m/Posisi_m');
		$this->load->model('master_data_m/Master_data_m');
		$this->encryption->initialize(
			array(
				'cipher' => 'aes-192',
				'mode' => 'cfb',
				'key' => '8XtTlwWOpEVjBepLqRJGKroKIvUBUyqI'
			)
		);
		$exist = $this->session->getexist;
			if(!is_numeric(array_search('posisi', array_column($exist, 'URL')))){
				redirect('home');
		}
		
    }
    
	public function index(){
        $data['judul'] = 'Posisi';
        $this->template('master_data/posisi/posisi_v',$data);
    }

    public function dataPosisi()
	{
		$get = $this->input->get();
		
		$filtering = $this->uri->segment(3, 0);
		
        $id = (isset($get['id']) && !empty($get['id'])) ? $get['id'] : "";
		$limit = (isset($get['limit']) && !empty($get['limit'])) ? $get['limit'] : "";
		$search = (isset($get['search']) && !empty($get['search'])) ? $get['search'] : "";
		$offset = (isset($get['offset']) && !empty($get['offset'])) ? $get['offset'] : 0;
		$sort = (isset($get['sort']) && !empty($get['sort'])) ? $get['sort'] : "ID";
		$order = (isset($get['order']) && !empty($get['order'])) ? $get['order'] : "";

		$patterns = array();
		$patterns[0] = '/^a$/';
		$patterns[1] = '/^ak$/';
		$patterns[2] = '/^akt$/';
		$patterns[3] = '/^akti$/';
		$patterns[4] = '/^aktif$/';
		$patterns[5] = '/^n$/';
		$patterns[6] = '/^no$/';
		$patterns[7] = '/^non$/';
		$patterns[8] = '/^nona$/';
		$patterns[9] = '/^nonak$/';
		$patterns[10] = '/^nonakt$/';
		$patterns[11] = '/^nonakti$/';
		$patterns[12] = '/^nonaktif$/';
		$replacements = array();
		$replacements[0] = '1';
		$replacements[1] = '1';
		$replacements[2] = '1';
		$replacements[3] = '1';
		$replacements[4] = '1';
		$replacements[5] = '0';
		$replacements[6] = '0';
		$replacements[7] = '0';
		$replacements[8] = '0';
		$replacements[9] = '0';
		$replacements[10] = '0';
		$replacements[11] = '0';
		$replacements[12] = '0';
		
		if(!empty($id)){
            $this->db->where('ID', $id);
        }
        
        if(!empty($search)){
            $this->db->group_start();
            $this->db->like('LOWER("NAMA_POSISI")', strtolower($search));
            $this->db->or_like('CAST("STATUS" AS VARCHAR)', preg_replace($patterns, $replacements,strtolower($search)));
            $this->db->group_end();
        }
        
        
        // $this->db->select('*');
        $data['total'] = $this->Master_data_m->getData($id,'ADM_POSISI')->num_rows();

        //////////////////////////
        
        
        if(!empty($id)){
            $this->db->where('ID', $id);
        }
        
        if(!empty($search)){
            $this->db->group_start();
            $this->db->like('LOWER("NAMA_POSISI")', strtolower($search));
            $this->db->or_like('CAST("STATUS" AS VARCHAR)', preg_replace($patterns, $replacements,strtolower($search)));
            $this->db->group_end();
        }
        
        if(!empty($order)){
            $this->db->order_by($sort, $order);
        }
        
        if(!empty($limit)){
            $this->db->limit($limit,$offset);
        }
        
        
        // $this->db->select('*');
		$data['rows'] = $this->Master_data_m->getData($id,'ADM_POSISI')->result_array();
		
		foreach ($data['rows'] as $key => $value) {

			$data['rows'][$key]['ID'] = str_replace(['/', '=', '+', '@'],['miringmiring', 'samasama', 'plusplus', 'et'],$this->encryption->encrypt($value['ID']));			
			// print_r($data['rows'][$key]['id']); die();
			$data['rows'][$key]['STATUS'] = str_replace([1, 0], ['Aktif', 'Nonaktif'], $value['STATUS']);	
		}
		echo json_encode($data);
    }

	public function getDataPosisi($id){
        $decrypt_id = $this->encryption->decrypt(str_replace(['miringmiring', 'samasama', 'plusplus', 'et'], ['/', '=', '+', '@'], $id));
        $data['status'] = $this->Master_data_m->getStatus()->result_array();
        $data['data'] = $this->Master_data_m->getData($decrypt_id,'ADM_POSISI')->result();
        echo json_encode($data);
    }

    public function add()
    {
        $this->form_validation->set_rules('posisi', 'Posisi', 'trim|required|is_unique[ADM_POSISI.NAMA_POSISI]');
        if($this->form_validation->run()==FALSE){
            $hasil = [
                'error' => validation_errors()
            ];
        
        }else{
            $data=array(
                "NAMA_POSISI"=>$this->security->xss_clean($_POST['posisi']),
                "STATUS"=> 1 ,
            );

            $insert = $this->Master_data_m->insertData($data,'ADM_POSISI');
            
            if ($insert) {
                $hasil = [
                    'sukses'=>'Berhasil Mengubah Data!'
                ];
            }else{
                $hasil = [
                    'error' => 'Eror 404'
                ];
            }
        }
        echo json_encode($hasil);

    }
 
    public function edit(){
        $post_xss = $this->security->xss_clean($this->input->post());

        $decrypt_id = $this->encryption->decrypt(str_replace(['miringmiring', 'samasama', 'plusplus', 'et'], ['/', '=', '+', '@'], $post_xss['id']));
        
        $namaPosisi = $this->Master_data_m->getData($decrypt_id,'ADM_POSISI')->row_array();

        if ($namaPosisi['NAMA_POSISI'] != $_POST['posisi']) {
            $this->form_validation->set_rules('id', 'ID Posisi', 'required');
            $this->form_validation->set_rules('posisi', 'Nama Posisi', 'trim|required|is_unique[ADM_POSISI.NAMA_POSISI]');
        }else{
            $this->form_validation->set_rules('id', 'ID Posisi', 'required');
            $this->form_validation->set_rules('posisi', 'Nama Posisi', 'trim|required');
        }
            if($this->form_validation->run()==FALSE){
                $hasil = [
                    'error' => validation_errors()
                ];
            }else{
                $data = array(
                    "NAMA_POSISI"=>$post_xss['posisi'],
                    "STATUS"=>$_POST['status'],
                );

                $update = $this->Master_data_m->updateData($decrypt_id,$data,'ADM_POSISI');
                
                if ($update) {
                    $hasil = [
                        'sukses'=>'Berhasil Mengubah Data!'
                    ];
                }else{
                    $hasil = [
                        'error' => 'Eror 404'
                    ];
                }
               
            }
            
            echo json_encode($hasil);
       
    }
 
    public function hapus(){   
        $id = $this->input->post('id');
        $decrypt_id = $this->encryption->decrypt(str_replace(['miringmiring', 'samasama', 'plusplus', 'et'], ['/', '=', '+', '@'], $id));
        if($id==""){
            $hasil = [
                'error' => 'Gagal Menghapus Data'
            ];

        }else{
            $delete = $this->Master_data_m->deleteData($decrypt_id,'ADM_POSISI');
            
            if ($delete) {
                $hasil = [
                    'sukses'=>'Berhasil Mengubah Data!'
                ];
            }else{
                $hasil = [
                    'error' => 'Eror 404'
                ];
            }
        }
        echo json_encode($hasil);
    }

	public function aktifstatus(){
		$id = $this->input->post('id');
        
        $decrypt_id = $this->encryption->decrypt(str_replace(['miringmiring', 'samasama', 'plusplus', 'et'], ['/', '=', '+', '@'], $id));
		
        $data['rows'] = $this->db->get_where('ADM_POSISI',['ID' => $decrypt_id])->row_array();
		
        if($data['rows']['STATUS'] == '1'){
			$input = ["STATUS" => '0'];
            $this->Master_data_m->aktif($decrypt_id,$input,'ADM_POSISI');
            $hasil = [
                'sukses'=>'Data Berhasil Di Non-Aktifkan.'
            ];
		}else if($data['rows']['STATUS'] == '0'){
			$input = ["STATUS" => '1'];
            $this->Master_data_m->aktif($decrypt_id,$input,'ADM_POSISI');
            
            $hasil = [
                'sukses'=>'Data Berhasil Di Aktifkan.'
            ];
		}

        echo json_encode($hasil);
	}
}
