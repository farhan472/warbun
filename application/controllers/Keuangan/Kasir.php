<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kasir extends MY_Controller {
    public function __construct()
	{
		parent::__construct();
		if (!$this->session->login){
            redirect('login');
        }
		$exist = $this->session->getexist;
			if(!is_numeric(array_search('kasir', array_column($exist, 'URL')))){
				redirect('home');
		}
		$this->load->helper('form');
		$this->load->model('keuangan/Kasir_m');
		$this->load->model('keuangan/Keuangan_m');
		$this->encryption->initialize(
			array(
				'cipher' => 'aes-192',
				'mode' => 'cfb',
				'key' => '8XtTlwWOpEVjBepLqRJGKroKIvUBUyqI'
			)
		);
    }
    
	public function index(){
		$idUser = $this->session->userdata('id_user');

		$pembelian = $this->Kasir_m->idPembelian()->row_array();

		$idPembelian = $pembelian['KODE_PEMBELIAN'];
		
		$kodePembelianOld =  $pembelian['KODE_PEMBELIAN'];
		
		$kodePembelianNew =  $this->Kasir_m->getNoUrut();

		$checkDataPembelian = $this->Kasir_m->checkDataPembelian($idPembelian)->result_array();

		$dataPembelian = $this->db->get('KEU_KASIR_HEADER')->num_rows();
		
		if (!empty($checkDataPembelian)) {
			$data['kodePembelian'] = str_replace(['/', '=', '+', '@'],['miringmiring', 'samasama', 'plusplus', 'et'],$this->encryption->encrypt($kodePembelianNew));
			
			$kodePembelian = $kodePembelianNew;
			
			$this->session->set_userdata('kodePembelian',$kodePembelianNew);
			
			$insert = [
				'KODE_PEMBELIAN' => $kodePembelian,
				'CREATED_DATE' => date('Y-m-d H:i:s'),
				'ID_KASIR' => $this->session->userdata('id_user'),
				'KASIR' => $this->session->userdata('nama_user')
			];
	
			$insertKodePembelian = $this->Kasir_m->insertKodePembelian($insert);

		}else if ($dataPembelian == 0){
			$data['kodePembelian'] = str_replace(['/', '=', '+', '@'],['miringmiring', 'samasama', 'plusplus', 'et'],$this->encryption->encrypt($kodePembelianNew));
			
			$kodePembelian = $kodePembelianNew;
			
			$this->session->set_userdata('kodePembelian',$kodePembelianNew);
			
			$insert = [
				'KODE_PEMBELIAN' => $kodePembelian,
				'CREATED_DATE' => date('Y-m-d H:i:s'),
				'ID_KASIR' => $this->session->userdata('id_user'),
				'KASIR' => $this->session->userdata('nama_user')
			];
	
			$insertKodePembelian = $this->Kasir_m->insertKodePembelian($insert);
		
		}else{
			$data['kodePembelian'] = str_replace(['/', '=', '+', '@'],['miringmiring', 'samasama', 'plusplus', 'et'],$this->encryption->encrypt($kodePembelianOld));
			
			$kodePembelian = $kodePembelianOld;

			$update = [
				'CREATED_DATE' => date('Y-m-d H:i:s')
			];
			
			$this->Kasir_m->updateHeader($kodePembelian,$update);
			
			$this->session->set_userdata('kodePembelian',$kodePembelianOld);
		}

		$data['barang'] = $this->Kasir_m->getBarang()->result_array();
		
		$data['item'] = $this->Kasir_m->getItem($data['kodePembelian'])->result_array();
		
		// $this->template('keuangan/kasir/kasir_v',$data);

		redirect('keuangan/kasir/kasir');
	}

	public function kasir(){
		$idUser = $this->session->userdata('id_user');

		$pembelian = $this->Kasir_m->idPembelian()->row_array();

		$idPembelian = $pembelian['KODE_PEMBELIAN'];

		$kodePembelianOld =  $pembelian['KODE_PEMBELIAN'];

		$data['kodePembelian'] = str_replace(['/', '=', '+', '@'],['miringmiring', 'samasama', 'plusplus', 'et'],$this->encryption->encrypt($kodePembelianOld));
			
		$kodePembelian = $kodePembelianOld;
		
		$this->session->set_userdata('kodePembelian',$kodePembelianOld);

		$data['barang'] = $this->Kasir_m->getBarang()->result_array();

		$data['item'] = $this->Kasir_m->getItem($kodePembelianOld)->result_array();
	
		$this->template('keuangan/kasir/kasir_v',$data);
	}

	public function getDataItem(){
		$pembelian = $this->Kasir_m->idPembelian()->row_array();

		$kodePembelianOld =  $pembelian['KODE_PEMBELIAN'];

		$item = $this->Kasir_m->getItem($kodePembelianOld)->result();

		echo json_encode($item);
	}

	public function getDataProduk(){
		$barang = $this->Kasir_m->getBarang()->result();
		
		echo json_encode($barang);
	}

	public function getDataHargaProduk($kodePembelian){
		$hargaBarang = $this->Kasir_m->getDataHargaBarang($kodePembelian)->result();
		
		echo json_encode($hargaBarang);
	}

	public function tambahItem(){
		$post = $this->input->post();
		
		$id = $post['id'];
		
		$dataBarang = $this->Kasir_m->getDataBarang($id)->row_array();
		
		$kode = $this->encryption->decrypt(str_replace(['miringmiring', 'samasama', 'plusplus', 'et'], ['/', '=', '+', '@'], $post['kodepembelian']));
		
		$this->db->select('KODE_BARANG');
		$this->db->where('KODE_PEMBELIAN',$kode);
		$dataItem = $this->db->get('KEU_KASIR_ITEM')->result_array();
		
		if (!empty($dataItem)) {
			foreach ($dataItem as $key => $di) {
				$dataKodeBarang[] = $di['KODE_BARANG'];
			}
		}else{
			$dataKodeBarang[] = 'kosong';
		}
		
		if(in_array($dataBarang['KODE'],$dataKodeBarang)){
			$item = $this->db->where('KODE_PEMBELIAN',$kode)->where('KODE_BARANG',$dataBarang['KODE'])->get('KEU_KASIR_ITEM')->row_array();
			$data = [
				'JUMLAH' =>  $item['JUMLAH'] + 1
			];
			$this->db->where('KODE_BARANG',$dataBarang['KODE']);
			$this->db->update('KEU_KASIR_ITEM',$data);
			$hasil = [
				'sukses'=>'Berhasil Menambah Data!'
			];
		}else{
			$insert = [
				'KODE_PEMBELIAN' => $kode,
				'NAMA_BARANG' => $dataBarang['NAMA'],
				'JUMLAH' => 1,
				'KODE_BARANG' => $dataBarang['KODE'],
				'HARGA_SATUAN' => $dataBarang['HARGA_SATUAN']
			];
			$this->Kasir_m->insertItem($insert);
			$hasil = [
				'sukses'=>'Berhasil Menambah Data!'
			];
		}
		echo json_encode($hasil);
	}

	public function updateItem(){
		// POST values
		$id = $this->input->post('id');
		$field = $this->input->post('field');
		$value = $this->input->post('value');
		
		$kodeBarang = $this->Kasir_m->getDataItem($id)->row_array();
		$jmlBarang = $this->Kasir_m->getdataBarang("",$kodeBarang['KODE_BARANG'])->row_array();
		if($value > $jmlBarang['JUMLAH_BARANG']){
			$hasil = [
				'error' => 'Jumlahnya melebihi stock!'
			];
		}else{
			// Update records
			$update = $this->Kasir_m->updateItem($id,$field,$value);
			if ($update) {
				$hasil = [
					'sukses'=>'Berhasil Mengubah Data!'
				];
			}else{
				$hasil = [
					'error' => 'Eror 404'
				];
			}
		}
		echo json_encode($hasil);
	}

	public function insertTotal($kodePembelian){
		$post = $this->input->post();
		
		$data = [
			'TOTAL_PEMBAYARAN' => $post['bayar'],
			'TOTAL_PEMBELIAN' => $post['total'],
			'KEMBALIAN' => $post['kembalian']
		];

		$insert = $this->Kasir_m->updateHeader($kodePembelian,$data);

		json_encode($insert);
	}

	public function hapusItem(){
		$id = $this->input->post('id');
		$delete = $this->db->where('ID', $id)->delete('KEU_KASIR_ITEM');
		if ($delete) {
			$hasil = [
				'sukses'=>'Berhasil Menghapus Data!'
			];
		}else{
			$hasil = [
				'error' => 'Eror 404'
			];
		}
	echo json_encode($hasil);

	}

	public function updateHeader(){
		$kodePembelian = $this->input->post('kode');
		
		$items = $this->Kasir_m->getItem($kodePembelian)->result_array();
		
		$header = $this->Kasir_m->getDataHeader($kodePembelian)->row_array();
		if(!empty($items) && $header['KEMBALIAN'] > 0){
			$data = [
				"IS_COMPLETE" => 1
			];
			$update = $this->Kasir_m->updateHeader($kodePembelian,$data);
			if ($update) {
				$count = $this->countItem($kodePembelian);
				if($count){
					$hasil = [
						'sukses'=>'Data Telah Disimpan!'
					];
				}else{
					$hasil = [
						'error' => 'Eror 404'
					];
				}
			}else{
				$hasil = [
					'error' => 'Eror 404'
				];
			}
		}else if($header['KEMBALIAN'] <= 0){
			$hasil = [
				'error' => 'Uang Pembayaran Tidak Cukup!'
			];
		}else{
			$hasil = [
				'error' => 'Tidak ada item yang dipilih!'
			];
		}
		echo json_encode($hasil);
	}

	public function countItem($kodePembelian){
		$data = [];
		$items = $this->Kasir_m->getItem($kodePembelian)->result_array();
		foreach ($items as $key => $value) {
			$kodeItem = $value['KODE_BARANG'];
			$jmlItem = $value['JUMLAH'];
			
			$jmlBarang = $this->Kasir_m->getdataBarang("",$kodeItem)->row()->JUMLAH_BARANG;
			
			$data = [
				"JUMLAH_BARANG"	=> $jmlBarang - $jmlItem
			];

			$this->Kasir_m->updateBarang($kodeItem,$data);
		}

		return true;
	}


}
