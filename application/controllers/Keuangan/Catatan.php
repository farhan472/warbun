<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Catatan extends MY_Controller {
    public function __construct()
	{
		parent::__construct();
		
        if (!$this->session->login){
            redirect('login');
        }

		$exist = $this->session->getexist;
			if(!is_numeric(array_search('catatan', array_column($exist, 'URL')))){
				redirect('home');
        }

        $this->load->model('keuangan/Keuangan_m');
        // $this->load->model('master_data_m/Satuan_m');
		
        $this->encryption->initialize(
			array(
				'cipher' => 'aes-192',
				'mode' => 'cfb',
				'key' => '8XtTlwWOpEVjBepLqRJGKroKIvUBUyqI'
			)
		);
    }

    public function index(){
        $this->template('keuangan/catatan/catatan_v');
    }

    public function dataCatatan()
	{
		$get = $this->input->get();

        $filtering = $this->uri->segment(3, 0);
		
        $id = (isset($get['id']) && !empty($get['id'])) ? $get['id'] : "";
		$limit = (isset($get['limit']) && !empty($get['limit'])) ? $get['limit'] : "";
		$search = (isset($get['search']) && !empty($get['search'])) ? $get['search'] : "";
		$offset = (isset($get['offset']) && !empty($get['offset'])) ? $get['offset'] : 0;
		$sort = (isset($get['sort']) && !empty($get['sort'])) ? $get['sort'] : "ID";
		$order = (isset($get['order']) && !empty($get['order'])) ? $get['order'] : "";

		
        if(!empty($id)){
            $this->db->where('ID', $id);
        }
        
        if(!empty($search)){
            $this->db->group_start();
            $this->db->like('LOWER("kch"."NAMA_PENGIRIM")', strtolower($search));
            $this->db->or_like('LOWER("kch"."CATATAN")', strtolower($search));
            $this->db->or_like('LOWER("kch"."NAMA_PENERIMA")', strtolower($search));
            $this->db->or_like('LOWER("kch"."NAMA_STATUS")', strtolower($search));
            $this->db->group_end();
        }
        
        $this->db->where('ID_PENERIMA',$this->session->id_user);
        // $this->db->or_where('ID_PENGIRIM',$this->session->id_user);
        // $this->db->select('kch.ID,kch.NAMA_PENGIRIM,kch.NAMA_PENERIMA,kch.CATATAN,kch.STATUS,as.NAMA_STATUS');
        // $this->db->join('ADM_STATUS as','kch.STATUS = as.KODE');
        $data['total'] = $this->Keuangan_m->getData($id,'KEU_CATATAN_HEADER kch')->num_rows();

        //////////////////////////
        
        
        if(!empty($id)){
            $this->db->where('ID', $id);
        }
        
        if(!empty($search)){
            $this->db->group_start();
            $this->db->like('LOWER("kch"."NAMA_PENGIRIM")', strtolower($search));
            $this->db->or_like('LOWER("kch"."CATATAN")', strtolower($search));
            $this->db->or_like('LOWER("kch"."NAMA_PENERIMA")', strtolower($search));
            $this->db->or_like('LOWER("kch"."NAMA_STATUS")', strtolower($search));
            $this->db->group_end();
        }
        
        if(!empty($order)){
            $this->db->order_by($sort, $order);
        }
        
        if(!empty($limit)){
            $this->db->limit($limit,$offset);
        }

        $this->db->where('ID_PENERIMA',$this->session->id_user);
        // $this->db->or_where('ID_PENGIRIM',$this->session->id_user);
        // $this->db->select('kch.ID,kch.NAMA_PENGIRIM,kch.NAMA_PENERIMA,kch.CATATAN,kch.STATUS,as.NAMA_STATUS');
        // $this->db->join('ADM_STATUS as','kch.STATUS = as.KODE','left');
		$data['rows'] = $this->Keuangan_m->getData($id,'KEU_CATATAN_HEADER kch')->result_array();
		
		foreach ($data['rows'] as $key => $value) {
            
            $data['rows'][$key]['ID'] = str_replace(['/', '=', '+', '@'],['miringmiring', 'samasama', 'plusplus', 'et'],$this->encryption->encrypt($value['ID']));			
			// print_r($data['rows'][$key]['id']); die();
			$data['rows'][$key]['STATUS'] = str_replace([1, 0, 2,3], ['Belum Dikerjakan', 'Dalam Proses' , 'Sudah Dikerjakan','Selesai'], $value['STATUS']);	
        }
		echo json_encode($data);
    }

    public function getDataUserStatus(){
        $this->db->select('ID,NAMA');
        $data['user'] = $this->Keuangan_m->getData('','PR_USER')->result_array();
        foreach ($data['user'] as $key => $value) {
            $data['user'][$key]['ID'] = str_replace(['/', '=', '+', '@'],['miringmiring', 'samasama', 'plusplus', 'et'],$this->encryption->encrypt($value['ID']));
        };
        $this->db->where('TIPE','catatan');
        $data['status'] = $this->Keuangan_m->getData('','ADM_STATUS')->result();

        echo json_encode($data);
    }
    
    public function getDataUserStatusCatatan($id){
        
        $this->db->select('ID,NAMA');
        $data['user'] = $this->Keuangan_m->getData('','PR_USER')->result_array();
        
        $this->db->where('TIPE','catatan');
        $data['status'] = $this->Keuangan_m->getData('','ADM_STATUS')->result();
        
        $decrypt_id = $this->encryption->decrypt(str_replace(['miringmiring', 'samasama', 'plusplus', 'et'], ['/', '=', '+', '@'], $id));
        $data['catatan'] = $this->Keuangan_m->getData($decrypt_id,'KEU_CATATAN_HEADER')->result();

        echo json_encode($data);
    }

    public function add(){
        $this->form_validation->set_rules('catatan', 'Catatan', 'required|trim');
		$this->form_validation->set_rules('penerima', 'Nama Penerima', 'required|trim');
		$post_xss = $this->security->xss_clean($this->input->post());

		if ($this->form_validation->run() == FALSE) {
			$hasil = [
                'error' => validation_errors()
            ];
		} else {
            $decrypt_idPenerima = $this->encryption->decrypt(str_replace(['miringmiring', 'samasama', 'plusplus', 'et'], ['/', '=', '+', '@'], $post_xss['penerima']));
            $namaPenerima = $this->Keuangan_m->getData($decrypt_idPenerima,'PR_USER')->row()->NAMA;
			
            $idPengirim = $this->session->id_user;
            $namaPengirim = $this->Keuangan_m->getData($idPengirim,'PR_USER')->row()->NAMA;

            $data = [
				"CATATAN" => $post_xss["catatan"],
				"ID_PENERIMA" => $decrypt_idPenerima,
				"NAMA_PENERIMA" => $namaPenerima,
				"ID_PENGIRIM" => $idPengirim,
				"NAMA_PENGIRIM" => $namaPengirim,
				"STATUS" => 1,
				"NAMA_STATUS" => 'Belum Dikerjakan',
			];

			$insert = $this->Keuangan_m->insertData($data,'KEU_CATATAN_HEADER');

			if ($insert) {
                $hasil = [
                    'sukses'=>'Berhasil Menambah Data!'
                ];
            }else{
                $hasil = [
                    'error' => 'Eror 404'
                ];
            }
		}

		echo json_encode($hasil);
    }

    public function edit(){
        $post_xss = $this->security->xss_clean($this->input->post());

        $decrypt_id = $this->encryption->decrypt(str_replace(['miringmiring', 'samasama', 'plusplus', 'et'], ['/', '=', '+', '@'], $post_xss['id']));
        
		$this->form_validation->set_rules('status', 'Kode Status', 'required|trim');
            if($this->form_validation->run()==FALSE){
                $hasil = [
                    'error' => validation_errors()
                ];
            }else{
                $this->db->where('TIPE','catatan');
                $this->db->where('KODE',$post_xss["status"]);
                $namaStatus = $this->Keuangan_m->getData('','ADM_STATUS')->row()->NAMA_STATUS;
                
                $data = array(
					"STATUS" => $post_xss["status"],
                    "NAMA_STATUS" => $namaStatus
				);

                $update = $this->Keuangan_m->updateData($decrypt_id,$data,'KEU_CATATAN_HEADER');
                
                if ($update) {
                    $hasil = [
                        'sukses'=>'Berhasil Mengubah Data!'
                    ];
                }else{
                    $hasil = [
                        'error' => 'Eror 404'
                    ];
                }
               
            }
            
            echo json_encode($hasil);
    }
}