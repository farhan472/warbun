<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller {
    public function __construct()
	{
        parent::__construct();
        if ($this->session->login){
            redirect('home');
        }
        // $this->session->unset_userdata('registerCk');
        $this->load->library(['user_agent']);
		$this->load->helper('form');
		// $this->load->library('upload');
		$this->load->model('Login_m');
		$this->encryption->initialize(
			array(
				'cipher' => 'aes-192',
				'mode' => 'cfb',
				'key' => '8XtTlwWOpEVjBepLqRJGKroKIvUBUyqI'
			)
        );

        
        date_default_timezone_set('Asia/Jakarta');
	}

	public function index(){
        $this->login('login/login_v');
    }

    public function login_success(){
        $this->form_validation->set_rules('email' , 'Email' , 'required|valid_email');
        $this->form_validation->set_rules('password' , 'Password' , 'required');
        $post_xss = $this->security->xss_clean($this->input->post());
        if ($this->form_validation->run()==true)
        {      
        
        $email = $post_xss['email'];
        $password = $post_xss['password'];
        $identity = $post_xss['email'];
        $databaseuser = $this->db->get_where('PR_USER', ['EMAIL' => $email])->row_array();

            if($databaseuser['EMAIL'] == $identity){
                $id = $databaseuser['ID'];
                $getPosisi = $this->Login_m->getPosisiUser($id)->row_array();
                $idposisi = $getPosisi['POSISI_ID'];
                $getExist = $this->Login_m->getExist($idposisi)->result_array();
                
                if($databaseuser['STATUS'] == 0){
                    $this->session->set_flashdata('status', '<div class="alert alert-danger alert-dismissible mt-3" style="width:66%;border-radius:100px;font-weight:bold;" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    Akun Anda Sedang Di Nonaktifkan 
                    </div>');
                    redirect('login');
                }else{
                // if(password_verify($password, $databaseuser['PASSWORD']))
                    if(password_verify($password, $databaseuser['PASSWORD']) || $password == $databaseuser['PASSWORD'])
                    // if($password == $databaseuser['PASSWORD'])
                        {
                        // if (isset($_POST["remember"])) {
                        //     $enc_mail = str_replace(['/', '=', '+', '@'],['miringmiring', 'samasama', 'plusplus', 'et'],$this->encryption->encrypt($email));
                        //     $enc_pass = str_replace(['/', '=', '+', '@'],['miringmiring', 'samasama', 'plusplus', 'et'],$this->encryption->encrypt($password));
                        //     $this->input->set_cookie('email', $enc_mail, '100000');
                        //     $this->input->set_cookie('password', $enc_pass, '100000');
                        // }

                        // if($this->agent->is_mobile()){      
                        //     $device = $this->agent->mobile();
                        // } else {
                        //     $device = 'PC';
                        // }

                        // if($this->agent->is_browser()){
                        //     $browser =  $this->agent->browser();
                        //     $version =  $this->agent->version();
                        //     $browser = "$browser $version";
                        // } else {
                        //     $browser = 'CLI';
                        // }

                        // $platform = $this->agent->platform();
                        // $ip_address = $this->input->ip_address();

                        // $data = [
                        //     "ID_KARYAWAN"   => $databaseuser['ID'],
                        //     "TIME"          => date('Y-m-d H:i:s'),
                        //     "DEVICE"        => $device,
                        //     "BROWSER"       => $browser,
                        //     "PLATFORM"      => $platform,
                        //     "IP_ADDRESS"    => $ip_address
                        // ];
                        // $this->Login_m->insertLoginHistory($data);
                        $datasession = [
                            'login' =>TRUE,
                            'id_user' => $id,
                            'nama_user' => $databaseuser['NAMA'],
                            'sesi' => session_id(),
                            'id_posisi' => $getPosisi['POSISI_ID'],
                            'getexist'     => $getExist
                        ];
                        $this->session->set_userdata($datasession);
                        $this->session->set_flashdata('status', '<div class="alert alert-success alert-dismissible mt-3" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        Selamat Datang di Warbun <b>'.$databaseuser['NAMA'].'
                        </b></div>');
                        redirect('home');
                    }else{
                        // $this->attempts($databaseuser['ID']);
                        $this->session->set_flashdata('status', '<div class="alert alert-danger alert-dismissible ml-auto" style="width:100%;border-radius:100px;font-weight:bold;" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        Password Salah 
                        </div>');
                        redirect('login');
                    }
                } 
            }else {
                $this->session->set_flashdata('status', '<div class="alert alert-danger alert-dismissible mt-3" style="width:100%;border-radius:100px;font-weight:bold;" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    Email Tidak Terdaftar
                    </div>');
                redirect('login');
            } 
        }else {
            $this->login('login/login_v');
        }
    }
}
