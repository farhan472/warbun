<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends MY_Controller {
    public function __construct()
	{
		parent::__construct();
		$this->load->helper('form');
		$this->load->model('Register_m');
		$this->encryption->initialize(
			array(
				'cipher' => 'aes-192',
				'mode' => 'cfb',
				'key' => '8XtTlwWOpEVjBepLqRJGKroKIvUBUyqI'
			)
		);
    }
    
	public function index(){
        $this->login('register/register_v');
    }

    public function register_success()
	{
		$this->form_validation->set_rules('email', 'Email', 'required|trim|is_unique[PR_USER.EMAIL]');
		$this->form_validation->set_rules('nama', 'Nama', 'required|trim');
		$this->form_validation->set_rules('alamat', 'Alamat', 'required|trim');
		$this->form_validation->set_rules('no_telp', 'Nomor Telepon', 'required|trim|numeric|max_length[15]|is_unique[PR_USER.NO_TELP]');
		$post_xss = $this->security->xss_clean($this->input->post());

		if ($this->form_validation->run() == FALSE) {
			$this->login('register/register_v');
		} else {
			$kode = random_string('numeric', 6);
			$datasession = [
				'register' =>TRUE,
				'EMAIL' =>  $post_xss['email'],
				'NAMA' =>  $post_xss['nama'],
				'ALAMAT'  => $post_xss['alamat'], 
				'NO_TELP' => $post_xss['no_telp'],
				'KODE' => $kode,
				'sesi' =>  session_id(),
			];
			$this->session->set_userdata($datasession);
			
			if ($this->session->register) {
				$penerima= $this->session->userdata('EMAIL');
				$subject ='Pembuatan Akun di Website Warbun' ;
				$pesan = 'Silahkan Copy Kode yang disediakan dan Klik Link' ;
				$nama=  $this->session->userdata('NAMA');
				
				$send_email = $this->send_email($penerima,$subject,$pesan,$nama,$kode);               
				
				if ($send_email) {
					$this->session->set_flashdata('status', '<div class="alert alert-success alert-dismissible mt-3" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					Silahkan Cek Email Anda
					</div>');
					redirect(site_url('register/create_password'));
				}else{
					$this->session->set_flashdata('status', 'Gagal Mendaftar');
					$this->login('register/register_v');
				}
				
			} else {
				$this->session->set_flashdata('status', 'Gagal Mendaftar');
				$this->login('register/register_v');
			}
		}
	}
	
	public function create_password(){
		if ($this->session->register){
			$this->login('register/create_password_v');
        }else {
            redirect('login');
		}
	}
	
	public function create_password_success()
	{
		$this->form_validation->set_rules('kode', 'Email', 'required|trim|numeric');
		$this->form_validation->set_rules('password', 'Password', 'required|trim');
		$this->form_validation->set_rules('password2', 'Password Confirmation', 'required|trim|matches[password]');
		$post_xss = $this->security->xss_clean($this->input->post());

		if ($this->form_validation->run() == FALSE) {
			$this->login('register/create_password_v');
		} else {
			$kode = $post_xss['kode'];
			$kodeConfirm = $this->session->KODE;
			// var_dump($this->session->KODE); die();
			if ($kode == $kodeConfirm) {
				$dataUser = [
					"EMAIL" => $this->session->userdata('EMAIL'),
					"ALAMAT" => $this->session->userdata('ALAMAT'),
					"NO_TELP" => $this->session->userdata('NO_TELP'),
					"NAMA" => $this->session->userdata('NAMA'),
					"POSISI_ID" => 2,
					"PASSWORD" =>  password_hash($post_xss['password'], PASSWORD_DEFAULT),
				];
				$insert = $this->Register_m->insertUser($dataUser);

				if ($insert) {
					$penerima= $this->session->userdata('EMAIL');
					$subject ='Pembuatan Akun di Website Warbun' ;
					$pesan = 'Selamat Akun Anda Sudah Terdaftar' ;
					$nama=  $this->session->userdata('NAMA');
					$kode = '';
					$this->session->set_userdata('KODE',$kode);
					$this->session->set_flashdata('status', '<div class="alert alert-success alert-dismissible mt-3" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					Silahkan Cek Email Anda
					</div>');
					$this->send_email($penerima,$subject,$pesan,$nama,$kode);
					$this->session->sess_destroy();               
					redirect(site_url('login'));
				} else {
					$this->session->set_flashdata('status', 'Gagal Mendaftar');
					$this->login('register/create_password_v');
				}
			} else {
				$this->session->set_flashdata('status', '<div class="alert alert-danger alert-dismissible mt-3" style="width:66%;border-radius:100px;font-weight:bold;" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				Kode Salah
				</div>');
				redirect('register/create_password');
			}
		
		}
	}
}