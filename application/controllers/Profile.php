<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends MY_Controller {
    public function __construct()
	{
		parent::__construct();
		if (!$this->session->login){
            redirect('login');
        }
		$this->load->helper('form');
		$this->load->model('Profile_m');
		$this->encryption->initialize(
			array(
				'cipher' => 'aes-192',
				'mode' => 'cfb',
				'key' => '8XtTlwWOpEVjBepLqRJGKroKIvUBUyqI'
				)
			);
		// $exist = $this->session->getexist;
		// if(!is_numeric(array_search('Profile', array_column($exist, 'URL')))){
		// redirect('home');
		// }
    }
    
	public function index($id){
        $decrypt_id = $this->encryption->decrypt(str_replace(['miringmiring', 'samasama', 'plusplus', 'et'], ['/', '=', '+', '@'], $id));
        $data['data'] = $this->Profile_m->getUser($decrypt_id)->row_array();
        $data['dataPosisi'] = $this->Profile_m->getPosisi()->result_array();
		$data['id'] = $id;
		$this->template('profile/edit_profile_v', $data);
    }


	public function submitEditProfile($id)
	{
		$post_xss = $this->security->xss_clean($this->input->post());
		$decrypt_id = $this->encryption->decrypt(str_replace(['miringmiring', 'samasama', 'plusplus', 'et'], ['/', '=', '+', '@'], $id));

		$email = $this->db->query('SELECT "EMAIL" FROM public."PR_USER" WHERE "ID" = '.$decrypt_id)->row()->EMAIL;
		$no_telp = $this->db->query('SELECT "NO_TELP" FROM public."PR_USER" WHERE "ID" = '.$decrypt_id)->row()->NO_TELP;
		if ($no_telp != $post_xss['no_telp'])
        {
			$this->form_validation->set_rules('no_telp', 'Nomor Telepon', 'required|trim|numeric|max_length[15]|is_unique[PR_USER.NO_TELP]');    
		}
		else if ($email != $post_xss['email'] )
        {
				$this->form_validation->set_rules('email', 'Email', 'required|trim|is_unique[PR_USER.EMAIL]');    
		}
		else if ($email != $post_xss['email'] && $no_telp != $post_xss['no_telp'] )
        {
				$this->form_validation->set_rules('email', 'Email', 'required|trim|is_unique[PR_USER.EMAIL]');    
				$this->form_validation->set_rules('no_telp', 'Nomor Telepon', 'required|trim|numeric|max_length[15]|is_unique[PR_USER.NO_TELP]');    
		} else 
        {
            $this->form_validation->set_rules('email', 'Email', 'required|trim');
            $this->form_validation->set_rules('nama', 'Nama', 'required|trim');
            $this->form_validation->set_rules('alamat', 'Alamat', 'required|trim');
            $this->form_validation->set_rules('no_telp', 'Nomor Telepon', 'required|trim|numeric|max_length[15]');
           
            if ($_FILES['foto']['error'] == 2 ) {
                $this->form_validation->set_rules('foto', 'Foto', 'required', ['required' => 'File melebihi 2MB']);
            }
            if ($_FILES['foto']['error'] == 0 ) {
                $this->form_validation->set_rules('foto', 'Foto', 'callback_file_gambar');
            }
        }
		if ($this->form_validation->run() == FALSE) {
         	$data['id'] = $id;
            $data['data'] = $this->Profile_m->getUser($decrypt_id)->row_array();
            $data['dataPosisi'] = $this->Profile_m->getPosisi()->result_array();
            $this->template('profile/edit_profile_v', $data);
		} else {
            if ($_FILES['foto']['error'] == 0) {
				$file = $_FILES['foto'];
				$upload = $this->uploadFoto($file);
				$data['file_name'] = $upload['file_name'];
				$data['orig_name'] = $upload['orig_name'];

				$foto_profile =array("FOTO"=>$data['file_name'],"FOTO_ORIGNAME"=>$data['orig_name']);
            }
            
			$dataUser = [
				"EMAIL" => $post_xss["email"],
				"ALAMAT" => $post_xss["alamat"],
				"NO_TELP" => $post_xss["no_telp"],
				"NAMA" => $post_xss["nama"],
            ];

            
            if(!empty($foto_profile)) {
				$gambar = $this->Profile_m->getUser($decrypt_id)->row_array();
				$file_gambar = FCPATH . "uploads/fotoProfile/".$gambar['FOTO'];
				unlink($file_gambar);
				$merge = array_merge($foto_profile,$dataUser);
            } else {
                $merge = $dataUser;
            }
            
			$update = $this->Profile_m->updateUser($decrypt_id,$merge);
			if ($update) {
				
				$this->session->set_flashdata('status', '<div class="alert alert-success alert-dismissible mt-3" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				Berhasil Mengubah Profile
				</div>');
				redirect(site_url('profile/index/'.$id));
			} else {
                $this->session->set_flashdata('status', 'Gagal mengubah profile');
                $data['dataPosisi'] = $this->Profile_m->getPosisi()->result_array();
                $data['id'] = $id;
                $data['data'] = $this->Profile_m->getUser($decrypt_id)->row_array();
				$this->template('profile/edit_profile_v', $data);
				// redirect('Profile/editProfile/' . $id);
			}
		}
    }
    
    private function uploadFoto($file) {
		$dir = FCPATH . "uploads/fotoProfile";
		$dir = str_replace(array("\\", "//"), "/", $dir);
		$config['upload_path'] 		= $dir;
		$config['allowed_types'] 	= 'jpg|jpeg|png';
		$config['file_name']		= $file['name'];
		$config['encrypt_name'] 	= true;
		$config['overwrite'] 		= true;
		$this->load->library('upload');
		$this->upload->initialize($config);
		
		if (!file_exists($dir)) {
            mkdir($dir, 777, true);//root
        }

        if ($this->upload->do_upload('foto')) {
			$gbr = $this->upload->data();
			//Compress Image
			$config['image_library']='gd2';
			$config['source_image']='./uploads/fotoProfile/'.$gbr['file_name'];
			$config['create_thumb']= FALSE;
			$config['maintain_ratio']= FALSE;
			$config['quality']= '100%';
			$config['width']= 300;
			$config['height']= 300;
			$config['new_image']= './uploads/fotoProfile/'.$gbr['file_name'];
			$this->load->library('image_lib', $config);
			$this->image_lib->resize();
			return $this->upload->data();
        } else {
            return $this->upload->display_errors();
        }
    }
    
    public function file_gambar($str){
		$allowed_mime_type_arr = array('image/jpg','image/jpeg','image/png');
		$mime = get_mime_by_extension($_FILES['foto']['name']);
		if(isset($_FILES['foto']['name']) && $_FILES['foto']['name']!=""){
			 if(in_array($mime, $allowed_mime_type_arr)){
				  return true;
			 }else{
				  $this->form_validation->set_message('foto', 'Hanya mendukung format file jpg/jpeg/png');
				  return false;
			 }
		}else{
			 $this->form_validation->set_message('foto', 'Please choose a file to upload.');
			 return false;
		}
	}
}
