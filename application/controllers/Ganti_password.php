<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ganti_password extends MY_Controller {
    public function __construct()
	{
		parent::__construct();
		if (!$this->session->login){
            redirect('login');
        }
		$this->load->helper('form');
		$this->load->model('Profile_m');
		$this->encryption->initialize(
			array(
				'cipher' => 'aes-192',
				'mode' => 'cfb',
				'key' => '8XtTlwWOpEVjBepLqRJGKroKIvUBUyqI'
			)
		);
		// $exist = $this->session->getexist;
		// 	if(!is_numeric(array_search('domisili', array_column($exist, 'URL')))){
		// 		redirect('home');
		// 	}
		
    }
    
	public function index($id){
        $data['judul'] = 'Ganti Password';
        $decrypt_id = $this->encryption->decrypt(str_replace(['miringmiring', 'samasama', 'plusplus', 'et'], ['/', '=', '+', '@'], $id));
		$data['id'] = $id;  
        $this->template('ganti_password/ganti_password_v',$data);
    }

	public function submitGantiPassword($id)
	{
        $decrypt_id = $this->encryption->decrypt(str_replace(['miringmiring', 'samasama', 'plusplus', 'et'], ['/', '=', '+', '@'], $id));
        $databaseuser = $this->Profile_m->getUser($decrypt_id)->row_array();
        $data['id'] = $id;
        
        
        $this->form_validation->set_rules('password_lama', 'Password Lama', 'required|trim');
        $this->form_validation->set_rules('password', 'Password Baru', 'required|trim');
        $this->form_validation->set_rules('password2', 'Password Confirmation', 'required|trim|matches[password]');
        $post_xss = $this->security->xss_clean($this->input->post());
        
        if ($this->form_validation->run() == FALSE) {
            $data['id'] = $id;
            $this->template('ganti_password/ganti_password_v',$data);
        } else {
            $password = $post_xss['password_lama'];
            // if(password_verify($password, $databaseuser['PASSWORD']) || $password == $databaseuser['PASSWORD'])
            if(password_verify($password, $databaseuser['PASSWORD']))
            // if($password == $databaseuser['PASSWORD'])
            {
                $data = [
                    "PASSWORD" => password_hash($post_xss['password'], PASSWORD_DEFAULT)
                ];
                
                $insert = $this->Profile_m->gantiPassword($decrypt_id,$data);
                
                if ($insert) {
                    $this->session->set_flashdata('status', '<div class="alert alert-success alert-dismissible mt-3" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    Berhasil Mengubah Password
                    </div>');
                    redirect(site_url('ganti_password/index/'.$id));
                } else {
                    $this->session->set_flashdata('status', 'Gagal Mengubah Password');
                    $this->template('ganti_password/ganti_password_v');
                }
            }else{
                $this->session->set_flashdata('status', '<div class="alert alert-danger alert-dismissible mt-3" style="width:66%;border-radius:100px;font-weight:bold;" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                Password Lama Anda Salah
                </div>');
                redirect('ganti_password/index/'.$id);
            }
        }
	}

}
