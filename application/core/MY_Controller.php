<?php
defined('BASEPATH') or exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('MY_Model');
		$this->encryption->initialize(
			array(
				'cipher' => 'aes-192',
				'mode' => 'cfb',
				'key' => '8XtTlwWOpEVjBepLqRJGKroKIvUBUyqI'
			)
		);
	}
	
	protected function template($view, $data = array())
	{
		$idPosisi = $this->session->id_posisi;	
		$idUser = $this->session->id_user;	
		$data['idUser'] = str_replace(['/', '=', '+', '@'],['miringmiring', 'samasama', 'plusplus', 'et'],$this->encryption->encrypt($idUser));
		$data['dataUser'] = $this->MY_Model->getDataUser($idUser)->row_array();
		$data['top'] = $this->MY_Model->getTop($idPosisi)->result_array();
		$data['checkChild'] = $this->MY_Model->getCheckMenu()->result_array();
		foreach ($data['checkChild'] as $key => $value) {
			$data['checkChild'][$key]['PARENT_ID'] = str_replace([NULL], [0], $value['PARENT_ID']);	
		}
		$data['child'] = $this->MY_Model->getTop($idPosisi,'child')->result_array();
		$data['title'] = $this->MY_Model->getURL()->result_array();
		$this->load->view('templates/header', $data);
		$this->load->view('templates/sidebar');
		$this->load->view($view, $data);
		$this->load->view('templates/footer', $data);
	}

	// protected function template_user($view, $data = array())
	// {
	// 	$idPosisi = $this->session->id_posisi;	
	// 	$idUser = $this->session->id_user;	
	// 	$data['idUser'] = str_replace(['/', '=', '+', '@'],['miringmiring', 'samasama', 'plusplus', 'et'],$this->encryption->encrypt($idUser));
	// 	$data['dataUser'] = $this->Login_m->getDataUser($idUser)->row_array();
	// 	$data['top'] = $this->Login_m->getTop($idPosisi)->result_array();
	// 	$data['checkChild'] = $this->Login_m->getCheckMenu()->result_array();
	// 	foreach ($data['checkChild'] as $key => $value) {
	// 		$data['checkChild'][$key]['PARENT_ID'] = str_replace([NULL], [0], $value['PARENT_ID']);	
    //     }
	// 	$data['childKeuangan'] = $this->Login_m->getChildKeuangan($idPosisi)->result_array();
	// 	$data['childMaster'] = $this->Login_m->getChildMaster($idPosisi)->result_array();
	// 	$data['title'] = $this->Login_m->getURL()->result_array();
	// 	$this->load->view('templates_user/header', $data);
	// 	$this->load->view('templates_user/navbar');
	// 	$this->load->view($view, $data);
	// 	$this->load->view('templates_user/footer', $data);
	// }
	
	protected function login($view, $data = array())
	{
		$data['title'] = $this->MY_Model->getURL()->result_array();
		$this->load->view('templates/header', $data);
		// $this->load->view('templates/navbar');
		$this->load->view($view, $data);
		// $this->load->view('templates/footer', $data);
	}

	protected function send_email($penerima= '', $subject ='' ,$pesan = '',$nama='' ,$kode = ''){
		$this->load->library('email');
		$config = [
			'mailtype' => 'html',
			'charset' => 'utf-8',
			'protocol' => 'smtp',
			'smtp_host' => 'ssl://smtp.gmail.com',
			'smtp_user' => 'arfianto472@gmail.com',
			// 'smtp_pass' => 'ctbqtkzrdvqtrfmb',
			'smtp_pass' => 'wvspcwjevoehoslp',
			'smtp_port' => '465',
			'wordwrap' => TRUE,
			'crlf' => "\r\n",
			'newline' => "\r\n"
		];

		$this->email->initialize($config);

		$this->load->library('parser');
		$data['message'] = $pesan;
		$data['judul'] = $subject;
		$data['head'] = $subject;
		$data['nama'] = $nama;
		$data['kode'] = $kode;
		// $data['gambar'] = base_url('assets/images/Email.png');
		$data['gambar'] = 'https://raw.githubusercontent.com/ColorlibHQ/email-templates/master/10/images/email.png';

		// $this->output->set_header('content-type: text/css');
		// $this->email->set_header('Content-Type', 'text/html');

		$body = $this->parser->parse("register/emailTemplateRegister",$data,true);

		$this->email->from('arfianto472@gmail.com','No-reply');
		$this->email->to($penerima);
		$this->email->subject($subject);
		$this->email->message($body);

		return $this->email->send();
	}
}

/* End of file MY_Controller.php */
/* Location: ./application/core/MY_Controller.php */