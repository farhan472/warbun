<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile_m extends CI_Model {

	public function updateUser($id, $data){
		$this->db->where('ID', $id);
		return $this->db->update('PR_USER', $data);
	}
	
	public function gantiPassword($id, $data){
		$this->db->where('ID', $id);
		return $this->db->update('PR_USER', $data);
	}

	public function getUser($id){
		$this->db->where('ID', $id);
		return $this->db->get('PR_USER');
    }
    
    public function getPosisi(){
		return $this->db->get('ADM_POSISI');
	}

	public function checkUser($key)
	{
	 $this->db->like('LOWER("KODE")', strtolower($key), 'none');
	 $query = $this->db->get('PR_USER');
		if (!empty($query->result_array())){
				return FALSE;
		}
		else{
				return TRUE;
		}
	}
}