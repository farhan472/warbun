<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Barang_m extends CI_Model {

	public function insertBarang($data){
		
		$this->db->insert('ADM_BARANG', $data);
		return $this->db->affected_rows();
	}

	public function insertGambar($dataGambar){
		$this->db->insert('PR_GAMBAR_BARANG', $dataGambar);
		return $this->db->affected_rows();
	}

	public function getBarang($id="", $limit=0, $offset=0, $search="",$patterns=[], $replacements=[]){
        $this->db->join('ADM_BRAND AS BRAND' , 'BRAND.ID = ADM_BARANG.ID_BRAND');
		$this->db->select('ADM_BARANG.*, BRAND.NAMA_BRAND');
        
        if(!empty($id)){
			$this->db->where('ADM_BARANG.ID', $id);
		}

		if(!empty($search)){
			$this->db->group_start();
			$this->db->like('LOWER("ADM_BARANG"."NAMA")', strtolower($search));
			$this->db->or_like('LOWER("BRAND"."NAMA_BRAND")', strtolower($search));
			$this->db->or_like('LOWER("ADM_BARANG"."KODE")', strtolower($search));
			$this->db->or_like('LOWER("ADM_BARANG"."DESKRIPSI")', strtolower($search));
			$this->db->or_like('CAST("ADM_BARANG"."JUMLAH_BARANG" AS TEXT)', strtolower($search));
			$this->db->or_like('CAST("ADM_BARANG"."STATUS" AS VARCHAR)', preg_replace($patterns, $replacements,strtolower($search)));
			$this->db->group_end();
		
		}

		return $this->db->order_by("ID", 'asc')->get("ADM_BARANG", $limit, $offset, $search);
	}

	public function getGambar($id){
		$this->db->join('PR_GAMBAR_BARANG', 'PR_GAMBAR_BARANG.ID_BARANG = ADM_BARANG.ID');	
		$this->db->where("ADM_BARANG.ID", $id);
		$this->db->order_by("PR_GAMBAR_BARANG.MAIN_GAMBAR", "desc");
		return $this->db->get("ADM_BARANG");
	}
	
	public function updateBarang($id, $data){
		$this->db->where('ID', $id);
		return $this->db->update('ADM_BARANG', $data);
	}

	public function editBarang($id){
		$this->db->where('ID', $id);
		return $this->db->get('ADM_BARANG');
	}

	public function aktif($id,$data){
		$this->db->where('ID', $id);
		$this->db->update('ADM_BARANG', $data);
		return $this->db->affected_rows();
	}
	
	public function getBrand(){
		$this->db->where('STATUS', 1);
		return $this->db->get('ADM_BRAND');
	}
	
	public function checkBarang($key)
	{
	 $this->db->like('LOWER("KODE")', strtolower($key), 'none');
	 $query = $this->db->get('ADM_BARANG');
		if (!empty($query->result_array())){
				return FALSE;
		}
		else{
				return TRUE;
		}
	}

	public function aktifMain($id,$data){
		$this->db->where('ID', $id);
		$this->db->update('PR_GAMBAR_BARANG', $data);
		return $this->db->affected_rows();
	}
	
	public function nonaktifMain($id,$data){
		$this->db->where('ID_BARANG', $id);
		$this->db->update('PR_GAMBAR_BARANG', $data);
		return $this->db->affected_rows();
	}

	public function deleteDataBarang($id){ 
		$this->db->where("ID", $id);
		return $this->db->delete('ADM_BARANG');
	}

	public function deleteGambar($file){ 
		$this->db->where("FILE_NAME", $file);
		return $this->db->delete('PR_GAMBAR_BARANG');
	}
}