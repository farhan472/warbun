<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Posisi_m extends CI_Model {

	public function insertposisi($data){
		
		$this->db->insert('ADM_POSISI', $data);
		return $this->db->affected_rows();
	}

	public function getPosisi($id="", $limit=0, $offset=0, $search="",$patterns=[], $replacements=[]){
		if(!empty($id)){
			$this->db->where('ID', $id);
		}

		if(!empty($search)){
			$this->db->group_start();
			$this->db->like('LOWER("NAMA_POSISI")', strtolower($search));
			$this->db->or_like('CAST("STATUS" AS VARCHAR)', preg_replace($patterns, $replacements,strtolower($search)));
			$this->db->group_end();
		
		}

		return $this->db->order_by("ID", 'asc')->get("ADM_POSISI", $limit, $offset, $search);
	}
	
	public function updatePosisi($id, $data){
		$this->db->where('ID', $id);
		return $this->db->update('ADM_POSISI', $data);
		
	}

	public function editPosisi($id){
		$this->db->where('ID', $id);
		return $this->db->get('ADM_POSISI');
	}

	public function aktif($id,$data){
		$this->db->where('ID', $id);
		$this->db->update('ADM_POSISI', $data);
		return $this->db->affected_rows();
	}

}