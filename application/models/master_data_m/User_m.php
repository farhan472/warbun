<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_m extends CI_Model {

	public function insertUser($data){
		
		$this->db->insert('PR_USER', $data);
		return $this->db->affected_rows();
	}

	public function getUser($id="", $limit=0, $offset=0, $search="",$patterns=[], $replacements=[]){
        $this->db->join('ADM_POSISI AS POSISI' , 'POSISI.ID = PR_USER.POSISI_ID');
		$this->db->select('PR_USER.*, POSISI.NAMA_POSISI');
        
        if(!empty($id)){
			$this->db->where('PR_USER.ID', $id);
		}

		if(!empty($search)){
			$this->db->group_start();
			$this->db->like('LOWER("PR_USER"."NAMA")', strtolower($search));
			$this->db->or_like('LOWER("POSISI"."NAMA_POSISI")', strtolower($search));
			$this->db->or_like('LOWER("PR_USER"."EMAIL")', strtolower($search));
			$this->db->or_like('LOWER("PR_USER"."ALAMAT")', strtolower($search));
			$this->db->or_like('LOWER("PR_USER"."NO_TELP")', strtolower($search));
			$this->db->or_like('CAST("PR_USER"."STATUS" AS VARCHAR)', preg_replace($patterns, $replacements,strtolower($search)));
			$this->db->group_end();
		
		}

		return $this->db->order_by("ID", 'asc')->get("PR_USER", $limit, $offset, $search);
	}

	public function getDataUser($id){
		$this->db->where('ID', $id);
		return $this->db->get('PR_USER');
    }
	
	public function getPosisi(){
		$this->db->where('STATUS', 1);
		return $this->db->get('ADM_POSISI');
	}
	
	public function updateUser($id, $data){
		$this->db->where('ID', $id);
		return $this->db->update('PR_USER', $data);
		
	}

	public function editUser($id){
		$this->db->where('ID', $id);
		return $this->db->get('PR_USER');
	}

	public function gantiPassword($id, $data){
		$this->db->where('ID', $id);
		return $this->db->update('PR_USER', $data);
	}

	public function aktif($id,$data){
		$this->db->where('ID', $id);
		$this->db->update('PR_USER', $data);
		return $this->db->affected_rows();
	}

	public function checkUser($key)
	{
	 $this->db->like('LOWER("KODE")', strtolower($key), 'none');
	 $query = $this->db->get('PR_USER');
		if (!empty($query->result_array())){
				return FALSE;
		}
		else{
				return TRUE;
		}
	}
}