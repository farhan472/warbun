<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Menu_management_m extends CI_Model {

    public function getPosisi(){
		$this->db->where('STATUS', 1);
		$this->db->order_by('ADM_POSISI.NAMA_POSISI', 'asc');
		return $this->db->get('ADM_POSISI');
	}

	// public function getPosisi(){
	// 	return $this->db->where('STATUS', 1)->get('ADM_POSISI');
	// }
	
	public function getParentPosisi($id){
		$this->db->select('ADM_MENU_MANAGEMENT.*');
		return $this->db->where('ADM_MENU_MANAGEMENT'.'.MENU_ID',$id)->get('ADM_MENU_MANAGEMENT');
    }
    
    public function updateHirarki($id, $data, $nama_table){
		$this->db->where('ID', $id);
		return $this->db->update($nama_table, $data);
		
	}

	public function getMenu(){
		$this->db->select('ADM_MENU_MANAGEMENT.*');
		return $this->db->order_by('ADM_MENU_MANAGEMENT.NO_URUT', 'asc')->where('ADM_MENU_MANAGEMENT.PARENT_ID', NULL)->get('ADM_MENU_MANAGEMENT');
	}

	public function getParent($id_parent){
		$this->db->select('ADM_MENU_MANAGEMENT.*');
		return $this->db->order_by('ADM_MENU_MANAGEMENT'.'.MENU_ID', 'asc')->where('ADM_MENU_MANAGEMENT.PARENT_ID', $id_parent)->get('ADM_MENU_MANAGEMENT');
	}

	public function getNamaHirarki(){
		$this->db->order_by('ID');
		return $this->db->get('ADM_NAMA_HIRARKI');
	}

	public function getMenuRelasi($id){
		return $this->db->where('POSISI_ID', $id)->get('R_POSISI_MENU');
	}

	public function deleteHirarkiMenu($id){
		return $this->db->where('POSISI_ID', $id)->delete('R_POSISI_MENU');
	}

	public function insertHirarkiMenu($data){
		$this->db->insert_batch('R_POSISI_MENU', $data);
		return $this->db->affected_rows();
	}

}