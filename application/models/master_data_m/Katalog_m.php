<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Katalog_m extends CI_Model {

	public function insertKatalog($data){
		
		$this->db->insert('ADM_KATALOG', $data);
		return $this->db->affected_rows();
	}

	public function getKatalog($id=""){
        // $this->db->join('ADM_BARANG AS BARANG' , 'BARANG.ID = ADM_KATALOG.ID_BARANG');
        // $this->db->join('ADM_KODE_KATALOG AS KODE' , 'KODE.ID = ADM_KATALOG.KODE');
		
        if(!empty($id)){
			$this->db->where('ADM_KATALOG.ID', $id);
		}

		return $this->db->get("ADM_KATALOG");
	}
	
	public function updateKatalog($id, $data){
		$this->db->where('ID', $id);
		return $this->db->update('ADM_KATALOG', $data);
		
	}

	public function editKatalog($id){
		$this->db->where('ID', $id);

		return $this->db->get('ADM_KATALOG');
	}
	
	public function getBarang(){
		$this->db->where('STATUS',1 );
		return $this->db->get('ADM_BARANG');
	}
	
	public function getKodeKatalog(){
		return $this->db->get('ADM_KODE_KATALOG');
	}

	public function checkKatalog($key)
	{
	 $this->db->like('LOWER("KODE")', strtolower($key), 'none');
	 $query = $this->db->get('ADM_KATALOG');
		if (!empty($query->result_array())){
				return FALSE;
		}
		else{
				return TRUE;
		}
	}
}