<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_data_m extends CI_Model {
	public function getStatus(){
		$this->db->where('TIPE', 'master_data');
		return $this->db->get('ADM_STATUS');
	}

	public function aktif($id,$data,$table=""){
		$this->db->where('ID', $id);
		$this->db->update($table, $data);
		return $this->db->affected_rows();
	}

	public function getData($id="",$table=""){
		if(!empty($id)){
			$this->db->where('ID', $id);
		}
		return $this->db->get($table);
	}
	
	public function insertData($data,$table=""){
		$this->db->insert($table, $data);
		return $this->db->affected_rows();
	}

	public function updateData($id, $data,$table=""){
		$this->db->where('ID', $id);
		return $this->db->update($table, $data);
	}

	public function deleteData($id,$table=""){
        $this->db->where('ID', $id);
        return $this->db->delete($table);
    }
}