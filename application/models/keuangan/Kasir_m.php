<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kasir_m extends CI_Model {

    public function getBarang($id = ""){
        if(!empty($id)){
            $this->db->where('ID',$id);
        }
        $this->db->where('STATUS',1);
        return $this->db->get('ADM_BARANG');
    }

    public function getNoUrut($tahun = ""){
        $tahun = (empty($tahun)) ? date("Y") : $tahun;

        $this->db->select('COUNT("KODE_PEMBELIAN") AS URUT');
        
        if (!empty($tahun)) {
        $this->db->where('EXTRACT(YEAR FROM "CREATED_DATE") =', $tahun, false);
        }

        $get = $this->db->get('KEU_KASIR_HEADER')->row()->urut + 1;
        
        return "WB." . date("Ym") . "." . str_repeat(0, 5-strlen($get)).$get;
    }

    public function getDataBarang($id="",$kode=""){
        if(!empty($id)){
            $this->db->where('ID',$id);
        }
        if(!empty($kode)){
            $this->db->where('KODE',$kode);
        }
        return $this->db->get('ADM_BARANG');
    }
    
    public function getDataHargaBarang($kodePembelian){
        $this->db->where('KODE_PEMBELIAN',$kodePembelian);
        return $this->db->get('VW_KEU_KASIR_MAIN');
    }

    public function getItem($kodePembelian){
        $this->db->where('KODE_PEMBELIAN',$kodePembelian);
        $this->db->order_by('ID','desc');
        return $this->db->get('VW_KEU_KASIR_ITEM');
    }

    public function updateItem($id,$field,$value){
        $data=array($field => $value);
        $this->db->where('ID',$id);
        $this->db->update('KEU_KASIR_ITEM',$data);
        return $this->db->affected_rows();
    }

    public function updateHeader($kode,$data){
        $this->db->where('KODE_PEMBELIAN',$kode);
        $this->db->update('KEU_KASIR_HEADER',$data);
        return $this->db->affected_rows();
    }

    public function updateBarang($kode,$data){
        $this->db->where('KODE',$kode);
        $this->db->update('ADM_BARANG',$data);
        return $this->db->affected_rows();
    }
    
    public function insertKodePembelian($data){
        $this->db->insert('KEU_KASIR_HEADER',$data);
        return $this->db->affected_rows();
    }
    
    public function insertItem($data){
        $this->db->insert('KEU_KASIR_ITEM',$data);
        return $this->db->affected_rows();
    }

    public function checkDataPembelian($idPembelian){
        $this->db->select('B.KODE_PEMBELIAN');
        $this->db->where('A.KODE_PEMBELIAN',$idPembelian);
        $this->db->join('KEU_KASIR_ITEM B','A.KODE_PEMBELIAN = B.KODE_PEMBELIAN','right');
        return $this->db->get('KEU_KASIR_HEADER A');
    }

    public function idPembelian(){
        $this->db->select('KODE_PEMBELIAN');
        $this->db->order_by('KODE_PEMBELIAN','desc');
        return $this->db->get('KEU_KASIR_HEADER');
    }

    public function getDataItem($id){
        $this->db->where('ID',$id);
        return $this->db->get('KEU_KASIR_ITEM');
    }
    
    public function getDataHeader($kodePembelian){
        $this->db->where('KODE_PEMBELIAN',$kodePembelian);
        return $this->db->get('KEU_KASIR_HEADER');
    }

    


}
