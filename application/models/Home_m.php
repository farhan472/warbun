<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home_m extends CI_Model {
	public function getKatalog(){	
		$this->db->join('ADM_BARANG AS BARANG' , 'BARANG.ID = ADM_KATALOG.ID_BARANG','left');
		$this->db->join('PR_GAMBAR_BARANG AS GAMBAR' , 'GAMBAR.ID_BARANG = BARANG.ID and GAMBAR.MAIN_GAMBAR = 1','left');
		$this->db->select('BARANG.*,GAMBAR.FILE_NAME');
		$this->db->where('ADM_KATALOG.KODE', 1);
		$this->db->where('ADM_KATALOG.STATUS', 1);
		$this->db->order_by("BARANG.HARGA_SATUAN", 'asc');
		return $this->db->get('ADM_KATALOG');
	}
}