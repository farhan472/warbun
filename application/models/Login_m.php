<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_m extends CI_Model {
    
    public function getPosisiUser($id){
        $this->db->select('PR_USER.POSISI_ID,PR_USER.ID');
        $this->db->where('PR_USER.ID',$id);
        return $this->db->get('PR_USER');
        
    }

    public function getURL(){
        $this->db->join('ADM_MENU_MANAGEMENT','ADM_MENU_MANAGEMENT.MENU_ID = R_POSISI_MENU.MENU_MANAGEMENT_ID');
        $this->db->select('ADM_MENU_MANAGEMENT.NAMA_MENU,ADM_MENU_MANAGEMENT.URL');
        return $this->db->get('R_POSISI_MENU');
    } 
    
    public function getExist($id_posisi){
        $this->db->join('ADM_MENU_MANAGEMENT','ADM_MENU_MANAGEMENT.MENU_ID = R_POSISI_MENU.MENU_MANAGEMENT_ID');
        $this->db->select('ADM_MENU_MANAGEMENT.URL');
        $this->db->where('R_POSISI_MENU.POSISI_ID',$id_posisi);
        return $this->db->get('R_POSISI_MENU');
    }
    
    public function getChildKeuangan($idPosisi){
        $this->db->join('ADM_MENU_MANAGEMENT','ADM_MENU_MANAGEMENT.MENU_ID = R_POSISI_MENU.MENU_MANAGEMENT_ID');
        $this->db->select('ADM_MENU_MANAGEMENT.NAMA_MENU,ADM_MENU_MANAGEMENT.URL');
        $this->db->where('R_POSISI_MENU.POSISI_ID',$idPosisi);
        $this->db->where('ADM_MENU_MANAGEMENT.PARENT_ID',8);
        $this->db->order_by('ADM_MENU_MANAGEMENT.NAMA_MENU','asc');
        return $this->db->get('R_POSISI_MENU');
    }

    public function getDataUser($id){
        $this->db->where('ID',$id);
        return $this->db->get('PR_USER');
    }
    
    public function getChildMaster($idPosisi){
        $this->db->join('ADM_MENU_MANAGEMENT','ADM_MENU_MANAGEMENT.MENU_ID = R_POSISI_MENU.MENU_MANAGEMENT_ID');
        $this->db->select('ADM_MENU_MANAGEMENT.NAMA_MENU,ADM_MENU_MANAGEMENT.URL');
        $this->db->where('R_POSISI_MENU.POSISI_ID',$idPosisi);
        $this->db->where('ADM_MENU_MANAGEMENT.PARENT_ID',1);
        $this->db->order_by('ADM_MENU_MANAGEMENT.NAMA_MENU','asc');
        return $this->db->get('R_POSISI_MENU');
    } 
    
    public function getTop($idPosisi){
        $this->db->join('ADM_MENU_MANAGEMENT','ADM_MENU_MANAGEMENT.MENU_ID = R_POSISI_MENU.MENU_MANAGEMENT_ID');
        $this->db->select('ADM_MENU_MANAGEMENT.NAMA_MENU,ADM_MENU_MANAGEMENT.URL,ADM_MENU_MANAGEMENT.MENU_ID');
        $this->db->where('R_POSISI_MENU.POSISI_ID',$idPosisi);
        $this->db->where('ADM_MENU_MANAGEMENT.PARENT_ID',NULL);
        // $this->db->or_where('ADM_MENU_MANAGEMENT.PARENT_ID',);
        $this->db->order_by('ADM_MENU_MANAGEMENT.NO_URUT','asc');
        return $this->db->get('R_POSISI_MENU');
    }
    
    public function getCheckMenu(){
        $this->db->select('PARENT_ID');
        return $this->db->get('ADM_MENU_MANAGEMENT');
    }
    
    public function insertLoginHistory($data){
        $this->db->insert('LOGIN_HISTORY', $data);
        return $this->db->affected_rows();
    }

    public function updateGantiPassword($id){
        $pass = $this->input->post('password_baru');
        $pass_hash = password_hash($pass,PASSWORD_DEFAULT);
        $data = array(
            'PASSWORD' => $pass_hash
        );
        $this->db->where('ID', $id);
        $this->db->update('PR_KARYAWAN',$data);
    }

    public function cek_old($pass_old, $id){
        $query = $this->db->get_where('PR_KARYAWAN', array('ID'=>$id));
        if($query->num_rows() > 0){
            $data_user = $query->row();
            if(password_verify($pass_old, $data_user->PASSWORD)){
                return TRUE;
            } else{
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    public function data_email(){
        $this->db->select('*'); 
        $this->db->from('PR_KARYAWAN');   
        return $this->db->get()->result();
    }

    public function reset_password($reset_key, $password){
        $this->db->where('RESET_PASSWORD', $reset_key);
        $data = array('PASSWORD' => $password);
        $this->db->update('PR_KARYAWAN', $data);
        return ($this->db->affected_rows()>0) ? TRUE : FALSE;
    }

    public function check_reset_key($reset_key){
        $this->db->where('RESET_PASSWORD', $reset_key);
        $this->db->from('PR_KARYAWAN');
        return $this->db->count_all_results();
    }

    public function update_reset_key($reset_key, $email){
        $this->db->where('EMAIL_KANTOR',$email);
        $data = array('RESET_PASSWORD' => $reset_key);
        $this->db->update('PR_KARYAWAN', $data);
        if($this->db->affected_rows() > 0){
            return TRUE;
        } else {
            return FALSE;
        }

    }

    public function template_email_reset_password(){
        $this->db->join('ADM_TEMPLATE_EMAIL_VARIABLE', 'ADM_TEMPLATE_EMAIL_VARIABLE.KODE_AKTIVITAS_ID = ADM_TEMPLATE_EMAIL.KODE_AKTIVITAS_ID');
        $this->db->select('ADM_TEMPLATE_EMAIL.ISI_EMAIL');
        $this->db->where('ADM_TEMPLATE_EMAIL.KODE_AKTIVITAS_ID', 4);
        return $this->db->get('ADM_TEMPLATE_EMAIL');
    }



}