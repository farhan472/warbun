<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register_m extends CI_Model {
    public function insertUser($data){
		
		$this->db->insert('PR_USER', $data);
		return $this->db->affected_rows();
	}
}